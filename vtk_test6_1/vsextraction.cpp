#include "vsextraction.h"
#include <vtkDICOMImageReader.h>
#include <vtkPlaneSource.h>
#include <vtkTextureMapToPlane.h>
#include <vtkPolyDataMapper.h>
#include <vtkTexture.h>
#include <vtkActor.h>
#include <vtkImageData.h>
#include <vtkExtractVOI.h>
#include <vtkInformation.h>
#include <Qstring.h>
#include "vsDicomGrayScaller.h"


vsExtraction::vsExtraction():m_wc(25),m_wl(25)
{

    m_spacing[0] = 0;
    m_spacing[1] = 0;
}

void vsExtraction::setImageData(vtkImageData* reader) {

    m_reader = reader;
}

void vsExtraction::setCTMinMax( int min,int max ) {

    m_wc = (int)(( min + max ) * 0.5f );
    m_wl = max - min;

}


void vsExtraction::setSpacing(double* spacing) {

    m_spacing[0] = spacing[0];
    m_spacing[1] = spacing[1];


}

void vsExtraction::getTexturedPlane( int sid,vtkSmartPointer<vtkActor>& texturedPlane ) {


    //xyzの大きさをもらってくる．このままでは3次元画像として扱われる
    int* inputDims = m_reader->GetDimensions();

    //VOIで画像の一部を取り出す
    vtkSmartPointer<vtkExtractVOI> extractVOI =
        vtkSmartPointer<vtkExtractVOI>::New();
    extractVOI->SetInputData(m_reader);
    extractVOI->SetVOI( 0,inputDims[0],
                        0,inputDims[1],
                        sid,sid);//この値が取り出したいスライスを意味
    extractVOI->Update();



    vtkImageData* extracted = extractVOI->GetOutput();

    extracted->SetDimensions(inputDims[0],inputDims[1],1);

    //グレイスケール化
    vtkSmartPointer<vtkImageData> grayscale;
    vsDicomGrayScaller gs;
    gs.setWCWL( m_wc, m_wl );
    gs.getPixels(grayscale,extracted);


    //面の作成
    vtkSmartPointer<vtkPlaneSource> plane = vtkSmartPointer<vtkPlaneSource>::New();

    int width = m_reader->GetExtent()[1];
    int height = m_reader->GetExtent()[3];


    plane->SetOrigin(0, 0, 0);
    plane->SetPoint1( width*m_spacing[0],0,0);
    plane->SetPoint2( 0,height*m_spacing[1],0);

    //std::cout<< width*spacing[0]<<"  "<<height*spacing[1] <<std::endl;
    plane->SetCenter(width*m_spacing[0]/2, width*m_spacing[1]/2, sid*m_spacing[2]);

    plane->SetNormal(0.0, 0.0, 1.0);

    //テクスチャの作成
    vtkSmartPointer<vtkTexture> texture =
        vtkSmartPointer<vtkTexture>::New();
      texture->SetInputData(grayscale);

    //面にテクスチャを張り付ける
    vtkSmartPointer<vtkTextureMapToPlane> maptoplane =
        vtkSmartPointer<vtkTextureMapToPlane>::New();
      maptoplane->SetInputConnection(plane->GetOutputPort());

    //面を作製
    vtkSmartPointer<vtkPolyDataMapper> planeMapper =
        vtkSmartPointer<vtkPolyDataMapper>::New();
      planeMapper->SetInputConnection(maptoplane->GetOutputPort());

    //アクターとして作成
    texturedPlane = vtkSmartPointer<vtkActor>::New();
        texturedPlane->SetMapper(planeMapper);
        texturedPlane->SetTexture(texture);
        texturedPlane->SetPickable(false);


}
