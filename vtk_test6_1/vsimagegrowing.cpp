#include "vsimagegrowing.h"

vsImageGrowing::vsImageGrowing()
{
}



vsImageGrowing::~vsImageGrowing() {



}


void vsImageGrowing::setInputImage(vtkSmartPointer<vtkImageData> &imageData)  {

    m_Img = imageData;

}


template<class T>
void vsImageGrowing::execute(std::deque<SeedPoint> &seedPoints, std::deque<SeedCheckDir> &checkDirs,T& checker) {

    int* dims = m_Img->SetDimensions(dims[0],dims[1],dims[2]);
    vtkSmartPointer<vtkImageData>& dstImg = vtkSmartPointer<vtkImageData>::New();

    dstImg->SetDimensions(dims[0],dims[1],dims[2]);
    dstImg->AllocateScalars(VTK_TYPE_INT8);

    int rowLength = dims[0] * inData->GetNumberOfScalarComponents();
    int maxY = dims[1];
    int maxZ = dims[2];


    // Get increments to march through data

    int ext[6];
    int inIncX,inIncY,inIncZ;
    int outIncX,outIncY,outIncZ;
    dstImg->GetExtent(ext);
    m_Img->GetContinuousIncrements(ext, inIncX, inIncY, inIncZ);
    dstImg->GetContinuousIncrements(ext, outIncX, outIncY, outIncZ);

    short* src_pixel = static_cast<short*>(m_Img->GetScalarPointer());
    char* dst_pixel = static_cast<char*>(dstImage->GetScalarPointer());

    for( int z = 0; z < maxZ;++z) {

        for( int y = 0; y < maxY;++y) {

            for( int x = 0; x < maxX;++x) {

                *dst_pixel = 0;
                ++src_pixel;
                ++dst_pixel;
            }
            src_pixel += inIncY;
            dst_pixel += outIncY;
        }

        src_pixel += inIncZ;
        dst_pixel += outIncZ;
    }



    SeedPoint& p = seedPoints.front();
    seedPoints.pop_front();

    while( !seedPoints.empty() ) {

        for( unsigned int newidx = 0; newidx < checkDirs.size(); ++ newidx ) {

            SeedPoint checkPos = p + dir;
            if( checker.check( checkPos, m_Img ) ) {
                seedPoints.push_back(checkPos);
            }

        }

    }

}
