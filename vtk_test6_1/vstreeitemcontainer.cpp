#include "vstreeitemcontainer.h"
#include "vtkProp.h"
#include "vsTreeItem.h"

vsTreeItemContainer::vsTreeItemContainer()
{
}


vsTreeItemContainer::~vsTreeItemContainer()
{
}

std::list<vsTreeItem*>::iterator vsTreeItemContainer::begin() {

    return m_treeItems.begin();

}



void vsTreeItemContainer::push_back(vsTreeItem*& p) {

    m_treeItems.push_back(p);

}



std::list<vsTreeItem*>::iterator vsTreeItemContainer::end() {

    return m_treeItems.end();

}



void vsTreeItemContainer::findItem(std::list< vsTreeItem* >::iterator& itr,vtkProp* prop ) {

    for( itr = m_treeItems.begin(); itr != m_treeItems.end(); ++itr ) {

        if( (*itr)->m_prop == prop ) {
            break;
        }
    }

}



void vsTreeItemContainer::unselectAllItems() {


    std::list< vsTreeItem* >::iterator itr;
    for( itr = m_treeItems.begin(); itr != m_treeItems.end(); ++itr ) {

        (*itr)->setSelected(false);

    }

}
