﻿#ifndef VSDICOMGRAYSCALLER_H
#define VSDICOMGRAYSCALLER_H
#include "vsvtkinitializer.h"
#include <vtkSmartPointer.h>

class vtkImageData;


class vsDicomGrayScaller
{
private:
    double m_iWindowCenter;
    double m_iWindowWidth;
public:
    vsDicomGrayScaller();
    void getPixels(vtkSmartPointer<vtkImageData> &dstImage, vtkImageData *srcImage , bool transparent = true);
    void setWCWL(const double window_center, const double window_level );

};

#endif // VSDICOMGRAYSCALLER_H
