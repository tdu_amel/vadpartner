﻿#include "vsArtificialHeart.h"
#include "vmsg.h"


vsArtificialHeartParser *vsArtificialHeartParser::New()
{
    return new vsArtificialHeartParser();
}



/**
 * @brief vsArtificialHeartParser::parseXML コンパウンドVADの情報が記録されたXMLの解析を行って、パーツ分けされたVADデータを構築する
 * @param file　読み込むXML
 * @param ah　構築されたデータが入るスマートポインタ
 */
bool vsArtificialHeartParser::parseXML(QString& file,boost::shared_ptr<vsArtificialHeart>& ah){

    vBlock("vsArtificialHeartParser::parseXML");

    folderName = QDir(QFileInfo(file).dir());

    tinyxml2::XMLDocument xml;

    {
        vBlock("LoadFile");

        vDebugVar(file);

        xml.LoadFile(file.toStdString().c_str());

        if ( xml.Error() ) {

            vWarning("xml.Error()");
            vWarning("XML has error:%s",xml.ErrorName());
            return false;

        }

        vMsg("open is OK");

    }

    tinyxml2::XMLElement* element = 0;
    {
        vBlock("firstelement");

        //最初の要素を取得
        tinyxml2::XMLElement* artificialheart = xml.FirstChildElement("artificialheart");

        vDebugVar(artificialheart);
        if( !artificialheart ) {
            return false;
        }

        {
            vBlock("artificialheart");
            element = artificialheart->FirstChildElement("componemts");
        }

    }

    if ( !element ) {
        return false;
    }
    getElement(ah,element);

    return true;
}


/**
 * @brief vsArtificialHeartParser::getElement
 * @param me 下のelementの読み込んだ情報の保存先
 * @param element　上のmeの情報が入っていたファイルタグ
 */
void vsArtificialHeartParser::getElement(boost::shared_ptr<vsArtificialHeart>& me,tinyxml2::XMLElement* element) {

    vBlock("getElement");
    if( element == NULL )
    {
        return;
    }

    if ( element->Attribute( "type","stl" ) ) {
         //STL
         const char* attr = element->Attribute( "name" );
         vMsg("attr:%s",attr);
         me->attr = attr;
         me->name = folderName.absolutePath() + QString::fromLocal8Bit("/") + QString::fromLocal8Bit(attr);
         vMsg("fpath:%s",qPrintable(me->name));
    }

    //対象タグが子要素を持っているかチェック
    tinyxml2::XMLElement* child = element->FirstChildElement();
    if( child ) {
         vMsg("child");
         //自分に子要素の保存先を作る
         me->firstChild.reset(new vsArtificialHeart);
         //次は子要素を基準にして同様の処理を行う
         getElement(me->firstChild,child);
    }


    //対象タグが次の要素を持っているかチェック
    tinyxml2::XMLElement* elem_sibling = element->NextSiblingElement();
    if( elem_sibling ) {

            vMsg("sibling");
            //自分に同じ階層の次の要素の保存先を作る
            me->nextSibling.reset(new vsArtificialHeart);
            //次は隣の要素を基準にして同様の処理を行う
            getElement(me->nextSibling,elem_sibling);

    }

}

















/*
 *
void vsArtificialHeart::parseXML(const ptree::value_type &xml){

    name = xml.second.get<std::string>("name");
    address = xml.second.get<std::string>("address");
    gender = xml.second.get<std::string>("gender");
    age = xml.second.get<int>("age");

}
*/
