﻿#ifndef VSEXTRACTION_H
#define VSEXTRACTION_H
#include "vsvtkinitializer.h"

#include <vtkSmartPointer.h>
#include <vtkImageData.h>

class vtkActor;


class vsExtraction
{
private:
    vtkImageData* m_reader;
    int m_wc;//window center
    int m_wl;//window level
    double m_spacing[2];
public:
    vsExtraction();
    void setImageData(vtkImageData* reader);
    void getTexturedPlane( int sid,vtkSmartPointer<vtkActor>& texturedPlane);
    void getActor();
    void setSpacing(double* spacing);
    void setCTMinMax( int min,int max );
};

#endif // VSEXTRACTION_H
