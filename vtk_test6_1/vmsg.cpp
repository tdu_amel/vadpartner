﻿#include "vmsg.h"
#include <qdebug>


const QString g_log_file_name = QString("Log.txt");///< ログファイルの名前


__vMsg::__vMsg(QString blockname,unsigned int indent):
    m_localindent(indent),
    m_blockname(blockname)
{
    QString str;
    for ( unsigned int i = 0; i < m_indent; i++ ) {
        str += indentStr;
    }

    str += QString("start ") + QString("\"%1\"").arg(m_blockname);

    qDebug(qPrintable(str));
    m_indent += m_localindent;
}


__vMsg::__vMsg(QString blockname, const char* file, const int line, unsigned int indent):
    m_localindent(indent),
    m_blockname(blockname)
{

    QString str;
    setupBaseMessege(&str,file,line);

    str += QString("start ") + QString("\"%1\"").arg(m_blockname);

    m_fileName = file;

    qDebug(qPrintable(str));
    m_indent += m_localindent;
}


__vMsg::~__vMsg(){

    m_indent -= m_localindent;
    QString str;
    for ( unsigned int i = 0; i < m_indent; i++ ) {
        str += indentStr;
    }

    if ( m_fileName.length() > 0 ) {
        str += m_fileName + ">";
    }

    str += QString("end ") + QString("\"%1\"").arg(m_blockname);

    qDebug(qPrintable(str));

}


void __vMsg::setupBaseMessege(QString* str,const char *file, const int line) {

    if (!str) return;

    for ( unsigned int i = 0; i < m_indent; i++ ) {
        *str += indentStr;
    }

    if (file) {
        *str += file;
    }


    *str += QString("(%1)>").arg(line);


}

//qt_message

void __vMsg::msg(const char *file, const int line, const char* msg, const message_level, ...) {


    QString str;
    setupBaseMessege(&str,file,line);

    QString formatted_str;
    va_list ap;
    va_start(ap,msg);

#if QT_VERSION >= 0x050000
    formatted_str = QString::vasprintf(msg, ap);
#elif QT_VERSION >= 0x040800
    formatted_str = QString().vsprintf(msg, ap);
#endif

    va_end(ap);

    str += formatted_str;

    qDebug(qPrintable(str));
}

void __vMsg::warning(const char *file, const int line,const char* msg,...) {

    QString str;
    setupBaseMessege(&str,file,line);

    QString formatted_str;
    va_list ap;
    va_start(ap,msg);

#if QT_VERSION >= 0x050000
    formatted_str = QString::vasprintf(msg, ap);
#elif QT_VERSION >= 0x040800
    formatted_str = QString().vsprintf(msg, ap);
#endif

    va_end(ap);

    str += formatted_str;

    qWarning(qPrintable(str));

}

void __vMsg::critical(const char *file, const int line,const char* msg,...) {

    QString str;
    setupBaseMessege(&str,file,line);

    QString formatted_str;
    va_list ap;
    va_start(ap,msg);

#if QT_VERSION >= 0x050000
    formatted_str = QString::vasprintf(msg, ap);
#elif QT_VERSION >= 0x040800
    formatted_str = QString().vsprintf(msg, ap);
#endif

    va_end(ap);

    str += formatted_str;

    qCritical(qPrintable(str));

}

void __vMsg::fatal( const char *file,const int line,const char* msg,...) {
    QString str;
    setupBaseMessege(&str,file,line);

    QString formatted_str;
    va_list ap;
    va_start(ap,msg);

#if QT_VERSION >= 0x050000
    formatted_str = QString::vasprintf(msg, ap);
#elif QT_VERSION >= 0x040800
    formatted_str = QString().vsprintf(msg, ap);
#endif

    va_end(ap);

    str += formatted_str;

    qFatal(qPrintable(str));

}

unsigned int __vMsg::m_indent = 0;
QString __vMsg::indentStr = "|";
int __vMsg::message_level_console = 0;///<　コンソールに出力する最低メッセージレベル(msg = 0, warning = 1, critical = 2, fatal = 3)
int __vMsg::message_level_file = 0;///<　ファイルに出力する最低メッセージレベル(msg = 0, warning = 1, critical = 2, fatal = 3)
