﻿#include "vsCameraInteractor.h"
#include <vtkSmartPointer.h>
#include <vtkObjectFactory.h>

//vtkStandardNewMacro(vsCameraInteractor)
vsCameraInteractor *
vsCameraInteractor::New()
{
    return new vsCameraInteractor();
}
