#include "vshistgramemphasize.h"
#include <qdebug.h>

//#define vsHistgramEmphasize_DEBUG
vsHistgramEmphasize::vsHistgramEmphasize()
{
    from[0] = from[1] = 0;
    to[0] = to[1] = 0;
}


void vsHistgramEmphasize::setInputInfo(int min,int max) {

    from[0] = min;
    from[1] = max;

}

void vsHistgramEmphasize::setOutputInfo(int min,int max) {

    to[0] = min;
    to[1] = max;

}

void vsHistgramEmphasize::execute(vtkSmartPointer<vtkImageData>& dst, vtkImageData * src) {

    dst = vtkSmartPointer<vtkImageData>::New();
    dst->DeepCopy(src);

#ifdef vsHistgramEmphasize_DEBUG
    vMsg("vsHistgramEmphasize::extract()   started");
#endif


    int current_wl = from[1] - from[0];
    int center = (from[1] + from[0])/2;

    int result_wl = to[1] - to[0];

    double mul = result_wl * (1.0/current_wl);


    int* dims = dst->GetDimensions();
    short* start = (short*)(dst->GetScalarPointer(0,0,0));


     for( int z = 0; z < dims[2]; ++z ) {
#ifdef vsHistgramEmphasize_DEBUG
         vMsg("vsHistgramEmphasize-> z %d",z);
#endif
         int px1 = z*dims[1];
         for( int y = 0; y < dims[1]; ++y ) {


             int px2 = (y + px1)*dims[0];
             for( int x = 0; x < dims[0]; ++x ) {


                short* p = start+x+px2;
                *p = (short)(  ((*p) - center) * mul );


             }

         }

     }
#ifdef vsHistgramEmphasize_DEBUG
    vMsg("vsHistgramEmphasize::execute() finished");
#endif





}
