﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
// This is included here because it is forward declared in
// VadSim.h

//機能を詰め込みすぎてインクルードが一画面におさまりません
#include <QCloseEvent>
#include <QDateTime>
#include <QFileDialog>
#include <QMessageBox>
#include "mycounter.h"
#include <time.h>
#include <vtkActor.h>
#include <vtkAngleRepresentation3D.h>
#include <vtkAngleWidget.h>
#include <vtkArrowSource.h>
#include <vtkAxesActor.h>
#include <vtkCallbackCommand.h>
#include <vtkCamera.h>
#include <vtkCoordinate.h>
#include <vtkDICOMImageReader.h>
#include <vtkDistanceRepresentation3D.h>
#include <vtkDistanceWidget.h>
#include <vtkExecutionTimer.h>
#include <vtkExtractVOI.h>
#include <vtkImageCast.h>
#include <vtkImageData.h>
#include <vtkImageMedian3D.h>
#include <vtkImageNormalize.h>
#include <vtkImageThreshold.h>
#include <vtkImageWeightedSum.h>
#include <vtkImageWriter.h>
#include <vtkOrientationMarkerWidget.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <vtkVolume.h>
#include "vmsg.h"
#include "vsArtificialHeart.h"
#include "vsBoneDataConstructor.h"
#include "vsCTProgressFunctionObj.h"
#include "vsHeartDataConstructor.h"
#include "vsLiverDataConstructor.h"
#include "vsOrganExtraction.h"
#include "vsTransverseView.h"
#include "vsVector.h"
#include "vsdataexportersetval.h"
#include "vsextraction.h"
#include "vsinteractorstyleswitch.h"
#include "vslungdataconstructor.h"
#include "vsmouseinteractorstyle.h"
#include "vsvtkinitializer.h"


//Num Of ViewPort
#define VP_NUM 4


void MainWindow::export_measure_dist_start() {

    vBlock("export_measure_dist_start");
    QPushButton* senderBtn = reinterpret_cast<QPushButton*>( sender() );
    senderBtn->setText(QString("OK"));
    senderBtn->disconnect(senderBtn,SIGNAL(clicked()),this,SLOT(export_measure_dist_start()));
    senderBtn->connect(senderBtn,SIGNAL(clicked()),this,SLOT(export_measure_dist_end()));
    slotSetDistanceWidget();
}


void MainWindow::export_measure_angle_start() {

    QPushButton* senderBtn = reinterpret_cast<QPushButton*>( sender() );
    senderBtn->setText(QString("OK"));
    senderBtn->disconnect(senderBtn,SIGNAL(clicked()),this,SLOT(export_measure_angle_start()));
    senderBtn->connect(senderBtn,SIGNAL(clicked()),this,SLOT(export_measure_angle_end()));
    slotSetAngleWidget();

    //QMessageBox::information(this,QString("a"),QString("angle"),QMessageBox::Ok);

}


void MainWindow::export_measure_dist_end() {

    QPushButton* senderBtn = reinterpret_cast<QPushButton*>( sender() );
    senderBtn->setText(QString("measure"));

    senderBtn->disconnect(senderBtn,SIGNAL(clicked()),this,SLOT(export_measure_dist_end()));
    senderBtn->connect(senderBtn,SIGNAL(clicked()),this,SLOT(export_measure_dist_start()));

/*
    QVBoxLayout* data = reinterpret_cast<QVBoxLayout*>(senderBtn->property("myui").toLongLong());

    if ( !data ) {
        vMsg("myui is invalid");
        return;
    }
*/
    QLineEdit* lineedit = reinterpret_cast<QLineEdit*>(senderBtn->property("lineedit").toLongLong());

    if ( !lineedit ) {
        vMsg("lineedit is invalid");
        return;

    }

    if ( distanceWidget.Get() ) {
        lineedit->setText( QString::number( distanceWidget->GetDistanceRepresentation()->GetDistance() ) );
        distanceWidget->Off();
    }


}

void MainWindow::export_measure_angle_end() {

    QPushButton* senderBtn = reinterpret_cast<QPushButton*>( sender() );
    senderBtn->setText(QString("measure"));

    senderBtn->disconnect(senderBtn,SIGNAL(clicked()),this,SLOT(export_measure_angle_end()));
    senderBtn->connect(senderBtn,SIGNAL(clicked()),this,SLOT(export_measure_angle_start()));
    QLineEdit* lineedit = reinterpret_cast<QLineEdit*>(senderBtn->property("lineedit").toLongLong());

    if ( !lineedit ) {
        vMsg("lineedit is invalid");
        return;

    }

    if ( angleWidget.Get() ) {
        lineedit->setText( QString::number( (double)(angleWidget->GetAngleRepresentation()->GetAngle() * 60 )) );
        angleWidget->Off();
    }

}


/**
 * @brief VadSim::setOrientionIcon 今のカメラの向きがわかるプチ座標軸のようなものを表示します
 */
void MainWindow::setOrientionIcon() {

    vMsg("setOrientionIcon");

    if ( axes.Get() == 0 ) {
        axes = vtkSmartPointer< vtkAxesActor >::New();
    }

    if ( widget.Get() == 0 ) {
        widget = vtkSmartPointer<vtkOrientationMarkerWidget>::New();
    }

    widget->SetOrientationMarker( axes );
    widget->SetOutlineColor( 1, 1, 1 );
    widget->SetViewport( 0.0, 0.0, 0.2, 0.2 );
    widget->SetInteractor(m_renderinteractor[0]);
    widget->SetCurrentRenderer(m_renderer[0]);
    widget->SetEnabled(1);
    widget->InteractiveOff();
    widget->SetPickingManaged(false);

}


/**
 * @brief VadSim::loadCompoundSTL
 * メニューから複合STLデータを読み込む項目が選択されたときに呼び出されます．
 */
void MainWindow::loadCompoundSTL() {

    vBlock("loadCompoundSTL");

    QString xml_file_name = QFileDialog::getOpenFileName(
            this,
            tr("Choose Compound Vad Data" ) ,
            ".",
            QString::fromLocal8Bit( "XML file (*.xml)" ) );

    if ( !xml_file_name.isEmpty() )
    {

        boost::shared_ptr<vsArtificialHeart> ah(new vsArtificialHeart);

        vMsg( "compound:%s", qPrintable( xml_file_name ) );
        vtkSmartPointer<vsArtificialHeartParser> parser = vtkSmartPointer<vsArtificialHeartParser>::New();
        if( parser->parseXML(xml_file_name,ah) ) {
            vMsg("parseXML:ok");
            addCompoundSTL((QTreeWidgetItem*)NULL,ah->firstChild);
        }
        else {
            QMessageBox::warning(this,QString("load error!"),QString("can't load \"%1\"").arg(xml_file_name));
        }

    }

}


/**
 * @brief VadSim::addCompoundSTL
 * XMLをパースして得られたファイル名から実際にSTL読み込んでいく．
 * @param parent
 *  treeviewの親アイテム
 * @param ah
 * ahの中にはSTLのデータとファイル名を入れるが，でこの時点ではファイル名のみが入っている
 */
void MainWindow::addCompoundSTL(QTreeWidgetItem* parent,boost::shared_ptr<vsArtificialHeart>& ah) {

    vBlock("addCompoundSTL");

    QString stlFname(ah->name);
    vsTreeItem* item = new vsTreeItem(parent,QStringList(QFileInfo(stlFname).fileName()));

    if(parent) {
        parent->addChild(item);
        item->m_bHasRel = true;
    }
    else {
        ui->treeWidget->insertTopLevelItem(0,item);
    }

    vDebugVar( stlFname );


    MyCounter load("CompoundSTLTime.txt");
    load.begin();

        vtkSmartPointer<vtkActor> actor_STL = vtkSmartPointer<vtkActor>::New();
        m_stlManager.loadSTL(stlFname.toLocal8Bit(),actor_STL);
        m_renderer[0]->AddActor(actor_STL);

    load.end();


    item->m_prop = actor_STL;
    m_treeItems.push_back(item);

    if( ah->firstChild ) {
        addCompoundSTL(item,ah->firstChild);
    }

    if( ah->nextSibling ) {
        addCompoundSTL(parent,ah->nextSibling);
    }
    m_renderer[0]->ResetCamera();
    this->ui->qvtkWidget1->GetRenderWindow()->Render();

}


/**
 * @brief VadSim::loadSTL
 * メニューの「STLを読み込む」を選んだ時に呼び出されるスロット
 */
void MainWindow::loadSTL() {

    vBlock("loadSTL");

    QString stlFname = QFileDialog::getOpenFileName(
            this,
            tr("Choose Vad Data file" ) ,
            ".",
            QString::fromLocal8Bit( "STLfile (*.stl)" ) );


    if ( !stlFname.isEmpty() )
    {

        //ツリービューにアイテムを追加
        vsTreeItem* item = new vsTreeItem((QTreeWidgetItem*)NULL,QStringList(QString(QFileInfo(stlFname).fileName().toLocal8Bit())));
        ui->treeWidget->insertTopLevelItem(0,item);

        vDebugVar( stlFname );

        MyCounter load("STLTime.txt");
        load.begin();
            vtkSmartPointer<vtkActor> actor_STL = vtkSmartPointer<vtkActor>::New();
            m_stlManager.loadSTL(stlFname.toLocal8Bit(),actor_STL);
            m_renderer[0]->AddActor(actor_STL);
        load.end();

        item->m_prop = actor_STL;
        m_treeItems.push_back(item);

    }

    m_renderer[0]->ResetCamera();
    this->ui->qvtkWidget1->GetRenderWindow()->Render();
}




void MainWindow::construct_Lung(vsTreeItem* treeitemHumanBody, vtkImageData *loadedDicom) {
    actor_lung = vtkSmartPointer<vtkActor>::New();
    vsBoneDataConstructor::constructModel(actor_lung,loadedDicom);
    vsTreeItem* treeitemLung = new vsTreeItem(0,QStringList(QString("Lung")));
    treeitemLung->m_prop = actor_lung;
    m_treeItems.push_back(treeitemLung);
    treeitemHumanBody->addChild(treeitemLung);
    m_renderer[0]->AddActor(actor_lung); //set to m_renderer
}

void MainWindow::construct_Bone(vsTreeItem* treeitemHumanBody, vtkImageData *loadedDicom) {

    actor_bone = vtkSmartPointer<vtkActor>::New();
    vsBoneDataConstructor::constructModel(actor_bone,loadedDicom);
    vsTreeItem* treeitemBone = new vsTreeItem(0,QStringList(QString("Bone")));
    treeitemBone->m_prop = actor_bone;
    m_treeItems.push_back(treeitemBone);
    treeitemHumanBody->addChild(treeitemBone);
    m_renderer[0]->AddActor(actor_bone); //set to m_renderer

}


void MainWindow::construct_Liver(vsTreeItem* treeitemHumanBody, vtkImageData *loadedDicom, double w, double l) {

    actor_liver = vtkSmartPointer<vtkActor>::New();

    vsLiverDataConstructor::constructModel(actor_liver,loadedDicom,w,l);

    vsTreeItem* treeitemLiver = new vsTreeItem(0,QStringList(QString("Liver")));
    treeitemLiver->m_prop = actor_liver;
    m_treeItems.push_back(treeitemLiver);
    treeitemHumanBody->addChild(treeitemLiver);
    m_renderer[0]->AddActor(actor_liver); //set to m_renderer

}


void MainWindow::construct_Heart(vsTreeItem* treeitemHumanBody, vtkImageData *loadedDicom) {

    actor_heart = vtkSmartPointer<vtkActor>::New();
    vsHeartDataConstructor::constructModel(actor_heart,loadedDicom);

    vsTreeItem* treeitemHeart = new vsTreeItem(0,QStringList(QString("Heart")));
    treeitemHeart->m_prop = actor_heart;
    m_treeItems.push_back(treeitemHeart);
    treeitemHumanBody->addChild(treeitemHeart);
    m_renderer[0]->AddActor(actor_heart); //set to m_renderer
}


void MainWindow::loadCT() {

    //フォルダを開くダイアログ
    QString tgfile = QFileDialog::getExistingDirectory(this,tr("Choose CT data directory"),".");

    if ( tgfile.isEmpty() ) {
        return;
    }

    // ファイルに対する処理
    MyCounter load("CTTime.txt");
    load.begin();

    vtkSmartPointer<vtkCallbackCommand> progressCallback = vtkSmartPointer<vtkCallbackCommand>::New();
    progressCallback->SetCallback(vsCTProgressFunctionObj::reportProgress);

    vsCTProgressFunctionObj::start(0);

    vtkSmartPointer<vtkDICOMImageReader> reader = vtkSmartPointer<vtkDICOMImageReader>::New();

    reader->SetDirectoryName(tgfile.toStdString().c_str());
    reader->AddObserver(vtkCommand::ProgressEvent, progressCallback);
    reader->Update();
    vsCTProgressFunctionObj::end();

    //4分割ビューの2D担当の3つにDICOMリーダをセット
    vsDicomViewOrientation o[] = {VS_DVORIENTATION_Z,VS_DVORIENTATION_X,VS_DVORIENTATION_Y};
    for(int newidx = 0; newidx<VP_NUM-1;++newidx) {

        view[newidx].setDicomReader(reader);
        view[newidx].setOrientation(o[newidx]);
        view[newidx].resetSlicePos();
        view[newidx].activate();

     }

            //マーチングキューブ法を用いたサーフェスレンダリング
            vsTreeItem* treeitemHumanBody = new vsTreeItem((QTreeWidgetItem*)NULL,QStringList(QString("Human Body")));

            ui->treeWidget->insertTopLevelItem(0,treeitemHumanBody);
            treeitemHumanBody->m_prop = 0;
            m_treeItems.push_back(treeitemHumanBody);

            {
                MyCounter OrganExtractionLung("OrganExtractionLung.txt");
                OrganExtractionLung.begin();
                construct_Lung(treeitemHumanBody,reader->GetOutput());
                OrganExtractionLung.end();
            }

            {
                MyCounter OrganExtractionBone("OrganExtractionBone.txt");
                OrganExtractionBone.begin();
                construct_Bone(treeitemHumanBody,reader->GetOutput());
                OrganExtractionBone.end();
            }


            {
                //肝臓の読み込み
                std::string dirName = reader->GetDirectoryName();
                dirName += "\\";

                vMsg(dirName.c_str());

                QString filePath = dirName.c_str();

                if( filePath.length() > 0 ) {

                    double w,l;
                    filePath.replace("/","\\");
                    boost::shared_ptr<vsImageDICOM> dcm;
                    vsImageLoaderDICOM loader;

                    if( loader.load(filePath.toStdString().c_str(),&dcm) ) {
                        w = dcm->m_iWindowWidth;
                        l = dcm->m_iWindowCenter;
                    }

                    vMsg(filePath.toStdString().c_str());
                    vMsg("%lf,%lf",w,l);
                    //construct_Liver(treeitemHumanBody,reader->GetOutput(),w,l);
                }
            }


        {
                //heart
                //construct_Heart(treeitemHumanBody,reader->GetOutput());
        }

        load.end();

        ui->horizontalSlider->setMinimum(0);
        int* ext = reader->GetDataExtent();
        ui->horizontalSlider->setMaximum(ext[5]);

        rerenderAll();

}

/**
 * @brief resetCamera　カメラの初期化を行う
 * @param v　初期化を行いたいビュー(4分割部分)のID
 */
void MainWindow::resetCamera( int newidx ) {

    //対象レンダラーが構築される前に呼ばれたら終了
    if ( m_renderer[newidx] == 0 ) return;

    vMsg("reset cam");

    vsVector eyepos[4];
    vsVector focuspos[4];
    vsVector updir[4];

        eyepos[0] = vsVector(0,5000,0);
        eyepos[1] = vsVector(0, 0,5000);//axial?
        eyepos[2] = vsVector(5000,   0,0);//sagittal?
        eyepos[3] = vsVector(0,   5000,0);//coronal?



        focuspos[0] = vsVector(0,0,0);
        focuspos[1] = vsVector(0,0,0);//axial?
        focuspos[2] = vsVector(0,0,0);//sagittal?
        focuspos[3] = vsVector(0,0,0);//coronal?


        updir[0] = vsVector(0,0,-1);
        updir[1] = vsVector(0,-1,0);//axial?
        updir[2] = vsVector(0,0,-1);//sagittal?
        updir[3] = vsVector(0,0,-1);//coronal?

        vMsg("reset cam:%d",newidx);
        vtkCamera* cam = m_renderer[newidx]->GetActiveCamera();

        if( cam == 0 ) {
            vWarning("Camera is Null");
            return;
        }


        //視点を(x,y,z) = (0,5000,0)にセット
        cam->SetPosition( eyepos[newidx].x, eyepos[newidx].y, eyepos[newidx].z );

        //注視点を(x,y,z) = (0,0,0)にセット
        cam->SetFocalPoint( focuspos[newidx].x, focuspos[newidx].y, focuspos[newidx].z );

        //上方定義ベクトルを(x,y,z) = (0,0,-1)にセット
        cam->SetViewUp( updir[newidx].x, updir[newidx].y, updir[newidx].z );


        cam->SetParallelProjection(true);

        vMsg("set cam to renderer:%d",newidx);

        //レンダラーにカメラをセット
        m_renderer[newidx]->ResetCamera();

}


// Constructor
MainWindow::MainWindow(QWidget *parent):
    QMainWindow(parent)
{

    vBlock("init");
    initGUI();
    initVTK();
}


bool MainWindow::event(QEvent *e) {

    if (e->type() == QEvent::WindowActivate) {

        if( !m_setvalWindow.data() ) {
            m_setvalWindow = QSharedPointer<vsDataExporterSetVal>(new vsDataExporterSetVal(0));
            m_setvalWindow->setMainWindow(this);
            m_setvalWindow->show();
        }

    }
    return QWidget::event(e);

}

/**
 * @brief initGUI　GUIの設定を行う
 */
void MainWindow::initGUI() {

    vBlock("initGUI");

    this->ui = new Ui::VadSim;
    this->ui->setupUi(this);

    // Set up action signals and slots
    connect(this->ui->button_dist, SIGNAL(clicked()), this, SLOT(slotSetDistanceWidget()));
    connect(this->ui->button_angle, SIGNAL(clicked()), this, SLOT(slotSetAngleWidget()));
    connect(this->ui->resetCamera, SIGNAL(clicked()), this, SLOT(slotResetCamera()));
    connect(this->ui->edit_x, SIGNAL(editingFinished()), this, SLOT(slotEditingFinished()));
    connect(this->ui->edit_y, SIGNAL(editingFinished()), this, SLOT(slotEditingFinished()));
    connect(this->ui->edit_z, SIGNAL(editingFinished()), this, SLOT(slotEditingFinished()));

    connect(this->ui->edit_angle_x, SIGNAL(editingFinished()), this, SLOT(slotEditingFinished()));
    connect(this->ui->edit_angle_y, SIGNAL(editingFinished()), this, SLOT(slotEditingFinished()));
    connect(this->ui->edit_angle_z, SIGNAL(editingFinished()), this, SLOT(slotEditingFinished()));

    connect(this->ui->radioButton_abs, SIGNAL(clicked()), this, SLOT(slotRadioClicked()));
    connect(this->ui->radioButton_rel, SIGNAL(clicked()), this, SLOT(slotRadioClicked()));

    connect(this->ui->radioButton_rel, SIGNAL(clicked()), this, SLOT(slotRadioClicked()));

    connect(this->ui->treeWidget,SIGNAL(itemClicked(QTreeWidgetItem*,int)),this,SLOT(slotTreeSelected(QTreeWidgetItem*, int)));
    connect(this->ui->horizontalSlider,SIGNAL(valueChanged(int)),this,SLOT(slotSlideBarChanged(int)));
    connect(this->ui->horizontalSlider_min,SIGNAL(valueChanged(int)),this,SLOT(slotSlideBarChanged2(int)));
    connect(this->ui->horizontalSlider_max,SIGNAL(valueChanged(int)),this,SLOT(slotSlideBarChanged2(int)));


}





void MainWindow::on_actionOpen_Compound_VAD_triggered() {
    loadCompoundSTL();
}


void MainWindow::on_actionOpen_VAD_triggered() {
    loadSTL();
}


void MainWindow::on_actionOpen_DICOM_triggered() {
    loadCT();
}


/**
 * @brief initVTK　VTKの初期化処理を行う
 */
void MainWindow::initVTK() {

    vBlock("initVTK");

    QVTKWidget* qvtk[] = {this->ui->qvtkWidget1,this->ui->qvtkWidget2,this->ui->qvtkWidget3,this->ui->qvtkWidget4};

    double col[4] = { 0.5,0.6,0.7,0.8};

    //レンダラーの作成
    for(int newidx = 0; newidx < VP_NUM; ++newidx ) {

        m_renderer[newidx] =  vtkSmartPointer<vtkRenderer>::New();

        if ( !(m_renderer[newidx]) ) {
            vDebugVar(m_renderer[newidx]);
            continue;
        }

        //背景色の設定
        m_renderer[newidx]->SetBackground( col[newidx],col[newidx],col[newidx] );
        resetCamera(newidx);

        //Windowにレンダラーをセット
        qvtk[newidx]->GetRenderWindow()->AddRenderer(m_renderer[newidx]);

        //現在のインターアクター(イベントハンドラ)を取得
        m_renderinteractor[newidx] = qvtk[newidx]->GetRenderWindow()->GetInteractor();
    }

    vMsg("cam ok");


    //方向を示すアイコンのセット
    setOrientionIcon();

    for(int newidx = 0; newidx<VP_NUM-1;++newidx) {
        view[newidx].setInteractor(qvtk[newidx+1]->GetRenderWindow()->GetInteractor());
        view[newidx].setRenderer(m_renderer[newidx+1]);
    }

    //カメラモードと，移動モードの選択ができるインターアクションスタイルの設定
    m_style = vtkSmartPointer<vsInteractorStyleSwitch>::New();


    m_style->setContainer(&m_treeItems);
    m_style->setUI(this->ui);
    m_style->setSTLManager(&m_stlManager);

    m_renderinteractor[0]->SetInteractorStyle( m_style );

    m_style->SetDefaultRenderer(m_renderer[0]);


    //スタイルをインターアクターにセット
    m_renderinteractor[0]->SetInteractorStyle( m_style );

    m_style->setCameraMode();

}



/**
 * @brief rerender　すべてのビューでカメラの初期化を行う
 */
void MainWindow::resetCameraAll() {

    for( int newidx = 0; newidx < VP_NUM; ++newidx ) {
        resetCamera(newidx);
    }

}

/**
 * @brief rerender　すべてのビューで再描画を行う
 */
void MainWindow::rerenderAll() {

    for( int newidx = 0; newidx < VP_NUM; ++newidx ) {
        rerender(newidx);
    }

}


/**
 * @brief rerender　再描画を行う
 * @param v　再描画を行いたいビュー(4分割部分)のID
 */
void MainWindow::rerender(int v) {

    QVTKWidget* qvtk[] = {this->ui->qvtkWidget1,this->ui->qvtkWidget2,this->ui->qvtkWidget3,this->ui->qvtkWidget4};

    m_renderer[v]->Render();
    qvtk[v]->GetRenderWindow()->Render();


}


void MainWindow::slotSlideBarChanged2(int) {
/*
    vtkVolume* volume;
    if( vol.update(ui->horizontalSlider_min->value(),ui->horizontalSlider_max->value(),m_renderer[0]) ){
        volume = vol.getVolume();

        //選択できないようにする
        volume->SetPickable(false);
    }
*/
    ui->ct_min->setText(QString::number(ui->horizontalSlider_min->value()));
    ui->ct_max->setText(QString::number(ui->horizontalSlider_max->value()));


    if( actor_dicom_plane.Get() == 0 ) return;

    m_renderer[0]->RemoveActor(actor_dicom_plane);


    vMsg("actor_dicom_plane slotSlideBarChanged2");
    m_extraction.setCTMinMax(ui->horizontalSlider_min->value(),ui->horizontalSlider_max->value());
    m_extraction.getTexturedPlane(ui->horizontalSlider->value(),actor_dicom_plane);
    m_renderer[0]->AddActor(actor_dicom_plane);
    m_renderer[0]->Render();


    QVTKWidget* qvtk[] = {this->ui->qvtkWidget1,this->ui->qvtkWidget2,this->ui->qvtkWidget3,this->ui->qvtkWidget4};
    for( int newidx = 0; newidx < VP_NUM; ++newidx ) {
        m_renderer[newidx]->Render();
        qvtk[newidx]->GetRenderWindow()->Render();
    }

}



void MainWindow::slotSlideBarChanged(int val) {

    if( actor_dicom_plane.Get() == 0 ) return;

    vMsg("slotSlideBarChanged");
    m_renderer[0]->RemoveActor(actor_dicom_plane);
    m_extraction.getTexturedPlane(val,actor_dicom_plane);
    m_renderer[0]->AddActor(actor_dicom_plane);
    m_renderer[0]->Render();

    QVTKWidget* qvtk[] = {this->ui->qvtkWidget1,this->ui->qvtkWidget2,this->ui->qvtkWidget3,this->ui->qvtkWidget4};
    for( int newidx = 0; newidx < VP_NUM; ++newidx ) {
        m_renderer[newidx]->Render();
        qvtk[newidx]->GetRenderWindow()->Render();
    }


}


void MainWindow::slotTreeSelected( QTreeWidgetItem * item, int ) {

    m_style->getInteractorMove()->setRadioButtonState( reinterpret_cast<vsTreeItem*>(item) ->m_prop);
    m_style->getInteractorMove()->setSTLPosition( reinterpret_cast<vsTreeItem*>(item) ->m_prop);
    m_style->getInteractorMove()->setSTLAngle( reinterpret_cast<vsTreeItem*>(item) ->m_prop);
    m_style->getInteractorMove()->setSelectedActorName( reinterpret_cast<vsTreeItem*>(item) ->m_prop);

    m_stlManager.selectSTL(reinterpret_cast<vtkActor*>(reinterpret_cast<vsTreeItem*>(item)->m_prop));

    rerender(0);

}

struct distanceWidgetCallBack:public vtkCommand {
    static distanceWidgetCallBack *New()
      { return new distanceWidgetCallBack; }
    void Execute(vtkObject*, unsigned long eventId, void*) VTK_OVERRIDE
    {
        vBlock("distanceWidgetCallBack::Execute");

        switch (eventId)
        {
          case vtkCommand::LeftButtonReleaseEvent:
            DistanceWidget->GetDistanceRepresentation();
            break;
        }

    }

    vtkSmartPointer<vtkDistanceWidget> DistanceWidget;
};

/**
 * @brief VadSim::setDistanceWidget 距離の測定を行うウィジェットの設定を行う
 */
void MainWindow::slotSetDistanceWidget() {


    vBlock("slotSetDistanceWidget");

    if( !distanceWidget.Get() ) {
        distanceWidget = vtkSmartPointer<vtkDistanceWidget>::New();

        dist_representation = vtkSmartPointer<vtkDistanceRepresentation3D>::New();
        dist_representation->GetLineProperty()->SetColor(1.0,0,0);
        dist_representation->GetLabelProperty()->SetColor(1.0,0,0);
        distanceWidget->SetInteractor(m_renderinteractor[0]);
        distanceWidget->SetRepresentation(dist_representation);
        reinterpret_cast<vtkDistanceRepresentation *>(distanceWidget->GetRepresentation())->SetLabelFormat("%-#7.8g mm");
        dist_representation->SetLabelScale(15.0,15.0,15.0);
    }

    distanceWidget->On();

    _dist_widget_callback = vtkSmartPointer<distanceWidgetCallBack>::New();
    _dist_widget_callback->DistanceWidget = distanceWidget;
    distanceWidget->AddObserver(vtkCommand::EndInteractionEvent,_dist_widget_callback.Get());



}



void MainWindow::slotRadioClicked() {

    vBlock("slotRadioClicked");

    QTreeWidgetItem* item = ui->treeWidget->currentItem();

    if (item == 0) {
        vMsg("Nothing Selected");
    }

    if( ui->radioButton_abs->isChecked() ) {

         reinterpret_cast<vsTreeItem*>(item)->m_coord_mode = VCM_ABSOLUTE;

    } else {

         reinterpret_cast<vsTreeItem*>(item)->m_coord_mode = VCM_RELATIVE;

    }

    m_style->getInteractorMove()->setSTLPosition( reinterpret_cast<vsTreeItem*>(item)->m_prop );
    m_style->getInteractorMove()->setSTLAngle( reinterpret_cast<vsTreeItem*>(item)->m_prop );


}



void MainWindow::slotEditingFinished() {

   //if(ui->treeWidget->topLevelItemCount() == 0 )return;
    vBlock("slotEditingFinished");

    QTreeWidgetItem* item = ui->treeWidget->currentItem();

    if (item == 0) {
        vMsg("Nothing Selected");
    }


    double pos[3];
    pos[0] = ui->edit_x->text().toDouble();
    pos[1] = ui->edit_y->text().toDouble();
    pos[2] = ui->edit_z->text().toDouble();

    double angle[3];
    angle[0] = ui->edit_angle_x->text().toDouble();
    angle[1] = ui->edit_angle_y->text().toDouble();
    angle[2] = ui->edit_angle_z->text().toDouble();

    vsTreeItem* myTree = reinterpret_cast<vsTreeItem*>(item);

    if ( !myTree ) {
        return;
    }

    if( ui->radioButton_rel->isChecked() ) {

        for( int newidx = 0; newidx < 3; ++newidx ) {

            pos[newidx] += myTree->pos[newidx];
            angle[newidx] += myTree->angle[newidx];

        }

    }

    vtkProp3D* prop = reinterpret_cast<vtkProp3D*>( myTree->m_prop );
    if ( !prop ) {
        return;
    }

    prop->SetPosition( pos[0], pos[1],pos[2] );
    prop->SetOrientation( angle[0], angle[1],angle[2] );

    if ( myTree->m_prop ) {
        m_style->conveyTransform(myTree->m_prop);
    }

    rerenderAll();

}


void MainWindow::slotSetAngleWidget() {

    vBlock("slotSetAngleWidget");

    if ( !angleWidget ) {
        angleWidget = vtkSmartPointer<vtkAngleWidget>::New();
        angleWidget->SetInteractor(m_renderinteractor[0]);
        _angle_representation = vtkSmartPointer<vtkAngleRepresentation3D>::New();
        angleWidget->SetInteractor(m_renderinteractor[0]);
        angleWidget->SetRepresentation(_angle_representation);
    }

    angleWidget->On();


}

/**
 * @brief VadSim::slotResetCamera
 * カメラリセットのボタンを押したときに呼ばれるスロット
 */
void MainWindow::slotResetCamera() {
    vMsg("resetCam\n");

    resetCameraAll();
    rerenderAll();

}

/**
 * @brief VadSim::closeEvent
 * ウィンドウが閉じられるときに自動的に呼ばれる関数
 * @param e
 * イベントオブジェクト
 */
void MainWindow::closeEvent(QCloseEvent* e) {

    if( m_setvalWindow.data() != 0 ) {
        m_setvalWindow->close();
    }
    e->accept();
    qApp->exit();

}

void MainWindow::slotExit()
{

    if( m_setvalWindow.data() != 0 ) {
        vMsg("Close Dialog");
        m_setvalWindow->close();
    }
    qApp->exit();
}


MainWindow::~MainWindow(){

    vBlock("~MainWindow");

    m_setvalWindow.clear();

    if (this->ui) {
        delete this->ui;
        this->ui = 0;
    }

}


void MainWindow::on_pushButton_toCamMode_clicked()
{

    m_style->setCameraMode();
}

void MainWindow::on_pushButton_toMoveMode_clicked()
{
     m_style->setMoveMode();
}
