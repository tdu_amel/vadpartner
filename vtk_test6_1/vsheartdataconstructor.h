﻿#ifndef VSHEARTDATACONSTRUCTOR_H
#define VSHEARTDATACONSTRUCTOR_H

#include <vtkSmartPointer.h>
#include "vsOrganExtraction.h"
class vtkActor;
class vtkImageData;

class vsHeartDataConstructor:public vsOrganExtraction
{
public:
    vsHeartDataConstructor();
    static void constructImageData(vtkSmartPointer<vtkImageData>& dscImg,vtkImageData *srcImg);
    static void constructModel(vtkSmartPointer<vtkActor> &actor, vtkImageData *srcImg);
};


#endif // VSHEARTDATACONSTRUCTOR_H
