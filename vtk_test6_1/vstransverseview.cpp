﻿#include "vstransverseview.h"
#include "vsimageplanewidgetinteractstyle.h"
#include "qString.h"
#include "vMsg.h"
vsTransverseView::vsTransverseView()
{

    m_orientation = VS_DVORIENTATION_X;

    m_plane = vtkSmartPointer<vtkImagePlaneWidget>::New();

    m_plane->SetPicker(m_picker);
    m_plane->RestrictPlaneToVolumeOn();

    double col[] = {0.0,0.0,0.0};
    m_plane->GetPlaneProperty()->SetColor(col);

    m_plane->SetTexturePlaneProperty(m_property);
    m_plane->TextureInterpolateOff();
    m_plane->SetResliceInterpolateToLinear();
    m_plane->SetWindowLevel(1358,-27);
    m_plane->SetSlicePosition(0);





}


vsTransverseView::~vsTransverseView() {
}



void vsTransverseView::setInteractor(vtkRenderWindowInteractor *iRen)
{

    m_style = vtkSmartPointer<vsInteractorStyleImage>::New();
    m_style->setInteractor(iRen);
    m_style->setWidget(this);

    iRen->SetInteractorStyle(m_style.Get());


    m_plane->SetInteractor(iRen);
}



void vsTransverseView::setDicomReader(vtkSmartPointer<vtkDICOMImageReader>& reader)
{

    m_reader = reader;
    m_plane->SetInputConnection(m_reader->GetOutputPort());

    //xyzの大きさをもらってくる
    int* inputDims = m_reader->GetOutput()->GetDimensions();
    setSliceIndex(inputDims[m_orientation]/2);

    double w = m_plane->GetWindow();
    double c = m_plane->GetLevel();

    vMsg("w:%lf c:%lf",w,c);

}

void vsTransverseView::resetSlicePos() {
    int* inputDims = m_reader->GetOutput()->GetDimensions();
    setSliceIndex(inputDims[m_orientation]/2);
}

void vsTransverseView::setOrientation(vsDicomViewOrientation o)
{

    m_orientation = o;
    if( o == VS_DVORIENTATION_X) m_plane->SetPlaneOrientationToXAxes();
    else if( o == VS_DVORIENTATION_Y) m_plane->SetPlaneOrientationToYAxes();
    else if( o == VS_DVORIENTATION_Z) m_plane->SetPlaneOrientationToZAxes();


}


 void vsTransverseView::setSliceIndex(int idx) {

    m_plane->SetSliceIndex(idx);

    m_renderer->ResetCamera();



 }


void vsTransverseView::setRenderer(vtkSmartPointer<vtkRenderer>& ren)
{

    //参照カウントを増やすためspに保存
    m_renderer = ren;
    //m_renderer->SetActiveCamera(camera);
    m_plane->SetDefaultRenderer(m_renderer);



}

void vsTransverseView::activate() {

    m_plane->On();
    m_plane->InteractionOn();

}


int vsTransverseView::getSliceIndex()
{

    return m_plane->GetSliceIndex();

}


int vsTransverseView::getSliceMaxIndex()
{
    int* inputDims = m_reader->GetOutput()->GetDimensions();


    return inputDims[m_orientation];

}
