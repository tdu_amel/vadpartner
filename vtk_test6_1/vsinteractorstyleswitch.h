﻿#ifndef MYINTERACTORSTYLESWITCH_H
#define MYINTERACTORSTYLESWITCH_H
#include "ui_mainwindow.h"
#include "vsvtkinitializer.h"
#include "vsActorRelocateInteractor.h"
#include "vsCameraInteractor.h"
#include "vsSTLManager.h"
#include "vstreeitemcontainer.h"
#include <vtkAngleRepresentation3D.h>
#include <vtkAngleWidget.h>
#include <vtkDistanceRepresentation3D.h>
#include <vtkDistanceWidget.h>
#include <vtkInteractorStyleJoystickActor.h>
#include <vtkInteractorStyleJoystickCamera.h>
#include <vtkInteractorStyleSwitch.h>
#include <vtkInteractorStyleTrackballActor.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkSmartPointer.h>


/**
 * @brief vtkInteractorStyleSwitchを再実装したもの。
 * Interactorはマウス操作をカメラ移動やアクター操作に変換するもの。
 * マウスを動かした途端、カメラ移動とアクター操作が同時に起きるのは通常好ましくないため、これらInteractorはどちらか排他的にしか持てないが、vtkInteractorStyleSwitchはキー入力で自動的にInteractorを切り変えてくれる。
 */
class vsInteractorStyleSwitch:public vtkInteractorStyleSwitch {

private:
    vsTreeItemContainer* m_container;

public:

        static vsInteractorStyleSwitch* New();
        vtkTypeMacro(vsInteractorStyleSwitch, vtkInteractorStyleSwitch)


        vsInteractorStyleSwitch();
        virtual ~vsInteractorStyleSwitch();

        void OnChar();

        void setContainer(vsTreeItemContainer* container);

        void setUI(Ui_VadSim *___ui);



        void setSTLManager(vsSTLManager* stlManager) {

            reinterpret_cast<vsActorRelocateInteractor*>(this->TrackballActor)->setSTLManager(stlManager);

        }



        /**
         * @brief setDistRepresetation 距離計測機能をカメラ操作Interactorにセット
         * @param _rep　セットする距離測定機能クラスのインスタンスへのポインタ
         */
        void setDistRepresetation(vtkDistanceRepresentation *_rep);



        /**
         * @brief setDistRepresetation 角度計測機能をカメラ操作Interactorにセット
         * @param _rep　セットする角度測定機能クラスのインスタンスへのポインタ
         */
        void setAngleRepresetation(vtkAngleRepresentation *_rep);

        /**
         * @brief conveyTransform　複数パーツSTLにおいて、親パーツへの操作を子パーツへ伝える計算をさせる
         * @param actor　親パーツ
         */
        void conveyTransform(vtkProp* actor) {

            reinterpret_cast<vsActorRelocateInteractor*>(this->TrackballActor)->conveyTransform(actor);

        }

        /**
         * @brief getInteractorMove マウスでアクターを操作するInteractorを返す
         * @return　マウスでアクターを操作するInteractor
         */
        vsActorRelocateInteractor* getInteractorMove() {

            return reinterpret_cast<vsActorRelocateInteractor*>(this->TrackballActor);

        }
        void setCameraMode();


        void setMoveMode();

protected:
        void setModeName(QString str);
};



#endif // MYINTERACTORSTYLESWITCH_H
