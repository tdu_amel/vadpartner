#include "vsregiongrowingchecker.h"
#include <qdebug.h>

vsRegionGrowingChecker::vsRegionGrowingChecker()
{
}


vsRegionGrowingChecker::~vsRegionGrowingChecker() {



}



void vsRegionGrowingChecker::initDstImg(vtkSmartPointer<vtkImageData>& dstImage) {

/*
    int ext[6];
    int inIncX,inIncY,inIncZ;
    int outIncX,outIncY,outIncZ;
    dstImg->GetExtent(ext);
    m_Img->GetContinuousIncrements(ext, inIncX, inIncY, inIncZ);
    dstImg->GetContinuousIncrements(ext, outIncX, outIncY, outIncZ);


    char* dst_pixel = static_cast<char*>(dstImage->GetScalarPointer());

    for( int z = 0; z < maxZ;++z) {

        for( int y = 0; y < maxY;++y) {

            for( int x = 0; x < maxX;++x) {

                *dst_pixel = 0;
                ++src_pixel;
                ++dst_pixel;
            }
            src_pixel += inIncY;
            dst_pixel += outIncY;
        }

        src_pixel += inIncZ;
        dst_pixel += outIncZ;
    }
*/
}



bool vsRegionGrowingChecker::isInImage(SeedPoint& p,int extent[6]) {

    return p.x >= extent[0] || p.x < extent[1] || p.y >= extent[2] || p.y < extent[3] || p.z >= extent[4] || p.z < extent[5];

}



bool vsRegionGrowingChecker::ckeck(SeedPoint& p,SeedCheckDir& dir,vtkSmartPointer<vtkImageData> &dstImage,vtkSmartPointer<vtkImageData> &srcImage) {

/*
    int extent[6];
    srcImage->GetExtent(extent);

    if( !isInImage(p,extent) ) {

        vMsg("(%d %d %d) is invalid position. Extent is %d %d %d",p.x,p.y,p.z,extent[1],extent[3],extent[5]);
        return false;

    }

    short* dst_pixel = static_cast<short*>(dstImage->GetScalarPointer(p.x,p.y,p.z));
    char* src_pixel = static_cast<char*>(srcImage->GetScalarPointer(p.x,p.y,p.z));


    //今回はCT値が40[HU]より大きいピクセルを対象に
    if( *src_pixel > 40 ) {

        if( *dst_pixel == -1 ) {

            *dst_pixel = m_labelTable.size();

        } else {

            char minId = 128;
            std::vector<char> replacedLabelID;
            for( unsigned int newidx = 0; newidx < checkDirs.size(); ++newidx ) {

                SeedPoint checkPos = p + checkDirs[newidx];

                char label = static_cast<char*>(dstImage->GetScalarPointer(checkPos.x,checkPos.y,checkPos.z));
                if( label != -1 && label < minId ) {

                    if( minId != 128) {
                        replacedLabelID.push_back(minId);
                    }

                    minId = label;

                }

            }


            *dst_pixel = minId;


            for( unsigned int newidx = 0; newidx < replacedLabelID.size(); ++newidx ) {

                m_labelTable[ replacedLabelID[newidx] ] = minId;

            }


        }

        return true;

    }

*/
    return false;


}


void vsRegionGrowingChecker::replaceLabel(vtkSmartPointer<vtkImageData> &dstImage) {
/*
    int ext[6];
    int outIncX,outIncY,outIncZ;
    dstImg->GetExtent(ext);
    dstImg->GetContinuousIncrements(ext, outIncX, outIncY, outIncZ);


    char* dst_pixel = static_cast<char*>(dstImage->GetScalarPointer());

    for( int z = 0; z < maxZ; ++z ) {

        for( int y = 0; y < maxY; ++y ) {

            for( int x = 0; x < maxX; ++x ) {

                for( unsigned int newidx = 0; newidx <m_labelTable.size(); ++newidx ) {

                    if( *dst_pixel == newidx ) {
                        *dst_pixel = m_labelTable[newidx];
                    }

                }

                ++dst_pixel;

            }

            dst_pixel += outIncY;

        }

        dst_pixel += outIncZ;

    }
*/
}
