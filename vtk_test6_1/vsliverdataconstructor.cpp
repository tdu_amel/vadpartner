﻿#include "vsliverdataconstructor.h"
#include <vtkMedicalImageReader2.h>
#include <vtkMedicalImageProperties.h>
#include <vtkStringArray.h>
#include <vtkImageMedian3D.h>
#include <vtkImageData.h>
#include <vtkImageThreshold.h>
#include "vshistgramemphasize.h"
#include <vtkImageLaplacian.h>
#include <vtkImageWeightedSum.h>
#include <vtkMarchingCubes.h>
#include <vtkProperty.h>
#include <vtkPolyDataMapper.h>


vsLiverDataConstructor::vsLiverDataConstructor()
{

}



void vsLiverDataConstructor::constructImageData(vtkSmartPointer<vtkImageData>& dstImg,vtkImageData *srcImg,double w,double l) {

//liver

          int lower = l - w/2;
          int upper = l + w/2;

          //ノイズ除去のメディアンフィルタ
          vtkSmartPointer<vtkImageData> volume =
            vtkSmartPointer<vtkImageData>::New();
          volume->DeepCopy(srcImg);

          vtkSmartPointer<vtkImageMedian3D> medianFilter =
            vtkSmartPointer<vtkImageMedian3D>::New();
          medianFilter->SetInputDataObject(volume);
          medianFilter->SetKernelSize(3,3,1);
          medianFilter->Update();

          //lowerとupperの値をはずれた画素値は-2048に置き換え
          vtkSmartPointer<vtkImageThreshold> imageThreshold_liver =
            vtkSmartPointer<vtkImageThreshold>::New();
          imageThreshold_liver->SetInputDataObject(0,medianFilter->GetOutput());

          imageThreshold_liver->ThresholdBetween(lower,upper);
          imageThreshold_liver->ReplaceInOff();
          imageThreshold_liver->ReplaceOutOn();
          imageThreshold_liver->SetOutValue(-2048);
          imageThreshold_liver->Update();


          //ラぷラシアンフィルタでエッジ抽出(エッジ強調に使うため)
          vtkSmartPointer<vtkImageLaplacian> LaplacianFilter =
            vtkSmartPointer<vtkImageLaplacian>::New();

          LaplacianFilter->SetInputConnection(0,imageThreshold_liver->GetOutputPort());
          LaplacianFilter->Update();


          //元画像の重み0.8，エッジ抽出画像の重み0.2で二つの画像を重ね合わせ(エッジ強調)
          vtkSmartPointer<vtkImageWeightedSum> sumFilter =
            vtkSmartPointer<vtkImageWeightedSum>::New();
          sumFilter->SetWeight(0,.8);
          sumFilter->SetWeight(1,.2);
          sumFilter->AddInputConnection(imageThreshold_liver->GetOutputPort());
          sumFilter->AddInputConnection(LaplacianFilter->GetOutputPort());
          sumFilter->Update();


          //もう一度メディアンフィルタでノイズ除去
          vtkSmartPointer<vtkImageMedian3D> medianFilterFinal =
            vtkSmartPointer<vtkImageMedian3D>::New();
          medianFilterFinal->SetInputConnection(sumFilter->GetOutputPort());
          medianFilterFinal->SetKernelSize(3,3,1);
          medianFilterFinal->Update();


          vtkSmartPointer<vtkImageThreshold> imageThreshold_bone =
            vtkSmartPointer<vtkImageThreshold>::New();
          imageThreshold_bone->SetInputDataObject(medianFilterFinal->GetOutput());


          imageThreshold_bone->ThresholdBetween(-200, 200);
          imageThreshold_bone->ReplaceOutOn();
          imageThreshold_bone->SetOutValue(-1024);
          imageThreshold_bone->Update();

          //ヒストグラム強調　(全体としてヒストグラムを6倍に)
          vsHistgramEmphasize emph;

          //int width = (upper+lower);
          int width = (upper-lower);
          width = 20;

          int wcenter = width/2;
          wcenter = 60;

          double coef = 2.0;

          int o_width = width * coef;

          int o_lower = wcenter-o_width/2;//CT値の強調後の最低値
          int o_upper = wcenter+o_width/2;//CT値の強調後の最高値
          emph.setInputInfo(lower,upper);
          emph.setOutputInfo(o_lower,o_upper);
          vtkSmartPointer<vtkImageData> liverImg = vtkSmartPointer<vtkImageData>::New();
          emph.execute( liverImg,imageThreshold_bone->GetOutput() );

          int axis_coronal[] = {1,0,0};
          int axis_axial[] = {0,0,-1};
          int axis_saggital[] = {0,1,0};


          //saveDICOM(QString::fromLocal8Bit("Liver_"),QString::fromLocal8Bit("Coronal_Th"),liverImg,-1024,1024,-90,axis_coronal);
          //saveDICOM(QString::fromLocal8Bit("Liver_"),QString::fromLocal8Bit("Axial_Th"),liverImg,-1024,1024,0,axis_axial);
          //saveDICOM(QString::fromLocal8Bit("Liver_"),QString::fromLocal8Bit("Saggital_Th"),liverImg,lower,upper,90,axis_saggital);


          dstImg = liverImg;


}

void vsLiverDataConstructor::constructModel(vtkSmartPointer<vtkActor> &actor_liver,vtkImageData *srcImg,double w,double l)
{

    vtkSmartPointer<vtkImageData> dstImg;

    //肝臓の画像を抽出
    constructImageData(dstImg,srcImg,w,l);

       //「強調後の肝臓の最低CT値」以上の画素値を持つボクセルをMCで抜き出しで面に
       vtkSmartPointer<vtkMarchingCubes> surface_liver =
           vtkSmartPointer<vtkMarchingCubes>::New();


       surface_liver->SetInputData(srcImg);
       surface_liver->ComputeNormalsOn();
       surface_liver->SetValue(0, -2);
       //int lower = -45;
       //int upper = 94;

       vtkSmartPointer<vtkPolyDataMapper> mapper_liver =
           vtkSmartPointer<vtkPolyDataMapper>::New();
       mapper_liver->SetInputConnection(surface_liver->GetOutputPort());
       mapper_liver->ScalarVisibilityOff();


       //actorの作成
        actor_liver = vtkSmartPointer<vtkActor>::New();
        actor_liver->SetMapper(mapper_liver);
        actor_liver->GetProperty()->SetColor( 160/256.0, 100/256.0, 70/260.0 );//色は茶色
        actor_liver->GetProperty()->SetOpacity( 1 );

        //マウスで選択できないようにする
        actor_liver->SetPickable(false);
}
