﻿#ifndef VSVTKINITIALIZER_H
#define VSVTKINITIALIZER_H

#include <vtkAutoInit.h>
#include <vtkRenderingOpenGLConfigure.h>

/*
ex)if vtk version is 4.0.2 then
#define 	VTK_MAJOR_VERSION   4
#define 	VTK_MINOR_VERSION   0
#define 	VTK_BUILD_VERSION   2
*/

VTK_MODULE_INIT(vtkInteractionStyle)
VTK_MODULE_INIT(vtkRenderingFreeType)
#if VTK_MAJOR_VERSION >= 7
        VTK_MODULE_INIT(vtkRenderingOpenGL2)
 #define vtkRenderingVolume_AUTOINIT 1(vtkRenderingVolumeOpenGL2)
#elif VTK_MAJOR_VERSION >= 6
        VTK_MODULE_INIT(vtkRenderingOpenGL)
        VTK_MODULE_INIT(vtkRenderingFreeTypeOpenGL)
#define vtkRenderingVolume_AUTOINIT 1(vtkRenderingVolumeOpenGL)
#endif


#endif // VSVTKINITIALIZER_H
