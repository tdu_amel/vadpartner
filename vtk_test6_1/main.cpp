﻿#include <QApplication>
#include "vsvtkinitializer.h"
#include "mainwindow.h"
#include <QTextCodec>
#include <QtGlobal>
#include <QTranslator>
#include <QDir>
#include "vmsg.h"


/**
 * @mainpage VadSimについて
 * 
 * 
 * @section main_sec VadSimの目的
 * 人工心臓の埋め込みのシミュレーションを視覚的(3D表示による)，定量的(計測機能による)に行うことを目的としています．
 * 
 * @section main_sec VadSimのプログラムの流れ
 * 基本的な機能はMainWindow.cppから呼び出されます．
 * 
 * @section main_sec_howtobuild VadSimのビルド方法
 * VadSimではGUI機能にQt，画像処理機能にVTK，XMLファイルの読み込みにTinyXML，その他の処理にboost，というライブラリを用いており，まず先にそれらのライブラリを構築することが必要になる．
 * @ref build を参照
 * それらの環境が構築された場合，本プログラムのソースコード一式中にQtProjectMakerというプロジェクトを利用してVadSimビルドプロジェクトを作成する．
 * なお，実行形式はWindows版しかないため，それ以外の環境においては，付属ソースコードで自分でQtProjectMakerを作ってください．
 * また，実際に行っている処理は，ソースコードのプロジェクトへの追加，ライブラリの自動リンク設定，VTKのヘッダファイルの一か所へのコピー(ビルド時にinstall未実行の場合の対策)なので，
 * それらを自分でできるという人は不要です．
 *
 * @page build VadSimのビルド方法
 * @section sec_qt Qt環境の構築
 * Qtの公式ページへ行き，自分の環境にあったビルド済みキットをダウンロードし，開発環境QtCreatorとともにインストールすることが最も楽です．
 * Qt 4.8.2とQt 5.7.2(どちらもWindowsかつx64)によるVadSimのビルドを確認済みです．
 * それ以外のバージョンのQtを使用する場合，VadSimのビルド時に細かいソースコードの修正が必要になることがあります．
 * VadSimは大きなデータを扱う都合上，x64でないとDICOMなどの読み込み時にメモリの確保エラーが起きる可能性があります．
 * x64のプログラムを出力できるQtキットおよびコンパイラを設定してVadSimをビルドしてください．(Qt 4系の公式配布のビルド済みキットには64bitのプログラムを出力できるものはありません．自分で作る必要があります)
 * ビルド済みのQtキットを利用する場合では，Qtの機能はShared Link Library（WindowsではDLL）の形で提供されます．
 * プログラム内にQt機能を組み込む場合は，everywhereと書かれたソースコードをダウンロードをし，
 * 適宜設定をした上で，自分でビルドをする必要があります(ただし，この場合，QtのライセンスはLGPLではなくGPLが適用される)．
 * 詳細はhttp://doc.qt.io/qt-5/build-sources.htmlなどを参考にしてください．
 * ビルドは半日かそれ以上かかります．
 *
 * @section sec_vtk VTKのビルド方法
 * VTK内部からQtのAPIを呼び出しているため，先にQt環境の構築が必要になります．
 * VTKはソースコードのみが配布されており，各環境でライブラリをビルドするためのプロジェクトファイルや
 * Makefileを作成するにはCMAKEを使います．
 * VTKも初期設定ではShared Link Libraryの形で出力されるので，VadSim内部にVTK機能を含める場合は適宜CMAKE設定の変更やマクロ宣言が必要となります．
 * （CMAKEのBUILD_SHARED_LIBSの設定を切っただけではstatic linkできないっぽい？）
 * Version.7における設定の初期設定からの変更例を示します．cmakeのバージョンは3.7.0です．
 * cmake-guiがあるならそちらを使った方が楽です．
 * ソースディレクトリ（where is the soure code）にVTKのフォルダを指定し，
 * 出力先ディレクトリ(where to build binaries)にVTKのライブラリの出力先にしたいディレクトリを指定します
 * 【設定例】
 * CMAKE_INSTALL_PREFIX　を　出力先ディレクトリ(where to build binaries)　に変更
 *　VTK_BUILD_TESTING を　オフ　に
 * CMAKE_CONFIGURATION_TYPES　を　Debug;Relase　に
 * VTK_ALL_NEW_OBJECT_FACTORY　を　オン　に
 * VTK_Group_Imaging　を　オン　に
 *　Module_vtkGUISupportQt　と　Module_vtkGUISupportQtOpenGL　を　オン　に
 * VTK_ALL_NEW_OBJECT_FACTORY　を　オン　に
 *　VTK_MAKE_INSTANTIATORS　を　オンに
 * 他は初期設定のまま変えません．Module_vtkRenderingQtやVTK_Group_Qtなどはオフのままです．
 * ここでconfigureを実行します．（cmake-guiならconfigureボタン）
 * 出力したいプロジェクトの種類を聞かれると思うので，Visual Studio 14.0 2015 x64（Windowsの場合）やg++ Makefileなどを指定します．
 * Visual Studioの場合はx64がついているものを選んでください．
 * すると，QT_QMAKE_EXECUTABLE　VTK_QT_VERSION　Qt5_DIR（またはQt4_DIR）などが追加されます
 * VTK_QT_VERSIONには使用するキットのメジャーバージョン(Qt4系なら4，Qt5系なら5)を選択し，
 * QT_QMAKE_EXECUTABLEには使用するキットのqmake（qmake.exe）のパスを選択します
 * Qt5_DIR（またはQt4_DIR）には　（使用キットのディレクトリ）/lib/cmake のパスを選択します．
 * 再びconfigureを実行し，エラー項目(赤い項目)がすべて消えたらgenerateを実行してVisualStudioのプロジェクトまたはその他コンパイラのMakefileなどを出力します
 *　あとはそのプロジェクトファイルやMakefileを使ってビルドを実行するだけです．
 * Visual Stidioの場合なら，cmake-guiのOpen Projectを押してプロジェクトファイルを開くか，出力ディレクトリのvtk.slnを自分で開きます．
 * Visual Stidioのメニューの「ビルド」内に「バッチビルド」があるならそれを選択し，
 * プロジェクト名がINSTALL　かつ，ビルド構成が　DebugとReleaseのもの
 * の合計2つにチェックを入れてビルドを実行します．
 * バッチビルドがないときは「ソリューションのビルド」を選択してください．
 * ビルドは早くとも半日程度かかります．
 * vtk.slnにあるファイルにincludeというフォルダがない場合は，インストール処理が行われていないので，
 * cmakeを使ったときにソースディレクトリ（where is the soure code）に指定したフォルダ内にある拡張子が.hまたは.txxのファイルをすべて
 * includeフォルダ(ないときは自分で作る)にコピーしてください．
 *
 * また，ビルドをしただけではプロジェクトで使用する設定にはなっていないので，VadSim用プロジェクトを作成し，.proファイルに
 * CONFIG(debug,debug|release){
 *        LIBS += -L(Debug版ビルドのライブラリのあるディレクトリのパス) -l(リンクするライブラリファイルの拡張子を除いたファイル名1) -l(リンクするライブラリファイルの拡張子を除いたファイル名2) -l(リンクするライブラリファイルの拡張子を除いたファイル名3)　-l(リンクするライブラリファイルの拡張子を除いたファイル名2) -l(リンクするライブラリファイルの拡張子を除いたファイル名n)
 *          #こんな感じ⇒LIBS += -LC:/VTK/vtk7.0_build/qt4_static/lib/debug -lvtkalglib-7.1
 *          #見づらかったら一行に1ライブラリ書くといいかもしれません
 * }
 * CONFIG(release,debug|release){
 *        LIBS += -L(Release版ビルドのライブラリのあるディレクトリのパス> -l(リンクするライブラリファイルの拡張子を除いたファイル名)　 -l(リンクするライブラリファイルの拡張子を除いたファイル名2) -l(リンクするライブラリファイルの拡張子を除いたファイル名3)　-l(リンクするライブラリファイルの拡張子を除いたファイル名2) -l(リンクするライブラリファイルの拡張子を除いたファイル名n)
 * }
 * などのように使用するすべてのライブラリについてリンク設定を行います
 * さらに，
 * INCLUDEPATH += $$quote(C:/VTK/vtk7.0_build/qt4_static/include)
 * などのようにincludeフォルダもパスに追加します．
 * 学部生のために，.hなどのコピー処理，includeパスの追加，ライブラリのリンク設定はQtProjectMaker.exeというプログラムを使うことで，
 * プロジェクトの作成も含めて自動で実行されるようにしてあります．
 *
 *
 * @section sec_tinyxml TinyXMLのビルド方法
 * TinyXMLはソースコードのみが配布されており，各環境でライブラリをビルドするためのプロジェクトファイルや
 * MakeFileを作成するにはCMAKEを使います．
 *
 * @section sec_boost boostのビルド方法
 * VadSimでは（実は畏れ多くも）boostのスマートポインタしか使っていないので，ソースコードをダウンロードし，プロジェクトファイルにパスを追加するだけで使用できます．
 * Qtのプロジェクトファイル(.proファイル)に
 * INCLUDEPATH += $$quote(C:/boost_1_61_0)
 * などと書き足します
 * 学部生のために，この追加処理はQtProjectMaker.exeというプログラムを使うことでで自動で実行されるようにしてあります．
*/










int main( int argc, char** argv )
{
  // QT Stuff

#ifdef Q_OS_WIN32
    _CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
    _CrtSetReportMode( _CRT_ERROR, _CRTDBG_MODE_DEBUG );
#endif

  QApplication app( argc, argv );
#if QT_VERSION >= 0x050000
  //Qt5系以降でのTextCodecの設定
  QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));

#elif QT_VERSION >= 0x040800
  //Qt4.8系以降でのTextCodecの設定
  QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
  QTextCodec::setCodecForTr( QTextCodec::codecForName("UTF-8") );
#else
    set your QTexCodec
#endif




    QTranslator translator;
    if( translator.load(QString::fromLocal8Bit("vadsimtr_ja"), QCoreApplication::applicationDirPath() ) ) {
        app.installTranslator(&translator);
    }
    else {
        QString path = QCoreApplication::applicationDirPath() + QString::fromLocal8Bit("/vadsimtr_ja") ;
        vWarning("translator not found in %s",qPrintable(path));
    }


  MainWindow window;
  window.show();

  return app.exec();
}
