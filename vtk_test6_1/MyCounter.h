﻿#ifndef MYCOUNTER_H
#define MYCOUNTER_H
#include <qglobal.h>
/*
 *Windowsに依存しているので注意
*/
#ifdef Q_OS_WIN
#include <windows.h>
#pragma comment(lib, "winmm.lib")
#endif

#include <mmsystem.h>
#include <list>
#include <fstream>
#include <QString>

using namespace std;

#ifdef Q_OS_WIN
class MyCounter
{
private:

    LARGE_INTEGER nFreq;	///<周波数
    LARGE_INTEGER nBefore;	///<開始時のカウンタ
    LARGE_INTEGER nAfter;	///<終了時のカウンタ
    DWORD dwTime;

    std::ofstream fout;
    bool m_finish; ///<計測終了したらtrueに
    QString datestr;
    QString timestr;

public:
    MyCounter(const char* fname):m_finish(false) {

        fout.open( fname, ios::app );
        //変数の初期化
        memset(&nFreq,   0x00, sizeof nFreq);
        memset(&nBefore, 0x00, sizeof nBefore);
        memset(&nAfter,  0x00, sizeof nAfter);
        dwTime = 0;


    }

    inline void begin() {

        //計測開始！
        QDateTime dateTime = QDateTime::currentDateTime();
        datestr = dateTime.date().toString("yyyy/MM/dd");
        timestr = dateTime.time().toString("hh:mm:ss");

        m_finish = false;
        QueryPerformanceFrequency(&nFreq);
        QueryPerformanceCounter(&nBefore);

    }

    inline void end() {

        //計測終了！
        QueryPerformanceCounter(&nAfter);
        m_finish = true;

    }

    virtual ~MyCounter() {

        if( m_finish ) {
            //出力する
            dwTime = (DWORD)((nAfter.QuadPart - nBefore.QuadPart) * 1000 / nFreq.QuadPart);
            fout<<"["<<datestr.toStdString().c_str() << " " << timestr.toStdString().c_str() <<"]"<<"time:"<<dwTime<<"[ms]"<<std::endl;
        }
    }
};

#else

class MyCounter
{
private:
    QTimer _timer;
public:
    MyCounter(const char* fname){}

    inline void begin() {}

    inline void end() {}

    virtual ~MyCounter() {}
};


#endif



#endif // MYCOUNTER_H
