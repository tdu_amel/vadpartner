﻿#include "vslogger.h"
#include <QCoreApplication>
#include <QDebug>

const QString vsLogger::_log_file_name = "vslog.txt";///<　ログファイルの名前　「(実行ファイルのあるパス)/vslog.txt)」で固定


/**
 * @brief getInstance ロガーにアクセスするための関数
 * @return　作成されたもしくは作成済みのロガーのインスタンス
 */
vsLogger* vsLogger::getInstance()
{
    //既にインスタンスが作成されているか？
    if (!_inst.get())
    {
        //作成されていないので，インスタンスを作成　＆　ログファイルも作成
        _inst.reset(new vsLogger);

        QString dir_path = QCoreApplication::applicationDirPath();
        QString log_file_full_path = dir_path + "/" +_log_file_name;
        _file.setFileName(log_file_full_path);

        if (!_file.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            //ファイルのオープンに失敗
            qWarning() << "Can't open Log File:" << log_file_full_path
                       << " in " << __FILE__ << "(" << __LINE__ << ")";

            //でもとりあえずロガークラスのインスタンスは返しておく…
            return _inst.get();
        }

        _stream.setDevice(&_file);

    }

    //先程作成したor作成済みのインスタンスを返す
    return _inst.get();
}


void vsLogger::write_log_header(QString& action_name, QString& action_target, QString& target_count,
                          QString& file, QString& function, const int line)
{
    _stream.status();

    QTime time;

    _stream << time << file << "(" << line <<")"
            << " "
            << function << log_level << "%" <<
}

boost::shared_ptr<vsLogger> vsLogger::_inst = 0;///< 実インスタンス　Singletonパターンにより，実インスタンスは基本的に一つしか作られない
QFile vsLogger::_file;
QTextStream vsLogger::_stream;

