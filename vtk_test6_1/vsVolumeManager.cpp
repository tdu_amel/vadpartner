﻿#include "vsVolumeManager.h"

#include <QDebug>
#include <vtkColorTransferFunction.h>
#include <vtkDICOMImageReader.h>
#include <vtkFixedPointVolumeRayCastMapper.h>
#include <vtkGPUVolumeRayCastMapper.h>
#include <vtkImageThreshold.h>
#include <vtkPiecewiseFunction.h>
#include <vtkRenderer.h>
#include <vtkVolume.h>
#include <vtkVolumeProperty.h>

#include "vMsg.h"

vsVolumeManager::vsVolumeManager()
{
}


bool vsVolumeManager::update( int ct_min, int ct_max, vtkRenderer* renderer ) {

    if( m_reader == 0) {
        vMsg("error! (vsVolumeManager::update)vtkReader is Null.");
        return false;
    }

    if( m_volume != 0 ) {
        renderer->RemoveVolume(m_volume.GetPointer());
    }

    vtkSmartPointer<vtkImageThreshold> imageThreshold = vtkSmartPointer<vtkImageThreshold>::New();
    imageThreshold->SetInputConnection(m_reader->GetOutputPort());


    imageThreshold->ThresholdBetween(ct_min, ct_max);
    //vMsg("min max = (%d %d)",ct_min,ct_max);
    imageThreshold->ReplaceInOn();
    imageThreshold->SetInValue(0);
    imageThreshold->Update();

     //ボリュームレンダリング
     mapper = vtkSmartPointer<vtkFixedPointVolumeRayCastMapper>::New();
     opacityFunc = vtkSmartPointer<vtkPiecewiseFunction>::New();
     property = vtkSmartPointer<vtkVolumeProperty>::New();
     m_volume = vtkSmartPointer<vtkVolume>::New();
     colorFunc = vtkColorTransferFunction::New();

     vMsg("3\n");
     property->ShadeOff();
     property->SetInterpolationType(VTK_LINEAR_INTERPOLATION);
    // property->SetInterpolationTypeToLinear();


     vMsg("5\n");
         colorFunc->AddRGBSegment(0.0, 0.6, 0.6, 0.6, 255.0, 0.6, 0.6, 0.6 );

         opacityFunc->AddSegment(  0, 0.0, 10, 0.0 );
         opacityFunc->AddSegment( 10, 0.0, 1000, 0.7 );



     mapper->SetInputConnection( imageThreshold->GetOutputPort() );
 vMsg("3\n");
     property->SetScalarOpacity(opacityFunc); // composite first.
     property->SetColor( colorFunc );

 vMsg("4\n");
     // connect up the volume to the property and the mapper
     m_volume->SetProperty( property );
     m_volume->SetMapper( mapper );
     m_volume->SetPickable(false);



 vMsg("6\n");
     mapper->SetBlendModeToComposite();

     vMsg("7");


     renderer->AddVolume( m_volume.GetPointer() );
    vMsg("reconstruct %d-%d",ct_min,ct_max);

    return true;
}

void vsVolumeManager::setCTMinMax(int *min, int *max) {

    *min = m_ct_min;
    *max = m_ct_max;

}

void vsVolumeManager::setReader( vtkSmartPointer<vtkDICOMImageReader>& reader ) {

    m_reader = reader;

}

vtkVolume* vsVolumeManager::getVolume(  ) {

    return m_volume.Get();

}
