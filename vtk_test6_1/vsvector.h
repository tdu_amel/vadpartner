﻿#ifndef VSVECTOR_H
#define VSVECTOR_H

struct vsVector {
    double x,y,z;
    vsVector()
        :x(0),y(0),z(0){

    }

    vsVector(const double _x,const double _y,const double _z)
        :x(_x),y(_y),z(_z){

    }
};

#endif // VSVECTOR_H
