﻿#include "vsbonedataconstructor.h"
#include <vtkMedicalImageReader2.h>
#include <vtkMedicalImageProperties.h>
#include <vtkStringArray.h>
#include <vtkImageMedian3D.h>
#include <vtkImageData.h>
#include <vtkImageThreshold.h>
#include "vshistgramemphasize.h"
#include <vtkImageLaplacian.h>
#include <vtkImageWeightedSum.h>
#include <vtkMarchingCubes.h>
#include <vtkProperty.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <QSharedPointer>
#include <QProgressDialog>

vsBoneDataConstructor::vsBoneDataConstructor()
{

}


template<class process>
struct vsBoneConstructionProgress {

    static QSharedPointer<QProgressDialog> m_progressWindow;

    static void start(QString& caption) {
        m_progressWindow = QSharedPointer<QProgressDialog>(new QProgressDialog("", "", 0, 100, 0) );
        m_progressWindow->setWindowTitle(caption);
        m_progressWindow->show();
        m_progressWindow->setCancelButton(0);

    }

    static void end() {
        if( m_progressWindow.data() ) {
            m_progressWindow->close();
            m_progressWindow.clear();
        }
    }

    static void reportImageDataProgress( vtkObject* caller,
                                long unsigned int vtkNotUsed(eventId),
                                void* vtkNotUsed(clientData),
                                void* vtkNotUsed(callData)) {


        if( m_progressWindow.data() == 0 ) {
            return;
        }

        process* _process = reinterpret_cast<process*>(caller);
        double progress = _process->GetProgress();

        m_progressWindow->setValue(progress*100);
        QCoreApplication::processEvents();

        vMsg("%lf",progress);

    }

    static void reportMarchingCubeProgress( vtkObject* caller,
                                long unsigned int vtkNotUsed(eventId),
                                void* vtkNotUsed(clientData),
                                void* vtkNotUsed(callData)) {


        if( m_progressWindow.data() == 0 ) {
            return;
        }

        process* _process = reinterpret_cast<process*>(caller);
        double progress = _process->GetProgress();

        m_progressWindow->setValue(progress*100);
        QCoreApplication::processEvents();

        vMsg("%lf",progress);

    }

    static void reportMapperProgress( vtkObject* caller,
                                long unsigned int vtkNotUsed(eventId),
                                void* vtkNotUsed(clientData),
                                void* vtkNotUsed(callData)) {


        if( m_progressWindow.data() == 0 ) {
            return;
        }

        process* _process = reinterpret_cast<process*>(caller);
        double progress = _process->GetProgress();

        m_progressWindow->setValue(progress*100);
        QCoreApplication::processEvents();

        vMsg("%lf",progress);

    }

};

template<class T>
QSharedPointer<QProgressDialog> vsBoneConstructionProgress<T>::m_progressWindow;



void vsBoneDataConstructor::constructImageData(vtkSmartPointer<vtkImageData>& dstImg,vtkImageData *srcImg) {

        //bone face model
        vtkSmartPointer<vtkImageData> volume =
          vtkSmartPointer<vtkImageData>::New();
        volume->DeepCopy(srcImg);

        vtkSmartPointer<vtkImageThreshold> imageThreshold_bone =
          vtkSmartPointer<vtkImageThreshold>::New();
        imageThreshold_bone->SetInputDataObject(volume);
        short lower = 92;
        short upper = 1520;

        imageThreshold_bone->ThresholdBetween(lower, upper);
        imageThreshold_bone->ReplaceInOn();
        imageThreshold_bone->SetInValue(1);
        imageThreshold_bone->ReplaceOutOn();
        imageThreshold_bone->SetOutValue(0);
        imageThreshold_bone->Update();

        int axis_axial[] = {0,0,-1};
        int axis_coronal[] = {1,0,0};
        //saveDICOM(QString::fromLocal8Bit("Bone"),QString::fromLocal8Bit("Axial_Th"),imageThreshold_bone->GetOutput(),lower,upper,0,axis_axial);
        //saveDICOM(QString::fromLocal8Bit("Bone"),QString::fromLocal8Bit("Coronal_Th"),imageThreshold_bone->GetOutput(),lower,upper,-90,axis_coronal);


        dstImg = imageThreshold_bone->GetOutput();


}



void vsBoneDataConstructor::constructModel(vtkSmartPointer<vtkActor> &actor, vtkImageData *srcImg) {


    vtkSmartPointer<vtkImageData> dstImg;

    //の画像を抽出
    constructImageData(dstImg,srcImg);

    vtkSmartPointer<vtkMarchingCubes> surface_bone =
        vtkSmartPointer<vtkMarchingCubes>::New();


    surface_bone->SetInputData(dstImg);
    surface_bone->ComputeNormalsOn();
    surface_bone->SetValue(0, 1);


    vtkSmartPointer<vtkPolyDataMapper> mapper_bone =
        vtkSmartPointer<vtkPolyDataMapper>::New();
    mapper_bone->SetInputConnection(surface_bone->GetOutputPort());
    mapper_bone->ScalarVisibilityOff();

    //actor
     actor = vtkSmartPointer<vtkActor>::New();
     actor->SetMapper(mapper_bone);
     actor->GetProperty()->SetColor( 1, 1, 1 );
     actor->GetProperty()->SetOpacity( 1.0 );

     //選択できないようにする
     actor->SetPickable(false);

}
