﻿#include "vsheartdataconstructor.h"
#include <vtkMedicalImageReader2.h>
#include <vtkMedicalImageProperties.h>
#include <vtkStringArray.h>
#include <vtkImageMedian3D.h>
#include <vtkImageData.h>
#include <vtkImageThreshold.h>
#include "vshistgramemphasize.h"
#include <vtkImageLaplacian.h>
#include <vtkImageWeightedSum.h>
#include <vtkMarchingCubes.h>
#include <vtkProperty.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <QSharedPointer>
#include <QProgressDialog>
#include "vsBoneDataConstructor.h"
vsHeartDataConstructor::vsHeartDataConstructor()
{

}






template<class process>
struct vsHeartConstructionProgress {

    static QSharedPointer<QProgressDialog> m_progressWindow;

    static void start(QString& caption) {
        m_progressWindow = QSharedPointer<QProgressDialog>(new QProgressDialog("", "", 0, 100, 0) );
        m_progressWindow->setWindowTitle(caption);
        m_progressWindow->show();
        m_progressWindow->setCancelButton(0);

    }

    static void end() {
        if( m_progressWindow.data() ) {
            m_progressWindow->close();
            m_progressWindow.clear();
        }
    }

    static void reportImageDataProgress( vtkObject* caller,
                                long unsigned int vtkNotUsed(eventId),
                                void* vtkNotUsed(clientData),
                                void* vtkNotUsed(callData)) {


        if( m_progressWindow.data() == 0 ) {
            return;
        }

        process* _process = reinterpret_cast<process*>(caller);
        double progress = _process->GetProgress();

        m_progressWindow->setValue(progress*100);
        QCoreApplication::processEvents();

        vMsg("%lf",progress);

    }

    static void reportMarchingCubeProgress( vtkObject* caller,
                                long unsigned int vtkNotUsed(eventId),
                                void* vtkNotUsed(clientData),
                                void* vtkNotUsed(callData)) {


        if( m_progressWindow.data() == 0 ) {
            return;
        }

        process* _process = reinterpret_cast<process*>(caller);
        double progress = _process->GetProgress();

        m_progressWindow->setValue(progress*100);
        QCoreApplication::processEvents();

        vMsg("%lf",progress);

    }

    static void reportMapperProgress( vtkObject* caller,
                                long unsigned int vtkNotUsed(eventId),
                                void* vtkNotUsed(clientData),
                                void* vtkNotUsed(callData)) {


        if( m_progressWindow.data() == 0 ) {
            return;
        }

        process* _process = reinterpret_cast<process*>(caller);
        double progress = _process->GetProgress();

        m_progressWindow->setValue(progress*100);
        QCoreApplication::processEvents();

        vMsg("%lf",progress);

    }

};

template<class T>
QSharedPointer<QProgressDialog> vsHeartConstructionProgress<T>::m_progressWindow;



void vsHeartDataConstructor::constructImageData(vtkSmartPointer<vtkImageData>& dstImg,vtkImageData *srcImg) {

    int upper = 30;
    int lower = 10;
        //bone face model
        vtkSmartPointer<vtkImageData> volume =
          vtkSmartPointer<vtkImageData>::New();
        volume->DeepCopy(srcImg);
        vtkSmartPointer<vtkImageMedian3D> medianFilter =
          vtkSmartPointer<vtkImageMedian3D>::New();
        medianFilter->SetInputDataObject(volume);
        medianFilter->SetKernelSize(3,3,1);
        medianFilter->Update();

        vtkSmartPointer<vtkImageThreshold> imageThreshold_heart =
          vtkSmartPointer<vtkImageThreshold>::New();
        imageThreshold_heart->SetInputDataObject(0,medianFilter->GetOutput());

        imageThreshold_heart->ThresholdBetween(lower,upper);
        imageThreshold_heart->ReplaceInOff();
        imageThreshold_heart->ReplaceOutOn();
        imageThreshold_heart->SetOutValue(-2048);
        imageThreshold_heart->Update();

        vtkSmartPointer<vtkImageData> lapImage = vtkSmartPointer<vtkImageData>::New();
        lapImage->DeepCopy((imageThreshold_heart->GetOutput()));

        vtkSmartPointer<vtkImageLaplacian> LaplacianFilter =
          vtkSmartPointer<vtkImageLaplacian>::New();

        LaplacianFilter->SetInputDataObject(0,lapImage);
        LaplacianFilter->Update();



        vtkSmartPointer<vtkImageWeightedSum> sumFilter =
          vtkSmartPointer<vtkImageWeightedSum>::New();
        sumFilter->SetWeight(0,.7);
        sumFilter->SetWeight(1,.3);
        sumFilter->AddInputConnection(imageThreshold_heart->GetOutputPort());
        sumFilter->AddInputConnection(LaplacianFilter->GetOutputPort());
        sumFilter->Update();




        vtkSmartPointer<vtkImageMedian3D> medianFilterFinal =
          vtkSmartPointer<vtkImageMedian3D>::New();
        medianFilterFinal->SetInputConnection(sumFilter->GetOutputPort());
        medianFilterFinal->SetKernelSize(3,3,1);
        medianFilterFinal->Update();


        //ヒストグラム強調　(全体としてヒストグラムを6倍に)
        vsHistgramEmphasize emph;

        int width = (upper+lower);
        int wcenter = width/2;
        double coef = 16;

        int o_width = width * coef;

        int o_lower = wcenter-o_width/2;//CT値の強調後の最低値
        int o_upper = wcenter+o_width/2;//CT値の強調後の最高値
        emph.setInputInfo(lower,upper);
        emph.setOutputInfo(o_lower,o_upper);
        vtkSmartPointer<vtkImageData> heartImg = vtkSmartPointer<vtkImageData>::New();
        emph.execute( heartImg,medianFilterFinal->GetOutput() );


        vtkSmartPointer<vtkMarchingCubes> surface_heart =
            vtkSmartPointer<vtkMarchingCubes>::New();

        vtkSmartPointer<vtkImageThreshold> imageThreshold_heart2 =
          vtkSmartPointer<vtkImageThreshold>::New();
        imageThreshold_heart2->SetInputDataObject(0,heartImg);

        imageThreshold_heart2->ThresholdBetween(-200,150);
        imageThreshold_heart2->ReplaceInOff();
        imageThreshold_heart2->ReplaceOutOn();
        imageThreshold_heart2->SetOutValue(-2048);
        imageThreshold_heart2->Update();



        int axis_axial[] = {0,0,-1};
        int axis_coronal[] = {1,0,0};
        //saveDICOM(QString::fromLocal8Bit("Heart"),QString::fromLocal8Bit("Axial_Th"),imageThreshold_heart2->GetOutput(),o_lower,o_upper,0,axis_axial);
        //saveDICOM(QString::fromLocal8Bit("Heart"),QString::fromLocal8Bit("Coronal_Th"),imageThreshold_heart2->GetOutput(),o_lower,o_upper,-90,axis_coronal);


        dstImg = imageThreshold_heart2->GetOutput();


}



void vsHeartDataConstructor::constructModel(vtkSmartPointer<vtkActor> &actor, vtkImageData *srcImg) {


    vtkSmartPointer<vtkImageData> dstImg;

    //画像を抽出
    constructImageData(dstImg,srcImg);

    vtkSmartPointer<vtkMarchingCubes> surface_heart =
        vtkSmartPointer<vtkMarchingCubes>::New();


    surface_heart->SetInputData(dstImg);
    surface_heart->ComputeNormalsOn();
    surface_heart->SetValue(0, -200);

    vtkSmartPointer<vtkPolyDataMapper> mapper_heart =
        vtkSmartPointer<vtkPolyDataMapper>::New();
    mapper_heart->SetInputConnection(surface_heart->GetOutputPort());
    mapper_heart->ScalarVisibilityOff();

    //actor
     actor = vtkSmartPointer<vtkActor>::New();
     actor->SetMapper(mapper_heart);
     actor->GetProperty()->SetColor( 1, 0, 0 );
     actor->GetProperty()->SetOpacity( 0.2 );


     //選択できないようにする
     actor->SetPickable(false);
}
