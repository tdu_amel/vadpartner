﻿#ifndef DATAEXPORTER_H
#define DATAEXPORTER_H

#include <QDialog>
#include <QVector>
#include <QVariant>
#include <boost/shared_ptr.hpp>
#include "vsExportData.h"


namespace Ui {
class DataExporterSetting;
}

class vsDataExporterSetting : public QDialog
{
    Q_OBJECT

public:
    explicit vsDataExporterSetting(boost::shared_ptr<vsExportDataInfo>& info,QWidget *parent = 0);
    ~vsDataExporterSetting();
    void setExportDataInfo();
private slots:
    void on_pushButton_addItem_clicked();
    void on_pushButton_delete_clicked();
    void on_pushButton_OK_clicked();
    void on_pushButton_cancel_clicked();
    void dataTypechanged(int);
    void dataInfochanged(int);
    void namechanged(int r, int c);
private:
    void myRowAdder(bool newData = true, int i = 0);
    void apply();
    void setupGUI(vsExportDataInfo &info_tmp);

    Ui::DataExporterSetting *ui;
    boost::shared_ptr<vsExportDataInfo> m_info;
    vsExportDataInfo m_info_tmp;
};

#endif // DATAEXPORTER_H
