﻿#include "vsdataexportersetting.h"
#include "ui_DataExporterSetting.h"
#include "vmsg.h"
#include <QComboBox>

vsDataExporterSetting::vsDataExporterSetting(boost::shared_ptr<vsExportDataInfo> &info, QWidget *parent) :
    m_info(info),
    QDialog(parent),
    ui(new Ui::DataExporterSetting) {
    ui->setupUi(this);

    if ( info.get() ) {
        vBlock("vsDataExporterSetting");
        m_info_tmp.m_settingName = info->m_settingName;
        m_info_tmp.m_data = info->m_data;
        setupGUI(m_info_tmp);
    }
}

vsDataExporterSetting::~vsDataExporterSetting() {
    delete ui;
}


/**
 * @brief vsDataExporterSetting::setupGUI
 * @param info_tmp
 */
void vsDataExporterSetting::setupGUI(vsExportDataInfo &info_tmp) {

    ui->lineEdit->setText(info_tmp.m_settingName);

    for ( int i = 0; i < info_tmp.m_data.size(); i++ ) {

        vBlock(" setupGUI:for ");
        myRowAdder(false,i);

        int r;

        r = ui->tableWidget->rowCount() - 1;
        ui->tableWidget->model()->setData(ui->tableWidget->model()->index(r,0), (info_tmp.m_data[i])->identification );

        QComboBox* combo_type = reinterpret_cast<QComboBox*>( ui->tableWidget->cellWidget(r,1) );
        combo_type->setCurrentIndex((info_tmp.m_data[i])->type);
        combo_type->setProperty("userdata", (long long)((info_tmp.m_data[i]).get()));//x64対応

        QComboBox* combo_info = reinterpret_cast<QComboBox*>( ui->tableWidget->cellWidget(r,2) );
        combo_info->setProperty("userdata", (long long)((info_tmp.m_data[i]).get()));//x64対応

    }


}


/**
 * @brief vsDataExporterSetting::dataTypechanged
 */
void vsDataExporterSetting::dataTypechanged(int) {

    vBlock("dataTypechanged(int)");
    QComboBox* senderCmb = reinterpret_cast<QComboBox*>( sender() );
    int nRow = senderCmb->property("row").toInt();
    QComboBox* combo = reinterpret_cast<QComboBox*>( ui->tableWidget->cellWidget(nRow,2) );


    int newidx = senderCmb->currentIndex();
    vDebugVar(newidx);

    vsExportData* data = reinterpret_cast<vsExportData*>(combo->property("userdata").toLongLong());
    if( data == 0 ) {
        return;
    }

    if ( combo != 0 )  combo->clear();

    if( newidx == 0 ) {

        //Day
        if ( combo != 0 ) {
            for( QString* item = &vsExportDataFactory::name_day[0];
                    *item != QString(); item++ ) {
                combo->addItem(*item);

            }
        }

        data->type = vsExportData::DataType_DAY;

    }
    else if( newidx == 1 ) {

        //STL
        if ( combo != 0 ) {
            for( QString* item = &vsExportDataFactory::name_stl[0];
                 *item != QString(); item++) {
                combo->addItem(*item);
            }
        }

        data->type = vsExportData::DataType_STL;
    }
    else if( newidx == 2 ) {

        //Human Model
        if ( combo != 0 ) {
            for( QString* item = &vsExportDataFactory::name_humanmodel[0];
                 *item != QString(); item++ ) {
                combo->addItem(*item);
            }
        }
        data->type = vsExportData::DataType_HUMANMODEL;

    }
    else if( newidx == 3 ) {

        //Dicom Data
        if ( combo != 0 ) {
            for( QString* item = &vsExportDataFactory::name_dicomdata[0];
                 *item != QString(); item++ ) {
                combo->addItem(*item);
            }
        }

        data->type = vsExportData::DataType_DICOM;
    }
    else if( newidx == 4 ) {

        //Measurement
        if ( combo != 0 ) {
            for( QString* item = &vsExportDataFactory::name_measurement[0];
                 *item != QString(); item++ ) {

                combo->addItem(*item);
            }

        }

        data->type = vsExportData::DataType_MEASUREMENT;

    }
    else if( newidx == 5 ) {

        //Camera
        if ( combo != 0 ) {
            for( QString* item = &vsExportDataFactory::name_camera[0];
                 *item != QString(); item++ ) {
                combo->addItem(*item);
            }
        }

        data->type = vsExportData::DataType_CAMERA;

    }
    else if( newidx == 6 ) {

        //User Input
        if ( combo != 0 ) {
            for( QString* item = &vsExportDataFactory::name_userinput[0];
                 *item != QString(); item++ ) {
                combo->addItem(*item);
            }
        }

        data->type = vsExportData::DataType_USERINPUT;

    }

    vBlock("5");
    if ( combo != 0 ) {
        vBlock("6");
        combo->setCurrentIndex(0);
    }

}



void vsDataExporterSetting::myRowAdder(bool newData,int i) {

    vBlock("myRowAdder");
    int index = ui->tableWidget->rowCount();
    ui->tableWidget->insertRow(index);

    QComboBox* combo = new QComboBox();
    ui->tableWidget->setCellWidget(index,1,combo);
    combo->addItem(QString("Day"));
    combo->addItem(QString("STL"));
    combo->addItem(QString("Human Model"));
    combo->addItem(QString("Dicom Data"));
    combo->addItem(QString("Measurement"));
    combo->addItem(QString("Camera"));
    combo->addItem(QString("User Input"));

    combo->setProperty("row", (int) index);
    combo->setProperty("col", (int) 1);


    QComboBox* combo2 = new QComboBox();
    ui->tableWidget->setCellWidget(index,2,combo2);
    combo2->setProperty("row", (int) index);
    combo2->setProperty("col", (int) 2);


    boost::shared_ptr<vsExportData> data(new vsExportData);
    data->identification = QString("identification");
    ui->tableWidget->model()->setData(ui->tableWidget->model()->index(index,0),data->identification );
            
    if ( m_info && newData ) {
           m_info_tmp.m_data.push_back(data);
    }
    else if ( m_info ) {

        data = m_info_tmp.m_data[i];
    }

    combo->setProperty("userdata", (long long)(data.get()));//x64対応
    combo2->setProperty("userdata", (long long)(data.get()));//x64対応

    ui->tableWidget->connect(ui->tableWidget,SIGNAL(cellChanged(int,int)),this,SLOT(namechanged(int,int)));

    combo->connect(combo,SIGNAL(currentIndexChanged(int)),this,SLOT(dataTypechanged(int)));
    combo2->connect(combo2,SIGNAL(currentIndexChanged(int)),this,SLOT(dataInfochanged(int)));


    combo->setCurrentIndex(-1);
    combo->setCurrentIndex(0);
    combo2->setCurrentIndex(0);

}

void vsDataExporterSetting::apply() {

    if (m_info) {
        m_info_tmp.m_settingName = ui->lineEdit->text();
        *m_info = m_info_tmp;
    }

}

void vsDataExporterSetting::on_pushButton_OK_clicked() {
    apply();
    this->accept();
}


void vsDataExporterSetting::on_pushButton_cancel_clicked() {
    vMsg("on_pushButton_cancel");
    this->reject();
}



void vsDataExporterSetting::on_pushButton_addItem_clicked() {

    myRowAdder();

}

void vsDataExporterSetting::on_pushButton_delete_clicked() {

    vMsg("b");

}

void vsDataExporterSetting::dataInfochanged(int) {

    QComboBox* senderCmb = reinterpret_cast<QComboBox*>( sender() );
    int nRow = senderCmb->property("row").toInt();
    QComboBox* combo = reinterpret_cast<QComboBox*>( ui->tableWidget->cellWidget(nRow,2) );

    vsExportData* data = reinterpret_cast<vsExportData*>(combo->property("userdata").toLongLong());

    if( data ) {

        int newIdx = senderCmb->currentIndex();
        data->info = vsExportDataFactory::getDataInfoID(data->type,newIdx);

    }

}


void vsDataExporterSetting::namechanged(int r,int c) {

    vBlock("namechanged:");

    QTableWidget* widget = reinterpret_cast<QTableWidget*>( sender() );


    if ( !widget ) {
        vDebugVar(widget);
    }

    QComboBox* combo = reinterpret_cast<QComboBox*>( ui->tableWidget->cellWidget(r,c+1) );
    if ( !combo ) {
        vDebugVar(combo);
    }

    vsExportData* data = reinterpret_cast<vsExportData*>(combo->property("userdata").toLongLong());



    if ( !data ) {
        vDebugVar(data);
    }
    else {


        QString str = widget->model()->data(ui->tableWidget->model()->index(r,c)).toString();
        vMsg("data %p",data);
        vMsg("gg");
        data->identification = str;
        vMsg("hh");
        vMsg("namechanged:%s",qPrintable(data->identification ));

    }

}


