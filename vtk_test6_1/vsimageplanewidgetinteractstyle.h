﻿#ifndef __vsInteractorStyleImage_h
#define __vsInteractorStyleImage_h
#include "vsvtkinitializer.h"
#include <vtkInteractionStyleModule.h> // For export macro
#include <vtkInteractorStyleTrackballCamera.h>
#include "vsTransverseView.h"

// Motion flags

#define VTKIS_WINDOW_LEVEL 1024
#define VTKIS_PICK         1025
#define VTKIS_SLICE        1026

// Style flags

#define VTKIS_IMAGE2D 2
#define VTKIS_IMAGE3D 3
#define VTKIS_IMAGE_SLICING 4

class vtkImageProperty;

class vsTransverseView;
class vsInteractorStyleImage : public vtkInteractorStyleTrackballCamera
{
private:
    vsTransverseView* m_plane;
    vtkRenderWindowInteractor* m_iRen;
public:
  static vsInteractorStyleImage *New();
  vtkTypeMacro(vsInteractorStyleImage, vtkInteractorStyleTrackballCamera)
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Some useful information for handling window level
  vtkGetVector2Macro(WindowLevelStartPosition,int);
  vtkGetVector2Macro(WindowLevelCurrentPosition,int);

  // Description:
  // Event bindings controlling the effects of pressing mouse buttons
  // or moving the mouse.
  virtual void OnMouseMove();
  virtual void OnLeftButtonDown();
  virtual void OnLeftButtonUp();
  virtual void OnMiddleButtonDown();
  virtual void OnMiddleButtonUp();
  virtual void OnRightButtonDown();
  virtual void OnRightButtonUp();
  virtual void OnMouseWheelForward();
  virtual void OnMouseWheelBackward();
  void setWidget(vsTransverseView* plane);
  void setInteractor(vtkRenderWindowInteractor* ren);


  // These methods for the different interactions in different modes
  // are overridden in subclasses to perform the correct motion. Since
  // they might be called from OnTimer, they do not have mouse coord parameters
  // (use interactor's GetEventPosition and GetLastEventPosition)
  virtual void WindowLevel();


  // Interaction mode entry points used internally.
  virtual void StartWindowLevel();
  virtual void EndWindowLevel();



  // Description:
  // Set/Get current mode to 2D or 3D.  The default is 2D.  In 3D mode,
  // it is possible to rotate the camera to view oblique slices.  In Slicing
  // mode, it is possible to slice through the data, but not to generate oblique
  // views by rotating the camera.
  vtkSetClampMacro(InteractionMode, int, VTKIS_IMAGE2D, VTKIS_IMAGE_SLICING);
  vtkGetMacro(InteractionMode, int);
  void SetInteractionModeToImage2D() {
    this->SetInteractionMode(VTKIS_IMAGE2D); }
  void SetInteractionModeToImage3D() {
    this->SetInteractionMode(VTKIS_IMAGE3D); }
  void SetInteractionModeToImageSlicing() {
    this->SetInteractionMode(VTKIS_IMAGE_SLICING); }

  // Description:
  // Set the orientations that will be used when the X, Y, or Z
  // keys are pressed.  See SetImageOrientation for more information.
  vtkSetVector3Macro(XViewRightVector, double);
  vtkGetVector3Macro(XViewRightVector, double);
  vtkSetVector3Macro(XViewUpVector, double);
  vtkGetVector3Macro(XViewUpVector, double);
  vtkSetVector3Macro(YViewRightVector, double);
  vtkGetVector3Macro(YViewRightVector, double);
  vtkSetVector3Macro(YViewUpVector, double);
  vtkGetVector3Macro(YViewUpVector, double);
  vtkSetVector3Macro(ZViewRightVector, double);
  vtkGetVector3Macro(ZViewRightVector, double);
  vtkSetVector3Macro(ZViewUpVector, double);
  vtkGetVector3Macro(ZViewUpVector, double);

  // Description:
  // Set the view orientation, in terms of the horizontal and
  // vertical directions of the computer screen.  The first
  // vector gives the direction that will correspond to moving
  // horizontally left-to-right across the screen, and the
  // second vector gives the direction that will correspond to
  // moving bottom-to-top up the screen.  This method changes
  // the position of the camera to provide the desired view.
  void SetImageOrientation(const double leftToRight[3],
                           const double bottomToTop[3]);

  // Description:
  // Get the current image property, which is set when StartWindowLevel
  // is called immediately before StartWindowLevelEvent is generated.
  // This is the image property of the topmost vtkImageSlice in the
  // renderer or NULL if no image actors are present.
  vtkImageProperty *GetCurrentImageProperty() {
    return this->CurrentImageProperty; }

protected:
  vsInteractorStyleImage();
  ~vsInteractorStyleImage();

  void SetCurrentImageToNthImage(int newidx);

  int WindowLevelStartPosition[2];
  int WindowLevelCurrentPosition[2];
  double WindowLevelInitial[2];
  vtkImageProperty *CurrentImageProperty;

  int InteractionMode;
  double XViewRightVector[3];
  double XViewUpVector[3];
  double YViewRightVector[3];
  double YViewUpVector[3];
  double ZViewRightVector[3];
  double ZViewUpVector[3];

private:
  vsInteractorStyleImage(const vsInteractorStyleImage&);  // Not implemented.
  void operator=(const vsInteractorStyleImage&);  // Not implemented.
};

#endif
