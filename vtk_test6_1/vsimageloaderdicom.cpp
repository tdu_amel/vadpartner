﻿#include "vsimageloaderdicom.h"
#include <stdio.h>

vsImageLoaderDICOM::vsImageLoaderDICOM()
{
}


bool vsImageLoaderDICOM::load(const char* kFileName, boost::shared_ptr<vsImageDICOM>* out) {

    // WindowsPC->IntelCPUはリトルエンディアン

        FILE* fp = 0;
        errno_t err = 0;
        size_t readflag = 0;//ファイルが終わったかどうか
        vsDicomGroup tag = 0;
        vsDicomElement elem = 0;
        //short dataVR = 0;//VR
        int byteNum = 0;//データ長

        int data32 = 0;//データ用
        short data16 = 0;//データ用
        //unsigned char data8 = 0;//データ用
        double dataDouble = 0;//データ用

        //unsigned char prefix[4]={0};//prefix用
        char dataDS[128] = {0};//DS用


        const vsDicomGroup Group_ExpInfo = 0x0020;//検査情報
         const vsDicomElement Element_InstanceNumber = 0x0013;//InstanceNumber
         const vsDicomElement Element_SlicePos = 0x0032;//SlicePos


        const vsDicomGroup Group_MesureInfo = 0x0018;//測定条件
         const vsDicomElement Element_SliceThin = 0x0050;//スライス厚


        const vsDicomGroup Group_ImgInfo = 0x0028;//画像の情報用グループ
         const vsDicomElement Element_Rows = 0x0010;//画像幅ようエレメント
         const vsDicomElement Element_Columns = 0x0011;//画像高さ用エレメント
         const vsDicomElement Element_PixelSpacing = 0x030;//ピクセル間隔
         const vsDicomElement Element_Stride = 0x0100;//画素値の割り当てビット数
         const vsDicomElement Element_WindowCenter = 0x1050;//ウィンドウ(階調ヒストグラム)中心
         const vsDicomElement Element_WindowWidth = 0x1051;//ウィンドウ(階調ヒストグラム)幅


        const vsDicomGroup Group_ImageData = 0x7FE0; //イメージデータ用のグループ
         const vsDicomElement Element_ImageData = 0x0010;//イメージデータ用のエレメント



        bool bGetRows,bGetColumns,bGetBitsAllocated,bGetImageData;
        bGetRows = bGetColumns = bGetBitsAllocated = bGetImageData = false;//データをとったらこのフラグをオンにする


        //戻り値用
        vsImageDICOM* img = new vsImageDICOM();


       //ファイルを開く
       err  = fopen_s( &fp, kFileName, "rb" );
       if( err != 0 )
       {
            //画像読み込み失敗。ファイルがロックされている、ファイルが存在しないなどの理由が考えられる。
            return false;
       }


        /*

            以降のファイル構成
            Group					2byte
            Element					2byte
            Value Representation	2byte(DataType)+2Byte(DataLen)
            Data					DataLen byte

            GroupとElementの組み合わせをタグ(Tag)という。
            タグによって何の情報が来るかを表している。

            どの情報がどういうタグを使用するかは
            「JIRA 医用画像システム部会 DICOM委員会」の勉強会ページの標準DICOM画像タグセット集を参照
            http://www.jira-net.or.jp/dicom/dicom_data_01.html

        */


        //グループIDを読み込み
        readflag = fread(&tag,sizeof(vsDicomGroup),1,fp);
        if(tag == 0) {
            img->m_bBigEndian = 1; //ビッグエンディアンだった
        } else {
            img->m_bBigEndian = 0; //リトルエンディアンだった
        }
        fread(&elem,sizeof(vsDicomElement),1,fp);


        //データを読み終わるまでループ
        do {


            if( tag == Group_ImgInfo ) {

                //画像の情報のグループ

                if( elem == Element_Rows ) {

                    //エレメントより、続くデータは画像の行数を表すと判断できる
                    //VpOutputDebugStringConsole(_T("(%04x,%04x)vsDicomElement\n"),tag,elem);

                    //fread(&dataVR,sizeof(dataVR),1,fp);//VR
                    //VpOutputDebugStringConsole(_T("VR:%c\n"),dataVR);
                    //fread(&data16,sizeof(data16),1,fp);

                    //4byte
                    fread(&data32,sizeof(data32),1,fp);
                    byteNum = data32;

                    //VpOutputDebugStringConsole(_T("byteNum:%d\n"),byteNum);
                    if(byteNum == 2) {

                        fread(&data16,sizeof(data16),1,fp);
                        img->m_iHeight = data16;

                    } else {}


                } else if( elem ==  Element_Columns ) {

                    //エレメントより続くデータは画像の列数を表す
                    //VpOutputDebugStringConsole(_T("(%04x,%04x)Element_Columns\n"),tag,elem);
                    //fread(&dataVR,sizeof(dataVR),1,fp);//VR
                    //VpOutputDebugStringConsole(_T("VR:%c\n"),dataVR);
                    //fread(&data16,sizeof(data16),1,fp);

                    fread(&data32,sizeof(data32),1,fp);
                    byteNum = data32;

                    //VpOutputDebugStringConsole(_T("byteNum:%d\n"),byteNum);
                    if(byteNum == 2) {

                        fread(&data16,sizeof(data16),1,fp);
                        img->m_iWidth = data16;

                    } else {}


                } else if( elem ==  Element_Stride ) {

                    //エレメントより続くデータは画素値の割り当てビット数(1画素あたり何ビットか)を表す
                    //VpOutputDebugStringConsole(_T("(%04x,%04x)Element_Stride\n"),tag,elem);
                    //fread(&dataVR,sizeof(dataVR),1,fp);//VR
                    //VpOutputDebugStringConsole(_T("VR:%c\n"),dataVR);
                    //fread(&data16,sizeof(data16),1,fp);

                    fread(&data32,sizeof(data32),1,fp);
                    byteNum = data32;

                    //VpOutputDebugStringConsole(_T("byteNum:%d\n"),byteNum);
                    if(byteNum == 2) {

                        fread(&data16,sizeof(data16),1,fp);
                        img->m_iStride = data16;

                    } else {}
                    bGetBitsAllocated = true;

                } else if( elem ==  Element_WindowCenter ) {

                    //エレメントより、続くデータはウィンドウ(階調ヒストグラム)中心と判断できる
                    //VpOutputDebugStringConsole(_T("(%04x,%04x)Element_WindowCenter\n"),tag,elem);
                    //fread(&dataVR,sizeof(dataVR),1,fp);//VR
                    //VpOutputDebugStringConsole(_T("VR:%c\n"),dataVR);
                    //fread(&data16,sizeof(data16),1,fp);
                    //byteNum = data16;

                    fread(&data32,sizeof(data32),1,fp);
                    byteNum = data32;

                    //VpOutputDebugStringConsole(_T("byteNum:%d\n"),byteNum);

                    memset(dataDS,0,sizeof(dataDS));
                    fread(&dataDS,sizeof(unsigned char),byteNum,fp);
                    img->m_iWindowCenter = atoi(dataDS);

                    bGetBitsAllocated = true;

                } else if( elem ==  Element_WindowWidth ) {

                    //エレメントより、続くデータはウィンドウ(階調ヒストグラム)中心と判断できる
                    //VpOutputDebugStringConsole(_T("(%04x,%04x)Element_WindowWidth\n"),tag,elem);
                    //fread(&dataVR,sizeof(dataVR),1,fp);//VR
                    //VpOutputDebugStringConsole(_T("VR:%c\n"),dataVR);
                    //fread(&data16,sizeof(data16),1,fp);

                    fread(&data32,sizeof(data32),1,fp);
                    byteNum = data32;

                    //VpOutputDebugStringConsole(_T("byteNum:%d\n"),byteNum);

                    memset(dataDS,0,sizeof(dataDS));
                    fread(&dataDS,sizeof(unsigned char),byteNum,fp);
                    img->m_iWindowWidth = atoi(dataDS);

                    bGetBitsAllocated = true;

                } else if( elem ==  Element_PixelSpacing ) {

                    //エレメントより、続くデータは画素間隔と判断できる
                    //VpOutputDebugStringConsole(_T("(%04x,%04x)Element_WindowWidth\n"),tag,elem);
                    //fread(&dataVR,sizeof(dataVR),1,fp);//VR
                    //VpOutputDebugStringConsole(_T("VR:%c\n"),dataVR);
                    //fread(&data16,sizeof(data16),1,fp);
                    boost::shared_ptr<__int8> buf;
                    boost::shared_ptr<__int8> sub;

                    fread(&data32,sizeof(data32),1,fp);
                    byteNum = data32;
                    //VpOutputDebugStringConsole(_T("byteNum:%d\n"),byteNum);


                    buf.reset( new __int8[byteNum] );
                    sub.reset( new __int8[byteNum] );

                    fread(buf.get(),sizeof(unsigned char),byteNum,fp);


                    int num = 0;
                    for( int j = 0; j < 2; ++j ) {
                        for( int newidx = 0; newidx < byteNum; ++newidx ) {

                            if( buf.get()[num] == '\\' ) break;

                            sub.get()[newidx] = buf.get()[num];
                            ++num;
                        }

                        ++num;
                        img->m_dPixelSpacing[j] = atof(sub.get());

                    }

                    //memset(dataDS,0,sizeof(dataDS));
                    //fread(&dataDS,sizeof(unsigned char),byteNum,fp);


                    //img->m_dPixelSpacing = atoi(dataDS);


                    //vsDouble m_dPixelSpacing[2];///<ピクセル間距離
                    //vsDouble m_dSliceThin;///<スライス厚み

                } else {

                    //VpOutputDebugStringConsole(_T("(%04x,%04x)OtherTag\n"),tag,elem);
                    fread(&data32,sizeof(data32),1,fp);
                    byteNum = data32;
                    //VpOutputDebugStringConsole(_T("byteNum:%d\n"),byteNum);

                    fseek(fp,byteNum,SEEK_CUR);

                }

            } else if( tag == Group_ExpInfo ) {

                //InstanceNumber
                if( elem == Element_InstanceNumber ) {

                    fread(&data32,sizeof(data32),1,fp);
                    byteNum = data32;


                    boost::shared_ptr<char> mybuf(new char[byteNum+1]);
                    memset(mybuf.get(),0,byteNum*sizeof(char));
                    fread(mybuf.get(),sizeof(char),byteNum,fp);
                    img->m_iInstanceID = atoi(mybuf.get());


                } else if( elem == Element_SlicePos ) {

                    //const vsDicomGroup Group_ExpInfo = 0x0020;//検査情報
                    //const vsDicomElement Element_InstanceNumber = 0x0013;//InstanceNumber
                    //const vsDicomElement Element_SlicePos = 0x0032;//SlicePos
                    fread(&data32,sizeof(data32),1,fp);
                    byteNum = data32;

                    boost::shared_ptr<char> mybuf(new char[byteNum+1]);
                    memset(mybuf.get(),0,byteNum*sizeof(char));
                    fread(mybuf.get(),sizeof(char),byteNum,fp);


                    //char *tp;

                    /* \とNULL文字を区切りに文字列を抽出 */
                    img->m_iX  = atof( strtok( mybuf.get(), "\\\0" ) );
                    img->m_iY  = atof(strtok( NULL,"\\\0" ) );
                    img->m_iZ  = atof(strtok( NULL,"\\\0" ) );


                } else {

                    fread(&data32,sizeof(data32),1,fp);
                    byteNum = data32;

                    if( byteNum!=0 ) {
                        fseek(fp,byteNum,SEEK_CUR);
                    }

                }

         } else if( tag == Group_MesureInfo ) {

                //スライス厚の取得
                if( elem == Element_SliceThin ) {



                    fread(&data32,sizeof(data32),1,fp);
                    byteNum = data32;

                    //VpOutputDebugStringConsole(_T("byteNum:%d\n"),byteNum);
                    boost::shared_ptr<char> mybuf(new char[byteNum+1]);
                    memset(mybuf.get(),0,byteNum*sizeof(char));
                    fread(mybuf.get(),sizeof(char),byteNum,fp);

                    mybuf.get()[byteNum] = '\0';

                    dataDouble = atof(mybuf.get());
                    img->m_dSliceThin = dataDouble;

                } else {

                    fread(&data32,sizeof(data32),1,fp);
                    byteNum = data32;

                    if( byteNum!=0 ) {
                        fseek(fp,byteNum,SEEK_CUR);
                    }

                }

         } else if( tag == Group_ImageData ) {

                //画像データのグループ

                if( elem == Element_ImageData ) {
/*
                    //VpOutputDebugStringConsole(_T("(%04x,%04x)Element_ImageData\n"),tag,elem);
                    fread(&data16,sizeof(data16),1,fp);

                    //画像データのエレメントだった
                    if( !memcmp(&data16,"OW",sizeof(data16)) ) {

                        fseek(fp,+2,SEEK_CUR);

                    } else {

                        fseek(fp,-2,SEEK_CUR);

                    }

                    fread(&byteNum,sizeof(byteNum),1,fp);
                    //VpOutputDebugStringConsole(_T("byteNum:%d\n"),byteNum);

                    unsigned char* buf = new unsigned char[byteNum];
                    fread(buf,sizeof(unsigned char),byteNum,fp);
                    img->m_spData = boost::shared_ptr<unsigned char>(buf);
                    bGetImageData = true;
*/
                } else {

                    //VpOutputDebugStringConsole(_T("(%04x,%04x)OtherTag\n"),tag,elem);
                    fread(&data32,sizeof(data32),1,fp);
                    byteNum = data32;
                    //VpOutputDebugStringConsole(_T("byteNum:%d\n"),byteNum);

                    if( byteNum!=0 ) {
                        fseek(fp,byteNum,SEEK_CUR);
                        //boost::shared_ptr<unsigned char> tmp( new unsigned char[byteNum] );
                        //fread(tmp.get(),sizeof(unsigned char),byteNum,fp);
                    }

                }

            } else {

                    //VpOutputDebugStringConsole(_T("(%04x,%04x)OtherTag\n"),tag,elem);
                    fread(&data32,sizeof(data32),1,fp);
                    byteNum = data32;
                    //VpOutputDebugStringConsole(_T("byteNum:%d\n"),byteNum);


                    if( byteNum!=0 ) {
                        fseek(fp,byteNum,SEEK_CUR);
                        //boost::shared_ptr<unsigned char> tmp( new unsigned char[byteNum] );
                        //fread(tmp.get(),sizeof(unsigned char),byteNum,fp);
                    }

            }

            //タグ分(2byte)読み込み
            readflag = fread(&tag,sizeof(vsDicomGroup),1,fp);
            if( readflag > 0 )readflag = fread(&elem,sizeof(vsDicomElement),1,fp);

        }while( readflag > 0 );
        fclose(fp);//ファイルを閉じる

        //スマートポインタに結果をセット
        *out = boost::shared_ptr<vsImageDICOM>(img);

        return true;
    }


