﻿#include "vsSTLManager.h"
#include <vtkSTLReader.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <QDebug>
#include <QProgressDialog>
#include <QSharedPointer>
#include <QProgressBar>
#include <QCoreApplication>
#include <QFileInfo>
#include <vtkCallbackCommand.h>
#include "vmsg.h"

vsSTLManager::vsSTLManager()
    :m_oldSelectedSTL(-1),m_mainwindow(0)
{
}



vsSTLManager::~vsSTLManager() {

}



struct vsProgressFunctionObj {


    static QSharedPointer<QProgressDialog> m_progressWindow;
    static int m_progress;
    static int m_max;

    static void start(const char* fileName) {
        m_progressWindow =
                QSharedPointer<QProgressDialog>(new QProgressDialog("Load File...", "", 0, 100, 0) );

        m_progressWindow->show();
        m_progressWindow->setCancelButton(0);
        m_progress = 0;
        QFileInfo fileInfo(fileName);
        m_max = (fileInfo.size() / 1048576 *0.77);
        if( m_max == 0) m_max = 1;
    }

    static void end() {
        if( m_progressWindow.data() ) {
            m_progressWindow->close();
            m_progressWindow.clear();
        }
    }

    static void reportProgress( vtkObject* vtkNotUsed(caller),
                                long unsigned int vtkNotUsed(eventId),
                                void* vtkNotUsed(clientData),
                                void* vtkNotUsed(callData)) {


        if( m_progressWindow.data() == 0 ) {
            return;
        }

        if (m_max == 0) m_max = 1;


        int progress = m_progress++ * 100 / m_max;

        //vtkSTLReader* stlReader = reinterpret_cast<vtkSTLReader*>(caller);
        //double progress = stlReader->GetProgress();

        if( progress >= 100 ) progress = 99;

        m_progressWindow->setValue(progress);
        QCoreApplication::processEvents();

    }
};
QSharedPointer<QProgressDialog> vsProgressFunctionObj::m_progressWindow;
int vsProgressFunctionObj::m_progress = 0;
int vsProgressFunctionObj::m_max = 0;



void vsSTLManager::loadSTL(const char* fileName, vtkSmartPointer<vtkActor>& actor ) {

    vtkSmartPointer<vtkCallbackCommand> progressCallback =
      vtkSmartPointer<vtkCallbackCommand>::New();
    progressCallback->SetCallback(vsProgressFunctionObj::reportProgress);


    vsProgressFunctionObj::start(fileName);

    vtkSmartPointer<vtkSTLReader> reader =
      vtkSmartPointer<vtkSTLReader>::New();


    reader->SetFileName(fileName);
    reader->Update();


    // Visualize
    vtkSmartPointer<vtkPolyDataMapper> mapper =
      vtkSmartPointer<vtkPolyDataMapper>::New();
    mapper->SetInputConnection(reader->GetOutputPort());

    actor = vtkSmartPointer<vtkActor>::New();
    actor->SetMapper(mapper);

    m_ActorSTL.push_back(actor);

}

void vsSTLManager::selectSTL(vtkActor* actor ) {

    vBlock("vsSTLManager::selectSTL");

    if( actor == 0 ) {
        if( m_oldSelectedSTL != -1 ) {

            m_ActorSTL[m_oldSelectedSTL]->GetProperty()->SetColor(1.0, 1.0, 1.0);
            m_oldSelectedSTL = -1;

        }

        return ;
    }

    //線形探索
    unsigned int newidx;
    for( newidx = 0; newidx < m_ActorSTL.size(); ++newidx ) {

       if( m_ActorSTL[newidx].Get() == actor ) {
            break;
       }

    }

    vDebugVar(newidx);
    if( newidx == m_oldSelectedSTL ) return;


    if( newidx < m_ActorSTL.size() ) {

        if( m_oldSelectedSTL != -1 ) {

            m_ActorSTL[m_oldSelectedSTL]->GetProperty()->SetColor(1.0, 1.0, 1.0);

        }

        m_ActorSTL[newidx]->GetProperty()->SetColor(1, 0.5, 0.3);


        m_oldSelectedSTL = newidx;

    }


}
