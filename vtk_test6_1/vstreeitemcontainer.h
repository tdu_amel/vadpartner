﻿#ifndef VSTREEITEMCONTAINER_H
#define VSTREEITEMCONTAINER_H
#include "vsvtkinitializer.h"
#include "vstreeitem.h"
#include <list>
class vtkProp;



class vsTreeItemContainer
{
protected:
    std::list< vsTreeItem* > m_treeItems;

public:
    vsTreeItemContainer();
    virtual ~vsTreeItemContainer();

    void findItem(std::list< vsTreeItem* >::iterator& itr,vtkProp* prop );
    void unselectAllItems();
    std::list<vsTreeItem*>::iterator begin();
    std::list<vsTreeItem*>::iterator end();
    void push_back(vsTreeItem*& p);
};

#endif // VSTREEITEMCONTAINER_H
