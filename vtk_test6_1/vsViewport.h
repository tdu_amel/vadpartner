#ifndef VSVIEWPORT
#define VSVIEWPORT


struct vsViewPort{

    double p1_x,p1_y;
    double p2_x,p2_y;
    double r,g,b;

    bool isMouseOver(int x, int y , int ww, int wh);
};

#endif // VSVIEWPORT
