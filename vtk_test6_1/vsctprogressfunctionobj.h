﻿#ifndef VSCTPROGRESSFUNCTIONOBJ_H
#define VSCTPROGRESSFUNCTIONOBJ_H
#include <QDirIterator.h>
#include <QProgressDialog>
#include <QSharedPointer>
#include <QCoreApplication>
#include <vtkCommand.h>
#include <vtkDICOMImageReader.h>
#include "QDebug"
#include "vsImageDicom.h"
#include "vsImageLoaderDicom.h"
#include "vMsg.h"

struct vsCTProgressFunctionObj {

    static QSharedPointer<QProgressDialog> m_progressWindow;

    static void start(const char* fileName) {
        m_progressWindow = QSharedPointer<QProgressDialog>(new QProgressDialog(QString("Load %1...").arg(fileName), "", 0, 100, 0) );

        m_progressWindow->show();
        m_progressWindow->setCancelButton(0);

    }

    static void end() {
        if( m_progressWindow.data() ) {
            m_progressWindow->close();
            m_progressWindow.clear();
        }
    }

    static void reportProgress( vtkObject* caller,
                                long unsigned int vtkNotUsed(eventId),
                                void* vtkNotUsed(clientData),
                                void* vtkNotUsed(callData)) {


        if( m_progressWindow.data() == 0 ) {
            return;
        }

        vtkDICOMImageReader* stlReader = reinterpret_cast<vtkDICOMImageReader*>(caller);
        double progress = stlReader->GetProgress();

        m_progressWindow->setValue(progress*100);
        QCoreApplication::processEvents();

        vMsg("%lf",progress);

    }
};




#endif // VSCTPROGRESSFUNCTIONOBJ_H
