﻿#ifndef VSBODYBOXPRODUCTOR_H
#define VSBODYBOXPRODUCTOR_H
#include "vsvtkinitializer.h"
#include <vtkSmartPointer.h>
#include <vtkDICOMImageReader.h>


class vsBodyBoxProductor
{
    vtkSmartPointer<vtkDICOMImageReader> m_reader;
public:
    vsBodyBoxProductor();
    virtual ~vsBodyBoxProductor(){}

    void setDICOMImage(vtkSmartPointer<vtkDICOMImageReader>& reader);
};

#endif // VSBODYBOXPRODUCTOR_H
