﻿#ifndef VSTRANSVERSEVIEW_H
#define VSTRANSVERSEVIEW_H
#include "vsvtkinitializer.h"
#include <vtkImagePlaneWidget.h>
#include <vtkSmartPointer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkCellPicker.h>
#include <vtkProperty.h>
#include <vtkDicomImageReader.h>
#include <vtkImageData.h>
#include <vtkRenderer.h>
#include <vtkCamera.h>

class vsInteractorStyleImage;

enum vsDicomViewOrientation
{

    VS_DVORIENTATION_X = 0,
    VS_DVORIENTATION_Y = 1,
    VS_DVORIENTATION_Z = 2

};


class vsTransverseView
{
protected:
    vtkSmartPointer<vtkImagePlaneWidget> m_plane;

    vtkSmartPointer<vtkCellPicker> m_picker;
    vtkSmartPointer<vtkProperty> m_property;
    vtkSmartPointer<vtkDICOMImageReader> m_reader;
    vtkSmartPointer<vtkRenderer> m_renderer;
    vsDicomViewOrientation m_orientation;

    vtkSmartPointer<vsInteractorStyleImage> m_style;

public:
    vsTransverseView();
    virtual ~vsTransverseView();
    void setInteractor(vtkRenderWindowInteractor *iRen);
    void setDicomReader(vtkSmartPointer<vtkDICOMImageReader>& iRen);
    void setOrientation(vsDicomViewOrientation o);
    void setSliceIndex(int idx);
    void setRenderer(vtkSmartPointer<vtkRenderer>& ren);
    void activate();
    void resetSlicePos();
    int getSliceIndex();
    int getSliceMaxIndex();
};

#endif // VSTRANSVERSEVIEW_H
