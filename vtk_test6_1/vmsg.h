﻿#ifndef VMSG_H
#define VMSG_H
#include <cstdarg>
#include <qdebug>
#include <QTextStream>
#include <stack>


extern const QString g_log_file_name;///< ログファイルの名前
extern QTextStream g_log_stream;///< ログファイルを出力するテキストストリーム

/*
* vBlock　インデントをセットします　階層的に処理を行っている場所でメッセージを出す場合に，どこの処理の中なのかがわかりやすくなります　ブロックを抜けると自動でセットしたインデント分だけ消えます
* vDebugVar　変数をセットするだけで，その変数の名前と内容を表示します vDebugVar(var);なら　var is 10　などのように表示されます
* vMsg　一般的なメッセージの表示に
* vWarning　警告メッセージの表示に使ってください
* vCritical 致命的エラーの表示に使ってください
* vMsgS qDebug()などと同じようにストリーム形式でメッセージを出力 例）vMsgS() << "test" << 10;
*/

/**
 * @brief The __vMsg struct アプリケーション出力欄にメッセージを出すための構造体です
 * 内部で使用しているだけなので，実際メッセージを出す場合はvMsgなどを使ってください
 * 使うメリット：メッセージを発行した場所（ファイル名，行）を自動で表示，vBlockを使うことでインデントを設定できる　以上
 */
struct __vMsg {


    static QString indentStr;///<　インデントに使用する文字列　デフォルトでは|(パイプ)がつかわれます
    static unsigned int m_indent;///<現在の全体のインデント数
    static int message_level_console;///<　コンソールに出力する最低メッセージレベル(msg = 0, warning = 1, critical = 2, fatal = 3)
    static int message_level_file;///<　ファイルに出力する最低メッセージレベル(msg = 0, warning = 1, critical = 2, fatal = 3)

    unsigned int m_localindent;///<このメッセージのインデント
    QString m_blockname;///<このメッセージのブロック名
    QString m_fileName;///<このメッセージが発行されたファイルの名前


    explicit __vMsg(QString blockname, unsigned int indent = 1);
    explicit __vMsg(QString blockname, const char* file, const int line, unsigned int indent = 1);
    virtual ~__vMsg();


    static void setupBaseMessege(QString* str, const char *file, const int line);
    static void msg(const char *file, const int line, const char* msg , const message_level,...);
    static void warning(const char *file, const int line, const char* msg, ...);
    static void critical(const char *file, const int line, const char* msg, ...);
    static void fatal(const char *file, const int line, const char* msg, ...);

    template<typename T>
    static QDebug stream(const T v,const char* file, const int line) {

        QString str;

        setupBaseMessege(&str,file,line);

        return qDebug() << qPrintable(str) << v;
    }


    static QDebug stream(const char* file, const int line) {

        QString str;

        setupBaseMessege(&str,file,line);

        return qDebug()<< qPrintable(str) << "";
    }


};

#define __vFILE__ __FILE__
#define __vLINE__ __LINE__


//VisualStudio 2003 以前ではコンパイルエラーになります。
#if (defined(__GNUC__)) || (_MSC_VER >= 1400)
#define vMsg(x,...)\
    __vMsg::msg(__vFILE__,__vLINE__,x,__VA_ARGS__)
#else
    maybe __VA_ARGS__ is unenable in your compiler
#endif


#if (defined(__GNUC__)) || (_MSC_VER >= 1400)
#define vWarning(x,...)\
    __vMsg::warning(__vFILE__,__vLINE__,x,__VA_ARGS__)
#else
    maybe __VA_ARGS__ is unenable in your compiler
#endif

#if (defined(__GNUC__)) || (_MSC_VER >= 1400)
#define vCritical(x,...)\
    __vMsg::critical(__vFILE__,__vLINE__,x,__VA_ARGS__)
#else
    maybe __VA_ARGS__ is unenable in your compiler
#endif

#define vMsgS(x)\
    __vMsg::stream(x,__vFILE__,__vLINE__)


#define ___vToStr(x) #x
#define __vToStr(x) ___vToStr(x)


#define vBlock3(blockname) \
    __vMsg msg ## __vLINE__ ## (blockname,1)

#define vBlock0(blockname) \
    __vMsg msg ## __vLINE__  ## (blockname,0)

#define __vBlock() \
     __vMsg msg

#define _vBlock() \
     __vBlock() ## __vLINE__

#define vBlock(blockname) \
    _vBlock() ## (blockname,__vFILE__,__vLINE__,1)

#define vDebugVar(x)\
    __vMsg::stream(__vFILE__,__vLINE__) << #x <<"is" << x



#endif // VMSG_H
