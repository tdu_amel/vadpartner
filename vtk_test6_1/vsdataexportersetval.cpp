﻿#include "vsdataexportersetval.h"
#include "ui_dataexportersetval.h"
#include "vsDataExporterSetting.h"
#include <QMessageBox>
#include <QDir>
#include <algorithm>
#include <tinyxml2.h>
#include <QFileDialog>
#include <QLineEdit>
#include "mainwindow.h"
#include "vmsg.h"

/**
 * @brief vsDataExporterSetVal::vsDataExporterSetVal
 * @param parent
 */
vsDataExporterSetVal::vsDataExporterSetVal(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DataExporterSetVal),
    m_settingNum(0),
    m_mainwindow(0)
{
    ui->setupUi(this);
}

/**
 * @brief vsDataExporterSetVal::~vsDataExporterSetVal
 */
vsDataExporterSetVal::~vsDataExporterSetVal()
{
    vBlock("~vsDataExporterSetVal");
    delete ui;
}

/**
 * @brief vsDataExporterSetVal::setMainWindow
 * @param window
 */
void vsDataExporterSetVal::setMainWindow(MainWindow* window) {
    m_mainwindow = window;
    long long ptr = (long long)m_mainwindow;
    vMsg("SetMainWindow:%lld",ptr);
}

/**
 * @brief vsDataExporterSetVal::setupComboBox
 */
void vsDataExporterSetVal::setupComboBox() {
    QHash<QString,boost::shared_ptr<vsExportDataInfo>>::iterator itr;
    if ( m_settingNum != m_data.size() ) {
        ui->comboBox->clear();
        for( itr = m_data.begin(); itr != m_data.end(); itr++ ) {
            ui->comboBox->addItem( (*itr)->m_settingName );
        }
        m_settingNum = m_data.size();
    }
}

/**
 * @brief vsDataExporterSetVal::event
 * @param e
 * @return
 */
bool vsDataExporterSetVal::event(QEvent *e) {
    if (e->type() == QEvent::WindowActivate) {
        loadXMLs();
        setupComboBox();
    }
    return QWidget::event(e);
}

/**
 * @brief vsDataExporterSetVal::loadXML
 * @param file
 * @param data
 * @return
 */
bool vsDataExporterSetVal::loadXML(QString& file, boost::shared_ptr<vsExportDataInfo> &data) {
    vBlock("loadXML");
    if ( !data.get() ) {
        vWarning("data is Null");
        return false;
    }
    if ( file.length() == 0 ) {
        vWarning("file name is invalid");
        return false;
    }
    tinyxml2::XMLDocument xml;
        vDebugVar(file);
        xml.LoadFile(file.toStdString().c_str());
        if ( xml.Error() ) {
            vWarning("The XML has error:%s",xml.ErrorName());
            return false;
        }
    //最初の要素を取得
    tinyxml2::XMLElement* exportsetting = xml.FirstChildElement("exportsetting");
    vDebugVar(exportsetting);


    if( !exportsetting ) {
         return false;
    }
    //QString settingname = exportsetting->Attribute("settingname");
    tinyxml2::XMLElement* element = exportsetting->FirstChildElement("export");
    while ( element ) {
        boost::shared_ptr<vsExportData> exportData(new vsExportData);
        if ( !(exportData.get()) ) {
            return false;
        }
        const char *identification = element->Attribute("identification");
        if ( !identification ) {
            QMessageBox::warning(this,QString("XML parse Error"),QString("Invalid identification"),QMessageBox::Ok);
        }
        exportData->identification = identification;
        const char *type = element->Attribute("type");
        if ( !type ) {
            QMessageBox::warning(this,QString("XML parse Error"),QString("Invalid type"),QMessageBox::Ok);
        }
        exportData->type = (vsExportData::DataType)atoi(type);
        const char *info = element->Attribute("info");
        if ( !type ) {
            QMessageBox::warning(this,QString("XML parse Error"),QString("Invalid info"),QMessageBox::Ok);
        }
        exportData->info = (vsExportData::DataValInfo)atoi(info);
        data->m_data.push_back(exportData);
        element = element->NextSiblingElement();
    }
    return true;
}

/**
 * @brief vsDataExporterSetVal::loadXMLs
 * @return
 */
bool vsDataExporterSetVal::loadXMLs() {
    vBlock("loadXMLs");
    //保存先ディレクトリ　=　（vadsim.exeがあるフォルダ）\\exportsettings
    QString xml_dir_path_name(QCoreApplication::applicationDirPath() + "/exportsettings");
    QDir xml_dir_path(xml_dir_path_name);
    if ( !xml_dir_path.exists() ) {
         vBlock("err!");
        return false;
    }
    //全てのファイル・ディレクトリ名取得
    QStringList filelist = xml_dir_path.entryList();
    for ( QStringList::iterator itr = filelist.begin(); itr != filelist.end(); itr++ ) {
        QFileInfo info( xml_dir_path.absoluteFilePath(*itr) );
        vDebugVar((*itr));
        if ( info.isFile() ) {
            QString settingName = info.completeBaseName();
            if ( !(m_data.contains(settingName)) || (info.lastModified() != m_data[settingName]->m_lastModified) ) {
                //読み込んでいない設定かファイルが新しくなっていた場合はその設定ファイルを読み込む
                boost::shared_ptr<vsExportDataInfo> exportInfo(new vsExportDataInfo);
                if ( !exportInfo.get() ) return false;
                exportInfo->m_settingName = settingName;
                loadXML(info.absoluteFilePath(),exportInfo);
                if ( !(m_data.contains(settingName)) ) {
                    m_data.insert(settingName,exportInfo);
                }
                else {
                    m_data[settingName] = exportInfo;
                }
                m_data[settingName]->m_lastModified = info.lastModified();
            }
        }
    }
   return true;
}

/**
 * @brief vsDataExporterSetVal::on_pushButton_new_clicked
 */
void vsDataExporterSetVal::on_pushButton_new_clicked()
{
    boost::shared_ptr<vsExportDataInfo> data(new vsExportDataInfo);
    if (data.get() == 0)return;
    //設定ダイアログを起動
    vsDataExporterSetting dialog(data);
    int dialogRet = 0;
    for(;;) {
        dialogRet = dialog.exec();
        if( data->m_settingName.length() == 0  ) {
            QMessageBox::warning(this,QString(""),QString("Invalid Setting Name!"),QMessageBox::Ok);
        }
        else if( m_data.contains(data->m_settingName) ) {
            QMessageBox::warning(this,QString(""),QString("Setting Name '") +data->m_settingName+ QString("' is Already Existed!"),QMessageBox::Ok);
        }
        else {
            break;
        }
    }
    if( dialogRet ) {
        vBlock("setting");
        //ダイアログでOKを押したときのみここが実行される
        m_data.insert(data->m_settingName,data);
        ui->comboBox->addItem(data->m_settingName);
        export_setting(data);
    }
}


/**
 * @brief vsDataExporterSetVal::on_comboBox_currentIndexChanged
 * @param index
 */
void vsDataExporterSetVal::on_comboBox_currentIndexChanged(int index) {
    vBlock("on_comboBox_currentIndexChanged");
    QString str = ui->comboBox->itemText(index);
    if ( !m_data.contains( str ) ) {
        //コンボボックスで選んだ設定が実データ内に見つからなかった
        vMsg("Noitem \'%s\'",str.toStdString().c_str());
        {
            vBlock("settings names");
            //Qt4にQHash<QString,boost::shared_ptr<vsExportDataInfo>>::key_iteratorないです
            QList<QString> keys = m_data.keys();
            typedef QList<QString>::iterator IterType;
            for( IterType itr = keys.begin() ; itr != keys.end(); itr++ ) {
                vMsg("%s",qPrintable((*itr)));
            }
        }
        return;
    }
    //キーがあったときはここも実行
    boost::shared_ptr<vsExportDataInfo> exportdata_setting = m_data[str];
    vsExportDataFactory::addExportDataUI(m_mainwindow,&(ui->verticalLayout_exportdata_area),exportdata_setting);
}

/**
 * @brief vsDataExporterSetVal::on_pushButton_delete_clicked
 */
void vsDataExporterSetVal::on_pushButton_delete_clicked()
{
}

/**
 * @brief vsDataExporterSetVal::export_setting
 * @param data
 * @return
 */
bool vsDataExporterSetVal::export_setting(boost::shared_ptr<vsExportDataInfo>& data) {

    using namespace tinyxml2;
    XMLDocument xml;
    XMLDeclaration* decl = xml.NewDeclaration();
    xml.InsertEndChild(decl);
    decl->SetValue("xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"");
    // XMLElementが基本で、要素を作成出来ます
    XMLElement* tag_exportsetting = xml.NewElement("exportsetting");
    //settingnameという属性を追加
    tag_exportsetting->SetAttribute("settingname",data->m_settingName.toStdString().c_str());
    // まずはルートタグを追加します（FirstにするとPrologueの上に来てしまう）
    xml.InsertEndChild(tag_exportsetting);
    for ( int i = 0; i < data->m_data.size(); i++ ) {
        // 次は追加したルートタグに対してexportという子タグを入れたいのでNewElement
        XMLElement* tag_export = xml.NewElement("export");
        tag_export->SetAttribute("identification",data->m_data[i]->identification.toStdString().c_str());
        tag_export->SetAttribute("type",(int)(data->m_data[i]->type));
        tag_export->SetAttribute("info",(int)(data->m_data[i]->info));
        //末尾にexportタグを追加
        tag_exportsetting->InsertEndChild(tag_export);
    }
    //保存先ディレクトリ　=　（vadsim.exeがあるフォルダ）\\exportsettings
    QString save_dir_path(QCoreApplication::applicationDirPath() + "/exportsettings");
    QDir save_dir(save_dir_path);
    //保存先ディレクトリが存在しない場合は作成する．
    if ( !save_dir.exists() ) {
        save_dir.mkdir(save_dir_path);
    }
    QString save_name = save_dir_path + "/" + data->m_settingName + ".eds";
    xml.SaveFile(save_name.toStdString().c_str());
    return true;
}

/**
 * @brief vsDataExporterSetVal::on_pushButton_edit_clicked
 */
void vsDataExporterSetVal::on_pushButton_edit_clicked()
{
    boost::shared_ptr<vsExportDataInfo> data(new vsExportDataInfo);
    if ( data.get() == 0 ) return;
    QString org_settingname = ui->comboBox->currentText();
    if ( org_settingname.length() == 0 ) return;
    data->m_data = m_data[org_settingname]->m_data;
    data->m_settingName = m_data[org_settingname]->m_settingName;
    //設定ダイアログを起動
    vsDataExporterSetting dialog(data);
    int dialogRet = 0;
    for(;;) {
        dialogRet = dialog.exec();
        if( data->m_settingName.length() == 0  ) {
            data->m_settingName != org_settingname;
            QMessageBox::warning(this,QString(""),QString("Invalid Setting Name!"),QMessageBox::Ok);
        }
        else if ( (data->m_settingName != org_settingname) && (m_data.contains(data->m_settingName)) ) {
            //名前を変更した　かつ　変更後の名前と同じ名前が既に存在する
            data->m_settingName != org_settingname;
            QMessageBox::warning(this,QString(""),QString("Setting Name '") +data->m_settingName+ QString("' is Already Existed!"),QMessageBox::Ok);
        }
        else {
            break;
        }
    }
    if( dialogRet ) {
        vBlock("setting");
        //ダイアログでOKを押したときのみここが実行される
        m_data.insert(data->m_settingName,data);
        ui->comboBox->addItem(data->m_settingName);
        export_setting(data);
    }
}


/**
 * @brief vsDataExporterSetVal::getCurrentExportInfo
 * @param data
 */
void vsDataExporterSetVal::getCurrentExportInfo(boost::shared_ptr<vsExportDataInfo>& data) {

    vBlock("getCurrentExportInfo");

    QString currentText = ui->comboBox->currentText();

    if ( m_data.contains(currentText) ) {

        data = m_data[currentText];

    }
    else {
        vDebugVar(currentText);
    }

}

/**
 * @brief vsDataExporterSetVal::on_pushButton_clicked
 */
void vsDataExporterSetVal::on_pushButton_clicked() {

    vBlock("on_pushButton_clicked");

    QString file_name = QFileDialog::getSaveFileName(
                this,
                QString("Save Data as CSV"),
                QString("."),
                QString("csv (*.csv)")
                );

    QFile csv(file_name);

    if ( !csv.open(QIODevice::WriteOnly | QIODevice::Text) ) {
       vWarning("Can't open");
       return;
    }

    QTextStream stream(&csv);

    QLayout* tgtlayout = ui->verticalLayout_exportdata_area;


    if ( !tgtlayout ) {
        vMsg("tgtlayout is null");
        return;
    }

    boost::shared_ptr<vsExportDataInfo> data;
    getCurrentExportInfo(data);

    stream << "Setting Name:" << data->m_settingName << "\n";
    stream << "identification" << ","
           << "type" << ","
           << "info" << ","
           << "value" << "\n";


    for ( int i = 0; i < tgtlayout->count(); ++i ) {

        QLayout* export_ui_layout  = tgtlayout->itemAt(i)->layout();

        if ( !export_ui_layout ) {
            vDebugVar(export_ui_layout);
            return;
        }

        for ( int j = 0; j < export_ui_layout->count(); ++j ) {

              QWidget *widget = export_ui_layout->itemAt(j)->widget();
              vDebugVar( j );

              if (widget != NULL)
              {
                  QString cls_name = widget->metaObject()->className();
                    vDebugVar( cls_name );

                    if ( cls_name == "QLabel" ) {

                        vDebugVar( data->m_data[i]->info );
                        int id = vsExportDataFactory::getDataArrayID(data->m_data[i]->type,data->m_data[i]->info);

                        stream << data->m_data[i]->identification << ","
                               << vsExportDataFactory::name_type[data->m_data[i]->type] << ","
                               << vsExportDataFactory::names[data->m_data[i]->type][id] << ",";

                    }
                    else if ( cls_name == "QLineEdit" ){

                        QLineEdit *lineedit = reinterpret_cast<QLineEdit*>(widget);
                        vDebugVar( lineedit->text() );
                        stream << lineedit->text() << "\n";

                    }

              }
              else {
                  vMsg("widget is null");
              }

        }// for ( int j = 0; j < export_ui_layout->count(); ++j )

    } //for ( int i = 0; i < tgtlayout->count(); ++i )


}
