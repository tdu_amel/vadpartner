﻿#ifndef VSIMAGE_H
#define VSIMAGE_H
//#include <boost/shared_ptr.hpp>

struct vsImage
{
    unsigned int m_iWidth;//
    unsigned int m_iHeight;//
    unsigned int m_iStride;//画素値の割り当てビット数
    //boost::shared_ptr<unsigned char> m_spData;
    bool m_bBigEndian;

    vsImage():m_iWidth(0),m_iHeight(0),m_iStride(0){}
    virtual~ vsImage(){}

};

#endif // VSIMAGE_H
