﻿#ifndef VSIMAGEDICOM_H
#define VSIMAGEDICOM_H
#include "vsImage.h"



struct vsImageDICOM:public vsImage
{

    int m_iWindowCenter;
    int m_iWindowWidth;
    double m_dPixelSpacing[2];///<ピクセル間距離
    double m_dSliceThin;///<スライス厚み
    bool m_iMono2;
    int m_iInstanceID;///<スライスのID

    double m_iX;
    double m_iY;
    double m_iZ;
    vsImageDICOM():vsImage(),m_iWindowCenter(0),m_iWindowWidth(0),m_iMono2(true),m_iInstanceID(0),m_iX(0),m_iY(0),m_iZ(0){}
    virtual~ vsImageDICOM(){}


};
#endif // VSIMAGEDICOM_H
