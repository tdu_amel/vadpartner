﻿#include "vslungdataconstructor.h"
#include <vtkImageData.h>
#include <vtkSmartPointer.h>

vsLungDataConstructor::vsLungDataConstructor()
{


}

/*
 *    //lung face model


    vtkSmartPointer<vtkImageData> volume =
      vtkSmartPointer<vtkImageData>::New();
    volume->DeepCopy(srcImg);


    //#define DEBUG_EXTRACT

        short lower = -2048;
        short upper = -250;

        vsOrganExtraction lungExtractor;
        lungExtractor.setImage(volume);


        vtkSmartPointer<vtkImageData> dstImage;
        lungExtractor.extract(lower,upper,dstImage);



    #ifdef DEBUG_EXTRACT

        {

            int axis_coronal[] = {1,0,0};
            int axis_axial[] = {0,0,-1};
            int axis_saggital[] = {0,1,0};

            saveDICOM(QString::fromLocal8Bit("Lung_extract"),QString::fromLocal8Bit("Coronal"),dstImage,0,1,-90,axis_coronal);

            saveDICOM(QString::fromLocal8Bit("Lung_extract"),QString::fromLocal8Bit("Axial"),dstImage,0,1,0,axis_axial);

            saveDICOM(QString::fromLocal8Bit("Lung_extract"),QString::fromLocal8Bit("Saggital"),dstImage,0,1,90,axis_saggital);

        }


        {

            int axis_coronal[] = {1,0,0};
            int axis_axial[] = {0,0,-1};
            int axis_saggital[] = {0,1,0};


            saveDICOM(QString::fromLocal8Bit("Lung_none"),QString::fromLocal8Bit("Coronal"),volume,-1024,1024,-90,axis_coronal);

            saveDICOM(QString::fromLocal8Bit("Lung_none"),QString::fromLocal8Bit("Axial"),volume,-1024,1024,0,axis_axial);

            saveDICOM(QString::fromLocal8Bit("Lung_none"),QString::fromLocal8Bit("Saggital"),volume,-1024,1024,90,axis_saggital);

        }
    #endif
        dscImg = dstImage;
 */

void vsLungDataConstructor::constructModel(vtkSmartPointer<vtkActor> &actor, vtkImageData *srcImg, double w, double l) {
    vtkSmartPointer<vtkImageData> dstImg = vtkSmartPointer<vtkImageData> ::New();
    constructImageData(dstImg,srcImg,w,l);
}

void vsLungDataConstructor::constructImageData(
        vtkSmartPointer<vtkImageData>& dstImg,
        vtkImageData *srcImg,
        double w,double l) {
/*

    vtkSmartPointer<vtkMarchingCubes> surface_lung =
        vtkSmartPointer<vtkMarchingCubes>::New();


    surface_lung->SetInputData(dscImg);

    surface_lung->ComputeNormalsOn();
    surface_lung->SetValue(0, 1);


    vtkSmartPointer<vtkPolyDataMapper> mapper_lung =
        vtkSmartPointer<vtkPolyDataMapper>::New();
    mapper_lung->SetInputConnection(surface_lung->GetOutputPort());
    mapper_lung->ScalarVisibilityOff();
    mapper_lung->Update();

    //actor
    actor_lung = vtkSmartPointer<vtkActor>::New();
    actor_lung->SetMapper(mapper_lung);
    actor_lung->GetProperty()->SetColor( 0.5, 1, 0.8 );
    actor_lung->GetProperty()->SetOpacity( 0.3 );


    //選択できないようにする
    actor_lung->SetPickable(false);

    m_renderer[0]->AddActor(actor_lung); //set to m_renderer



    vsTreeItem* treeitemLung = new vsTreeItem(0,QStringList(QString("Lung")));
    treeitemLung->m_prop = actor_lung;
    m_treeItems.push_back(treeitemLung);
    treeitemHumanBody->addChild(treeitemLung);
*/
}
