﻿#ifndef MYINTERACTORSTYLEMOVE_H
#define MYINTERACTORSTYLEMOVE_H

#include "vsvtkinitializer.h"
#include <QDebug.h>
#include <list>
#include <qvtkWidget.h>
#include <vtkAbstractPicker.h>
#include <vtkAbstractPropPicker.h>
#include <vtkActor.h>
#include <vtkAssemblyPath.h>
#include <vtkInteractorEventRecorder.h>
#include <vtkInteractorObserver.h>
#include <vtkInteractorStyleTrackballActor.h>
#include <vtkMatrix4x4.h>
#include <vtkObserverMediator.h>
#include <vtkPickingManager.h>
#include <vtkPropCollection.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <vtkTransform.h>
#include "ui_mainwindow.h"
#include "vMsg.h"
#include "vsSTLManager.h"
#include "vsTreeItem.h"
#include "vsTreeItemContainer.h"


class vsActorRelocateInteractor : public vtkInteractorStyleTrackballActor
{
private:
    Ui_VadSim *ui;
    vsTreeItemContainer* m_container;
    vsSTLManager* m_stlManager;
    bool m_pressed;
    vtkActor* m_selected_actor;
public:
     vsActorRelocateInteractor();

     virtual ~vsActorRelocateInteractor(){}

     static vsActorRelocateInteractor* New();

     vtkTypeMacro(vsActorRelocateInteractor, vtkInteractorStyleTrackballActor)

     void setContainer(vsTreeItemContainer* container);

     void setSTLManager(vsSTLManager* stlManager);

     void setSelectedActorName(vtkProp* actor);

     void setUI(Ui_VadSim *_ui);

     Ui_VadSim* getUI();

     void conveyTransform(vtkProp* root_actor);


     //オーバーライド関数
     void OnMiddleButtonDown(){}
     void OnMiddleButtonUp(){}

     void OnRightButtonDown();
     void OnRightButtonUp();

    void OnMouseMove() {
        vtkInteractorStyleTrackballActor::OnMouseMove();
        if( m_selected_actor != NULL ) {
            //vMsg("Move");
            conveyTransform(m_selected_actor);
        }
    }

     void setRadioButtonState(vtkProp* actor) {

         if( actor == NULL || ui == NULL ) {

            return;

         }


         std::list< vsTreeItem* >::iterator itr;
         m_container->findItem(itr,actor);
         if( itr == m_container->end()) return;

         if( (*itr)->m_bHasRel ) ui->radioButton_rel->setEnabled(true);
         else  ui->radioButton_rel->setEnabled(false);

         if( (*itr)->m_coord_mode == VCM_ABSOLUTE ) ui->radioButton_abs->setChecked(true);
         else   ui->radioButton_rel->setChecked(true);

         m_container->unselectAllItems();
         (*itr)->setSelected(true);

     }



     void setSTLPosition(vtkProp* actor);

     /**
      * @brief setPosEdit
      */
     void setPosEdit();



     /**
      * @brief setSTLAngle
      * @param actor
      */
     void setSTLAngle(vtkProp* actor);


     /**
      * @brief OnLeftButtonDown
      */
     void OnLeftButtonDown();

     /**
      * @brief OnLeftButtonUp
      */
     void OnLeftButtonUp();

private:

     void convey(vtkTransform* transform_stack, vtkMatrix4x4* parent_mat, vsTreeItem* item);

};


#endif // MYINTERACTORSTYLEMOVE_H
