﻿#ifndef VSDICOMTOBITMAPSAVER_H
#define VSDICOMTOBITMAPSAVER_H
#include <QString>
#include <stdio.h>
#include <vtkSmartPointer.h>

class vtkImageData;

class vsDicomToImgSaver
{
public:
    vsDicomToImgSaver();

    /**
     * @brief vsDicomToBitMapSaver::saveDICOM　dataName/AxisNameのディレクトリにCTをBMPとして出力する
     * @param dataName
     * @param AxisName
     * @param image
     * @param ct_to_black　画像出力時に黒いピクセルにするCT値(この値より大きいのCT値も黒として出力される)
     * @param ct_to_white　画像出力時に白いピクセルにするCT値(この値未満のCT値も白として出力される)
     * @param slice_face_axis 積み上げられたスライスデータを、再度切り取るための面の軸
     * @param slice_face_angle 積み上げられたスライスデータを、再度切り取るための面の角度
     */
    static void saveDICOM(QString& dataName,QString& AxisName,vtkImageData* image,
                                                int ct_to_black,int ct_to_white,int slice_face_axis[3],double slice_face_angle);

    /**
     * @brief saveDICOMasText
     * @param dstImage
     * @param f
     * @param minz 開始Slice番号
     * @param maxz　終了Slice番号 -1を指定することで最後のスライスまでを指定したのと同じ意味に
     */
    static void saveDICOM_As_Text(vtkSmartPointer<vtkImageData>& dstImage,const char* fileName,int minz,int maxz = -1 );


    static void saveDICOMEmph(QString save_name,QString AxisName,vtkImageData* image,
                                        int ct_to_black, int ct_to_white,float coef,int axis[3],double angle);

};

#endif // VSDICOMTOBITMAPSAVER_H
