﻿#ifndef VSIMAGEGROWING_H
#define VSIMAGEGROWING_H
#include "vsvtkinitializer.h"
#include <deque>
#include <vtkSmartPointer.h>
#include <vtkImageData.h>
#include <vector>

struct SeedPoint {
  int x,y,z;
};

struct SeedCheckDir {
    int x,y,z;
};

class vsImageGrowing
{
protected:
    vtkSmartPointer<vtkImageData> m_Img;
public:
    vsImageGrowing();
    virtual ~vsImageGrowing();
    void setInputImage(vtkSmartPointer<vtkImageData>& imageData);

    template<class T>
    void execute(std::deque<SeedPoint>& seedPoints,std::deque<SeedCheckDir> &checkDirs, T &checker);

};

#endif // VSIMAGEGROWING_H
