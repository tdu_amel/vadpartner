﻿#ifndef VSMACRO_H
#define VSMACRO_H
#include <QtCore/QtGlobal>
#include <QDebug.h>
#ifdef MSC_VER
#define VS_ERROR_MSG2(strMsg) vMsg("%s(%d) %s:%s",__FILE__,LINE__,__FUNCTION__,strMsg)

#define VS_ERROR_MSG(msg) VS_ERROR_MSG2(#msg)
#else
#define VS_ERROR_MSG(msg)
#endif

#if QT_VERSION >= 0x050000
#  define GET_CSTR_FROM_QSTRING(x) x.toStdString().c_str()
#else
#  define GET_CSTR_FROM_QSTRING(x) x.toAscii().data()
#endif // VSMACRO_H
