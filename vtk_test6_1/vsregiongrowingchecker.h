#ifndef VSREGIONGROWINGCHECKER_H
#define VSREGIONGROWINGCHECKER_H
#include "vsimagegrowing.h"
#include <vector>

class vsRegionGrowingChecker
{
protected:
    std::vector< unsigned char > m_labelTable;
public:
    vsRegionGrowingChecker();
    virtual~ vsRegionGrowingChecker();
    void initDstImg(vtkSmartPointer<vtkImageData> &dstImage);
    bool ckeck(SeedPoint& p,SeedCheckDir& dir,vtkSmartPointer<vtkImageData> &dstImage,vtkSmartPointer<vtkImageData> &srcImage);
    bool isInImage(SeedPoint& p,int extent[6]);
    void replaceLabel(vtkSmartPointer<vtkImageData> &dstImage);
};

#endif // VSREGIONGROWINGCHECKER_H
