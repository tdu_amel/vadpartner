﻿#ifndef myMouseInteractorStyle_H
#define myMouseInteractorStyle_H
#include "vsvtkinitializer.h"
#include <vtkAngleRepresentation3D.h>
#include <vtkAngleWidget.h>
#include <vtkDistanceRepresentation3D.h>
#include <vtkDistanceWidget.h>
#include <vtkInteractorStyleJoystickActor.h>
#include <vtkInteractorStyleJoystickCamera.h>
#include <vtkInteractorStyleTrackballActor.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkSmartPointer.h>
#include "vstreeitemcontainer.h"
#include "ui_mainwindow.h"


// Define interaction style
class vsMouseInteractorStyle : public vtkInteractorStyleTrackballCamera
{
private:
    vtkDistanceRepresentation* distRep;
    vtkAngleRepresentation* angleRep;

  public:
    static vsMouseInteractorStyle* New();
    vtkTypeMacro(myMouseInteractorStyle, vtkInteractorStyleTrackballCamera)

    vsMouseInteractorStyle():distRep(0),angleRep(0) {



    }




    void setDistRepresetation(vtkDistanceRepresentation *_rep) {

        distRep = _rep;

    }



    void setAngleRepresetation(vtkAngleRepresentation *_rep) {

        angleRep = _rep;

    }

};





#endif // myMouseInteractorStyle_H
