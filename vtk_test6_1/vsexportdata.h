﻿#ifndef VSEXPORTDATA_H
#define VSEXPORTDATA_H
#include <QString>
#include <QVariant>
#include <QVector>
#include <boost/shared_ptr.hpp>
#include <QCoreApplication>
#include <QDateTime>

struct vsExportData {

    QString identification;
    enum DataType {
        DataType_DAY = 0,
        DataType_STL,
        DataType_HUMANMODEL,
        DataType_DICOM,
        DataType_MEASUREMENT,
        DataType_CAMERA,
        DataType_USERINPUT
    };
    DataType type;

    //もっとうまいやり方がありそう あとはC++11な感じでint を継承させたい
    enum DataValInfo {
        VSEXPORTDATAVALINFO_INVALID = -1,
        VSEXPORTDATAVALINFO_DAY_DATA_START = 0,
        VSEXPORTDATAVALINFO_DAY_DICOM_YYYYMMDD,
        VSEXPORTDATAVALINFO_DAY_TODAY_YYYYMMDD,
        VSEXPORTDATAVALINFO_DAY_TODAY_YYYYMMDD_HHMMSS,
        VSEXPORTDATAVALINFO_DAY_EXPORT_TIME_YYYYMMDD,
        VSEXPORTDATAVALINFO_DAY_EXPORT_TIME_YYYYMMDD_HHMMSS,
        VSEXPORTDATAVALINFO_DAY_USER_INPUT_YYYYMMDD,
        VSEXPORTDATAVALINFO_DAY_USER_INPUT_YYYYMMDD_HHMMSS,
        VSEXPORTDATAVALINFO_DAY_DATA_END,
        VSEXPORTDATAVALINFO_STL_DATA_START,
        VSEXPORTDATAVALINFO_STL_FILE_NAME,
        VSEXPORTDATAVALINFO_STL_EULER_ANGLE_OF_A_STL_MODEL_DEG3,
        VSEXPORTDATAVALINFO_STL_WORLD_TRANSFORM_MATRIX_OF_A_MODEL,
        VSEXPORTDATAVALINFO_STL_DATA_END,
        VSEXPORTDATAVALINFO_HUMANMODEL_DATA_START,
        VSEXPORTDATAVALINFO_HUMANMODEL_SOURCE_DATA_DIRECTORY_NAME,
        VSEXPORTDATAVALINFO_HUMANMODEL_EULER_ANGLE_OF_A_HUMAN_MODEL_DEG3,
        VSEXPORTDATAVALINFO_HUMANMODEL_WORLD_TRANSFORM_MATRIX_OF_A_MODEL,
        VSEXPORTDATAVALINFO_HUMANMODEL_LOCAL_ORIGIN_OF_A_MODEL,
        VSEXPORTDATAVALINFO_HUMANMODEL_DATA_END,
        VSEXPORTDATAVALINFO_DICOMDATA_DATA_START,
        VSEXPORTDATAVALINFO_DICOMDATA_DATA_DIRECTORY_NAME,
        VSEXPORTDATAVALINFO_DICOMDATA_2D_IMAGE_WIDTH,
        VSEXPORTDATAVALINFO_DICOMDATA_2D_IMAGE_HEIGHT,
        VSEXPORTDATAVALINFO_DICOMDATA_SLICE_THICKNESS,
        VSEXPORTDATAVALINFO_DICOMDATA_IMAGE_POSITION_PATIENT,
        VSEXPORTDATAVALINFO_DICOMDATA_IMAGE_ORIENTATION_PATIENT,
        VSEXPORTDATAVALINFO_DICOMDATA_WINDOW_CENTER,
        VSEXPORTDATAVALINFO_DICOMDATA_WINDOW_WIDTH,
        VSEXPORTDATAVALINFO_DICOMDATA_DATA_END,
        VSEXPORTDATAVALINFO_MEASUREMENT_DATA_START,
        VSEXPORTDATAVALINFO_MEASUREMENT_MEASURED_DISTANCE_MM,
        VSEXPORTDATAVALINFO_MEASUREMENT_MEASURED_ANGLE_DEG,
        VSEXPORTDATAVALINFO_MEASUREMENT_DATA_END,
        VSEXPORTDATAVALINFO_USERINPUT_DATA_START,
        VSEXPORTDATAVALINFO_USERINPUT_INTEGER,
        VSEXPORTDATAVALINFO_USERINPUT_MIXED_DECIMAL,
        VSEXPORTDATAVALINFO_USERINPUT_STRING,
        VSEXPORTDATAVALINFO_USERINPUT_DATA_END,
        VSEXPORTDATAVALINFO_CAMERA_DATA_START,
        VSEXPORTDATAVALINFO_CAMERA_CAMERA_EYE_POS,
        VSEXPORTDATAVALINFO_CAMERA_CAMERA_FIXATION_POS,
        VSEXPORTDATAVALINFO_CAMERA_CAMERA_UPVEC,
        VSEXPORTDATAVALINFO_CAMERA_CAMERA_MATRIX_VIEW_MATRIX,
        VSEXPORTDATAVALINFO_CAMERA_DATA_END

    };

    DataValInfo info;


};


struct vsExportDataInfo {
    QString m_settingName;
    QVector<boost::shared_ptr<vsExportData>> m_data;
    QDateTime m_lastModified;
};


class QVBoxLayout;
class MainWindow;

struct vsExportDataFactory {

public:


    //この辺ももっとうまいやり方がありそう
    static QString name_day[];

    static QString name_stl[];

    static QString name_humanmodel[];

    static QString name_dicomdata[];

    static QString name_measurement[];

    static QString name_camera[];

    static QString name_userinput[];

    static QString name_type[];

    static QString* names[];

    static vsExportData::DataValInfo infovalue_day[];
    static vsExportData::DataValInfo infovalue_stl[];
    static vsExportData::DataValInfo infovalue_humanmodel[];
    static vsExportData::DataValInfo infovalue_dicomdata[];
    static vsExportData::DataValInfo infovalue_measurement[];
    static vsExportData::DataValInfo infovalue_userinput[];
    static vsExportData::DataValInfo infovalue_camera[];


    static int vsExportDataFactory::getDataArrayID(vsExportData::DataType datatype,
                                                   vsExportData::DataValInfo info);
    static vsExportData::DataValInfo getDataInfoID(vsExportData::DataType datatype, int selectID );
    static vsExportData::DataValInfo getID( int selectID, vsExportData::DataValInfo* array, int len );
    static vsExportData::DataValInfo getDayID(int selectID );
    static vsExportData::DataValInfo getSTLID(int selectID );
    static vsExportData::DataValInfo getHumanModelID(int selectID );
    static vsExportData::DataValInfo getDICOMDataID(int selectID );
    static vsExportData::DataValInfo getMeasurementID(int selectID );
    static vsExportData::DataValInfo getCameraID(int selectID );
    static vsExportData::DataValInfo getUserInputID(int selectID );


    /**
     * @brief addExportDataUI
     * エクスポートするデータを入力するUIを指定されたVerticalLayoutに追加します．
     * 実際にはデータのタイプを見てそれに合ったaddExportDataUI_XXXを呼び出しているだけです．
     * @param ui_area　UIが追加されるVerticalLayout
     * @param data　addExportDataUI
     */
    static void addExportDataUI(MainWindow *mainwindow, QVBoxLayout **ui_area,
                                boost::shared_ptr<vsExportDataInfo>& data);

    /**
     * @brief addExportDataUI_Date
     * 日付データに関するエクスポート用UIを作ります．addExportDataUIから呼ばれます．
     * @param ui_area　UIが追加されるVerticalLayout
     * @param data　データが追加されるUI
     */
    static void addExportDataUI_Date(QVBoxLayout* ui_area,boost::shared_ptr<vsExportData>& data);

    static void addExportDataUI_Measurement(MainWindow* mainwindow,QVBoxLayout* ui_area,
                                            boost::shared_ptr<vsExportData>& data);
};



#endif // VSEXPORTDATA_H
