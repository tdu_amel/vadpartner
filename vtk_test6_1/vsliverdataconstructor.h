﻿#ifndef VSLEVERDATACONSTRUCTOR_H
#define VSLEVERDATACONSTRUCTOR_H
#include "vsvtkinitializer.h"
#include <vtkStringArray.h>
#include <vtkMedicalImageReader2.h>
#include <vtkMedicalImageProperties.h>
#include <vtkStringArray.h>
#include <vtkSmartPointer.h>
#include <vtkActor.h>
#include "vsOrganExtraction.h"
/**
 * @brief The vsLiverDataConstructor class 肝臓の3Dデータ構築を行う
 */
class vsLiverDataConstructor:public vsOrganExtraction
{

public:
    vsLiverDataConstructor();

    /**
     * @brief constructImageData 肝臓抽出用に画像を多少加工
     * @param dstImg　変換後の画像が入るスマートポインタ。元データをディープコピーしてから変換する。
     * @param srcImg　入力画像。こちらは変更されないので、このあと別の処理にそのまま渡すことができる。
     * @param w　WindowWidth　CT画像のグレースケール変換に使う。変換に使うCT値の幅。 [l-w,l+w]の値がそれぞれ黒～白になる
     * @param l WindowLevel　CT画像のグレースケール変換に使う。変換に使うCT値の中央の値。
     */
    static void constructImageData(vtkSmartPointer<vtkImageData>& dstImg,vtkImageData *srcImg,double w,double l);

    /**
     * @brief constructModel 肝臓のみをモデル化。
     * @param actor_liver　結果の3Dデータ(vtkのアクター)が入るスマートポインタ
     * @param srcImg　もとになるCTデータ群
     * @param w　CT画像のグレースケール変換に使う。変換に使うCT値の幅。 [l-w,l+w]の値がそれぞれ黒～白になる
     * @param l　CT画像のグレースケール変換に使う。変換に使うCT値の中央の値。
     */
    static void constructModel(vtkSmartPointer<vtkActor> &actor_liver, vtkImageData *srcImg, double w, double l);

};

#endif // VSLEVERDATACONSTRUCTOR_H
