﻿#ifndef VSORGANEXTRACTION_H
#define VSORGANEXTRACTION_H
#include "vsvtkinitializer.h"
#include <vtkImageThresholdConnectivity.h>
#include <vtkDICOMImageReader.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkImageData.h>
#include <vtkSmartPointer.h>
#include <QString.h>

template<class T>
class vsLineElement {
 public:
    vtkImageData* m_img;
    T* p;
    int x,y,z;
    int dims[3];

    vsLineElement():p(0),m_img(0) {


    }

    T& operator = (const T* a) {
         p = a;
    }

    operator T*() const{
        return p;
    }

    bool operator == (const short a) const {
            return p == a;
    }

    bool isSamePosition(const vsLineElement& p) const {
            return x == p.x && y == p.y && z == p.z;
    }

    inline vsLineElement& shift(int dx,int dy, int dz) {

        x += dx;
        y += dy;
        z += dz;

       p += dx+(dy + dz*dims[1])*dims[0];


        // p = (T*)m_img->GetScalarPointer(x,y,z);

        return *this;

    }

    inline vsLineElement& setPixel(vtkImageData* _img,const int _x,const int _y,const int _z) {


            m_img = _img;
            x = _x;
            y = _y;
            z = _z;
            m_img->GetDimensions(dims);


            p = (T*)m_img->GetScalarPointer(x,y,z);

        return *this;
    }

};

struct vsLine {

    vsLineElement<short> left;
    vsLineElement<short> right;

};

class vsOrganExtraction
{
private:

    vtkSmartPointer<vtkImageData> m_srcImage;

    int m_dims[3];
public:
    vsOrganExtraction();
    virtual ~vsOrganExtraction();

    bool extract( short lower, short upper, vtkSmartPointer<vtkImageData>& dstImage );
    void setImageFromReader(vtkSmartPointer<vtkDICOMImageReader>& reader);
    void setImage(vtkSmartPointer<vtkImageData>& image);
protected:
    void shift(vsLineElement<short> &line, vsLineElement<short> &dst, int dx, int dy, int dz);
    short* getshiftVal(vsLineElement<short> &p, int dx, int dy, int dz);
};


void debug_ct(vtkSmartPointer<vtkImageData>& dstImage,const char* f,int minz=0,int maxz=-1 );

#endif // VSORGANEXTRACTION_H
