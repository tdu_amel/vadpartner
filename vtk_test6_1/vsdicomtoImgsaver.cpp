﻿#include "vsdicomtoImgsaver.h"
#include "vsDicomGrayScaller.h"
#include <QDir>
#include <QString>
#include <QFileInfo>
#include <vtkBMPWriter.h>
#include <vtkPNGWriter.h>
#include <vtkTransform.h>
#include <vtkImageData.h>
#include <vtkImageReslice.h>
#include <vtkImageCast.h>
#include "vsDicomGrayScaller.h"
#include "vsHistgramEmphasize.h"

vsDicomToImgSaver::vsDicomToImgSaver()
{

}



void vsDicomToImgSaver::saveDICOM(QString& dataName,QString& AxisName,vtkImageData* image,int ct_to_black,int ct_to_white,int slice_face_axis[3],double slice_face_angle) {

    vtkSmartPointer<vtkImageData> dstImg;
    dstImg = vtkSmartPointer<vtkImageData>::New();
    dstImg->DeepCopy(image);

    double bounds[6];
    dstImg->GetBounds(bounds);

    // Rotate about the center of the image
    vtkSmartPointer<vtkTransform> transform =
      vtkSmartPointer<vtkTransform>::New();

    // Compute the center of the image
    double center[3];
    center[0] = (bounds[1] + bounds[0]) / 2.0;
    center[1] = (bounds[3] + bounds[2]) / 2.0;
    center[2] = (bounds[5] + bounds[4]) / 2.0;

    // Rotate about the center
    transform->Translate(center[0], center[1], center[2]);
    transform->RotateWXYZ(slice_face_angle, slice_face_axis[0], slice_face_axis[1], slice_face_axis[2]);
    transform->Translate(-center[0], -center[1], -center[2]);

    // Reslice does all of the work
    vtkSmartPointer<vtkImageReslice> reslice =
      vtkSmartPointer<vtkImageReslice>::New();
    reslice->SetInputData(dstImg);
    reslice->SetResliceTransform(transform);
    reslice->SetInterpolationModeToCubic();

    reslice->SetOutputSpacing(
      dstImg->GetSpacing()[0],
      dstImg->GetSpacing()[1],
      dstImg->GetSpacing()[2]);

    reslice->SetOutputOrigin(
      dstImg->GetOrigin()[0],
      dstImg->GetOrigin()[1],
      dstImg->GetOrigin()[2]);

    reslice->SetOutputExtent(
      dstImg->GetExtent());
    reslice->Update();


    QDir saveDirName(dataName);
    QDir saveDirAxis(saveDirName.absolutePath() + QString("/")+AxisName);

    if (!saveDirName.exists()) {
        saveDirName.mkdir(saveDirName.absolutePath());
    }

    if (!saveDirAxis.exists()) {
        saveDirAxis.mkdir(saveDirAxis.absolutePath());
    }

    vtkSmartPointer<vtkImageCast> cast = vtkSmartPointer<vtkImageCast>::New();
    cast->SetInputData(reslice->GetOutput());
    cast->SetOutputScalarTypeToShort();
    cast->Update();


    vtkSmartPointer<vtkImageData> rawBmpData;
    vsDicomGrayScaller colChange;
    double wc = (ct_to_black + ct_to_white)/2.0;
    double wl = ct_to_white - ct_to_black;
    colChange.setWCWL(wc,wl);
    colChange.getPixels(rawBmpData,cast->GetOutput(),false);

    QString fileName = saveDirAxis.absolutePath();
    fileName += "/" + dataName+ "_%04d.bmp";
    QFileInfo info(fileName);

    vtkSmartPointer<vtkPNGWriter> writer = vtkSmartPointer<vtkPNGWriter>::New();
    writer->SetFilePattern(info.absoluteFilePath().toStdString().c_str());
    writer->SetInputData(rawBmpData);
    writer->Write();

}


void vsDicomToImgSaver::saveDICOM_As_Text(vtkSmartPointer<vtkImageData>& dstImage, const char* fileName, int minz, int maxz ) {

    FILE *stream;
    int* dims = dstImage->GetDimensions();

    if( maxz == -1 )maxz = dims[2];

    for( int z = minz; z < maxz; ++z ) {
        char fname[261];
        sprintf(fname,"%s%d.txt",fileName,z);
        fopen_s( &stream, fname, "w" );
        for( int y = 0; y < dims[1]; ++y ) {

            for( int x = 0; x < dims[0]; ++x ) {

                short* p = (short*)(dstImage->GetScalarPointer(x,y,z));
                fprintf(stream,"%d",*p);

            }
            fprintf(stream,"\n");
        }
        fclose(stream);
    }

}



void vsDicomToImgSaver::saveDICOMEmph(QString save_name, QString AxisName, vtkImageData* image, int ct_to_black, int ct_to_white, float coef, int axis[3], double angle) {

        vsHistgramEmphasize emph;

        int width = (ct_to_white+ct_to_black);
        int wcenter = width/2;

        int o_width = width * coef;

        int o_lower = wcenter - o_width / 2;
        int o_upper = wcenter + o_width / 2;

        emph.setInputInfo(ct_to_black,ct_to_white);
        emph.setOutputInfo(o_lower,o_upper);
        vtkSmartPointer<vtkImageData> dstImg;
        emph.execute( dstImg,image );

        double bounds[6];
        dstImg->GetBounds(bounds);

        // Rotate about the center of the image
        vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();

        // Compute the center of the image
        double center[3];
        center[0] = (bounds[1] + bounds[0]) / 2.0;
        center[1] = (bounds[3] + bounds[2]) / 2.0;
        center[2] = (bounds[5] + bounds[4]) / 2.0;

        // Rotate about the center
        transform->Translate(center[0], center[1], center[2]);
        transform->RotateWXYZ(angle, axis[0], axis[1], axis[2]);
        transform->Translate(-center[0], -center[1], -center[2]);

        // Reslice does all of the work
        vtkSmartPointer<vtkImageReslice> reslice = vtkSmartPointer<vtkImageReslice>::New();
        reslice->SetInputData(dstImg);
        reslice->SetResliceTransform(transform);
        reslice->SetInterpolationModeToCubic();
        reslice->SetOutputSpacing(
          dstImg->GetSpacing()[0],
          dstImg->GetSpacing()[1],
          dstImg->GetSpacing()[2]);
        reslice->SetOutputOrigin(
          dstImg->GetOrigin()[0],
          dstImg->GetOrigin()[1],
          dstImg->GetOrigin()[2]);

        reslice->SetOutputExtent(
          dstImg->GetExtent());
        reslice->Update();


        QString save_dir_axis_name = save_name + "/" + AxisName;

        QDir save_dir(save_name);
        if ( !save_dir.exists() ) {
            save_dir.mkdir(save_name);
        }

        QDir save_dir_axis(save_dir_axis_name);
        if ( !save_dir_axis.exists() ) {
            save_dir_axis.mkdir(save_dir_axis_name);
        }

        vtkSmartPointer<vtkImageCast> cast = vtkSmartPointer<vtkImageCast>::New();
        cast->SetInputData(reslice->GetOutput());
        cast->SetOutputScalarTypeToShort();
        cast->Update();

        QString fileName = save_dir_axis_name + "/" + save_name +"_emph"+ QString::number(coef) +"_%04d.bmp";


        vtkSmartPointer<vtkImageData> outputImage;
        vsDicomGrayScaller colChange;
        colChange.setWCWL(0,400);
        colChange.getPixels(outputImage,cast->GetOutput(),false);

        vtkSmartPointer<vtkBMPWriter> writer = vtkSmartPointer<vtkBMPWriter>::New();
        writer->SetFilePattern(fileName.toStdString().c_str());
        writer->SetInputData(outputImage);
        writer->Write();


}



