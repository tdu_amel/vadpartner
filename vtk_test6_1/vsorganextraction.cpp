﻿#include "vsorganextraction.h"
#include <vtkImageData.h>
#include <vtkImageThreshold.h>
#include <vtkPointData.h>
#include <vtkDataArray.h>
#include <boost/shared_ptr.hpp>
#include <queue>
#include <vtkTransform.h>
#include <vtkImageReslice.h>
#include <vtkImageCast.h>
#include <vtkBMPWriter.h>
#include <windows.h>
#include "vsDicomGrayScaller.h"
#include "vMsg.h"





vsOrganExtraction::vsOrganExtraction()
{

    m_dims[0] = m_dims[1] = m_dims[2] = 0;

}


vsOrganExtraction::~vsOrganExtraction() {



}



void vsOrganExtraction::setImageFromReader(vtkSmartPointer<vtkDICOMImageReader>& reader) {

    m_srcImage = vtkSmartPointer<vtkImageData>::New();
    m_srcImage->DeepCopy(reader->GetOutput());

}



void vsOrganExtraction::setImage(vtkSmartPointer<vtkImageData>& image) {

    m_srcImage = vtkSmartPointer<vtkImageData>::New();
    m_srcImage->DeepCopy(image);

}




void vsOrganExtraction::shift(vsLineElement<short> &p,vsLineElement<short> &dst, int dx, int dy, int dz) {


    dst.setPixel(p.m_img,p.x+dx,p.y+dy,p.z+dz);


}


short* vsOrganExtraction::getshiftVal(vsLineElement<short> &p, int dx, int dy, int dz) {

    return (short*)p.m_img->GetScalarPointer(p.x+dx,p.y+dy,p.z+dz);

}


#include <QDateTime>
#include <QtCore/QtGlobal>
void saveQueue( std::queue< boost::shared_ptr<vsLine> > &lineQueueDown,const char* fname) {


    QDateTime dateTime = QDateTime::currentDateTimeUtc();
    QString datestr = dateTime.date().toString("yyyy_MM_dd");
    QString timestr = dateTime.time().toString("hh_mm_ss");


    char fileName[261];
#if QT_VERSION >= 0x050000
    sprintf(fileName,"%s(%s %s).txt",fname,datestr.toStdString().c_str(),timestr.toStdString().c_str());
#else
    sprintf(fileName,"%s(%s %s).txt",fname,datestr.toStdString().c_str(),timestr.toStdString().c_str());
#endif


    std::queue< boost::shared_ptr<vsLine> > cpyQueue(lineQueueDown);



    std::vector< boost::shared_ptr<vsLine> > t;

    while( cpyQueue.size() > 0 ) {

        t.push_back( cpyQueue.front() );
        cpyQueue.pop();

    }


    FILE *stream;
    errno_t err = fopen_s( &stream, fileName, "w" );
    if( err != 0 )
    {
       printf( "The file '%s' was opened\n",fileName );
       return;
    }


    for(unsigned int newidx = 0; newidx < t.size(); ++newidx  ) {

           fprintf(stream,"[%d](%d,%d)-(%d,%d)\n",newidx,t[newidx]->left.x,t[newidx]->left.y,t[newidx]->right.x,t[newidx]->right.y);

    }

    if( stream )
     fclose(stream);


}


void saveDICOM2(QString& name,QString& AxisName,vtkImageData* image,int lower,int upper,double angle,int axis[3]) {

    vtkSmartPointer<vtkImageData> dstImg;
    dstImg = vtkSmartPointer<vtkImageData>::New();
    dstImg->DeepCopy(image);

    double bounds[6];
    dstImg->GetBounds(bounds);

    // Rotate about the center of the image
    vtkSmartPointer<vtkTransform> transform =
      vtkSmartPointer<vtkTransform>::New();

    // Compute the center of the image
    double center[3];
    center[0] = (bounds[1] + bounds[0]) / 2.0;
    center[1] = (bounds[3] + bounds[2]) / 2.0;
    center[2] = (bounds[5] + bounds[4]) / 2.0;

    // Rotate about the center
    transform->Translate(center[0], center[1], center[2]);
    transform->RotateWXYZ(angle, axis[0], axis[1], axis[2]);
    transform->Translate(-center[0], -center[1], -center[2]);

    // Reslice does all of the work
    vtkSmartPointer<vtkImageReslice> reslice =
      vtkSmartPointer<vtkImageReslice>::New();
    reslice->SetInputData(dstImg);
    reslice->SetResliceTransform(transform);
    reslice->SetInterpolationModeToCubic();
    reslice->SetOutputSpacing(
      dstImg->GetSpacing()[0],
      dstImg->GetSpacing()[1],
      dstImg->GetSpacing()[2]);
    reslice->SetOutputOrigin(
      dstImg->GetOrigin()[0],
      dstImg->GetOrigin()[1],
      dstImg->GetOrigin()[2]);


    reslice->SetOutputExtent(
      dstImg->GetExtent());
    reslice->Update();

QString dir1 = name;
dir1 += "\\" + name;

QString dir2 = dir1;
dir2 += "\\" + AxisName;


    CreateDirectoryA(name.toStdString().c_str(),NULL);
    CreateDirectoryA(dir1.toStdString().c_str(),NULL);
    CreateDirectoryA(dir2.toStdString().c_str(),NULL);



    vtkSmartPointer<vtkImageCast> cast = vtkSmartPointer<vtkImageCast>::New();
    cast->SetInputData(reslice->GetOutput());
    cast->SetOutputScalarTypeToShort();
    cast->Update();

QString fileName = dir2;
fileName += "\\" + name+ "_%04d.bmp";
    vtkSmartPointer<vtkImageData> lastImage;
    vsDicomGrayScaller colChange;
    double wc = (lower + upper)/2.0;
    double wl = upper - lower;
    colChange.setWCWL(wc,wl);
    colChange.getPixels(lastImage,cast->GetOutput(),false);
   vtkSmartPointer<vtkBMPWriter> writer = vtkSmartPointer<vtkBMPWriter>::New();
#if QT_VERSION >= 0x050000
   writer->SetFilePattern(fileName.toStdString().c_str());
#else
   writer->SetFilePattern(fileName.toStdString().c_str());
#endif


   writer->SetInputData(lastImage);
   writer->Write();

}

bool vsOrganExtraction::extract( short lower, short upper, vtkSmartPointer<vtkImageData>& dstImage ) {



    if( m_srcImage.Get() == 0 ) {
        vMsg("vsOrganExtraction::extract()m_srcImage is null");
        return false;
    }



    vtkSmartPointer<vtkImageThreshold> imageThreshold =
      vtkSmartPointer<vtkImageThreshold>::New();
    imageThreshold->SetInputDataObject(0,m_srcImage);



    imageThreshold->ThresholdBetween(lower, upper);
    imageThreshold->ReplaceInOn();
    imageThreshold->ReplaceOutOn();
    imageThreshold->SetOutputScalarTypeToShort();
    imageThreshold->SetInValue(1);
    imageThreshold->SetOutValue(0);
    imageThreshold->Update();
    int axis_axial[] = {0,0,-1};
    saveDICOM2(QString::fromLocal8Bit("Lung_extract_tmp"),QString::fromLocal8Bit("Axial"),imageThreshold->GetOutput(),0,1,0,axis_axial);
    vtkSmartPointer<vtkImageData> binarized = vtkSmartPointer<vtkImageData>::New();
    binarized = imageThreshold->GetOutput();


    //vMsg("vsOrganExtraction::extract()");
    int* dims = binarized->GetDimensions();

    //大きさを保存
     m_dims[0] = dims[0];
     m_dims[1] = dims[1];
     m_dims[2] = dims[2];


     dstImage = vtkSmartPointer<vtkImageData>::New();
     dstImage->DeepCopy(binarized);

    boost::shared_ptr<vsLine> currentLine;
    boost::shared_ptr<vsLine> currentLineUp;
    std::queue< boost::shared_ptr<vsLine> > lineQueueDown;
    std::queue< boost::shared_ptr<vsLine> > lineQueueUp;


   short* start = (short*)(dstImage->GetScalarPointer(0,0,0));
   //debug_ct(dstImage,"src");

    for( int z = 0; z < dims[2]; ++z ) {

        //vMsg("z %d",z);

        int px1 = z*dims[1];
        for( int y = 0; y < dims[1]; ++y ) {


            int px2 = (y + px1)*dims[0];
            for( int x = 0; x < dims[0]; ++x ) {


                short* p = start+x+px2;


                if( *p == 1 ) {


                    currentLine.reset( new vsLine );
                    currentLine->right.setPixel(dstImage,x,y,z);
                    currentLine->left = currentLine->right;
                    lineQueueDown.push(currentLine);

                    currentLine.reset();
                    //saveQueue(lineQueueDown,"queue");
                    //vMsg("me (%d,%d)-(%d,%d)",currentLine->left.x,currentLine->left.y,currentLine->right.x,currentLine->right.y);
                    break;

                }
                break;

            }


        }


        int lastLineLX = 0;
        int lastLineRX = m_dims[0];

        int lastLineLXUp = 0;
        int lastLineRXUp = m_dims[0];
        while( lineQueueDown.size() > 0 || lineQueueUp.size() > 0) {

            if(lineQueueDown.size() > 0) {

                    //vMsg(QString::number(lineQueueDown.size()).toAscii());
                    boost::shared_ptr<vsLine> topLineDown;
                    topLineDown = lineQueueDown.front();



                    if( z == 0 ) {
                        static int newidx = 0;
                        char fnam_tmp[261];
                        sprintf(fnam_tmp,"tmpImg_%d(y:%d)_",newidx++,topLineDown->left.y);
                        //vMsg(fnam_tmp);
                        //debug_ct(dstImage,fnam_tmp,0,1);
                        //saveQueue(lineQueueDown,"queue");
                        //vMsg("top (%d,%d)-(%d,%d)",topLineDown->left.x,topLineDown->left.y,topLineDown->right.x,topLineDown->right.y);
                    }



                    lineQueueDown.pop();


                    //ラインを左に伸ばす
                    while( topLineDown->left.x > 0 && *(topLineDown->left.p) == 1 ) {

                        //画像からはみ出ないとする
                        --(topLineDown->left.x);
                        --(topLineDown->left.p);

                    }


                    //ラインを右に伸ばす
                    while( topLineDown->right.x < m_dims[0]-1 && *(topLineDown->right.p) == 1 ) {

                        ++(topLineDown->right.x);
                        ++(topLineDown->right.p);

                    }


                    //ラインを構成する各ピクセルの真下のピクセルのチェック
                    if( topLineDown->left.y < dims[1]-1 ) {

                        vsLineElement<short> p = topLineDown->left;

                        for( p.shift(0,1,0); p.x < m_dims[0]-1 && p.x <= topLineDown->right.x; p.shift(1,0,0) ) {

                                 //真下をチェック
                                 if( *(p.p) == 1 ) {
                                     //1なので削除対象

                                     if( currentLine.get() == 0 ) {

                                         //まだラインが作られてないので新しく作りましょう
                                         currentLine.reset( new vsLine );
                                         currentLine->left = p;
                                         currentLine->right = topLineDown->right;
                                         currentLine->right.shift(0,1,0);

                                     }

                                 } else {

                                     //0なので削除対象でない
                                     if( currentLine.get() != 0 ) {

                                         //すでにラインが作ってあった場合は，ラインが右端に到達したことを意味する
                                         currentLine->right = p;
                                         lineQueueDown.push(currentLine);

                                         currentLine.reset();

                                     }

                                }

                        }

                        if( currentLine.get() != 0 ) {

                            lineQueueDown.push(currentLine);

                            currentLine.reset();

                        }

                        //上のピクセルについても調べる
                       if( topLineDown->right.y > 0 ) {
                            vsLineElement<short> p = topLineDown->left;


                            for( p.shift(0,-1,0); p.x < m_dims[0]-1 && p.x <= topLineDown->right.x; p.shift(1,0,0) ) {

                                     //真上をチェック
                                     if( *(p.p) == 1 ) {
                                         //1なので削除対象

                                         if( currentLineUp.get() == 0 ) {

                                             //まだラインが作られてないので新しく作りましょう
                                             currentLineUp.reset( new vsLine );
                                             currentLineUp->left = p;
                                             currentLineUp->right = topLineDown->right;
                                             currentLineUp->right.shift(0,-1,0);

                                         }

                                     } else {

                                         //0なので削除対象でない
                                         if( currentLineUp.get() != 0 ) {

                                             //すでにラインが作ってあった場合は，ラインが右端に到達したことを意味する
                                             currentLineUp->right = p;
                                             lineQueueUp.push(currentLineUp);

                                             currentLineUp.reset();

                                         }

                                    }

                            }


                            if( currentLineUp.get() != 0 ) {

                                lineQueueUp.push(currentLineUp);

                                currentLineUp.reset();


                            }

                       }//if( (topLineDown->left.x < lastLineLX || topLineDown->right.x > lastLineRX) && topLineDown->right.y < dims[1]-1  )


                    }


                    lastLineLX = topLineDown->left.x;
                    lastLineRX = topLineDown->right.x;

                    //vMsg("ko (%d,%d)-(%d,%d)",topLineDown->left.x,topLineDown->left.y,topLineDown->right.x,topLineDown->right.y);
                    memset(topLineDown->left.p,0,sizeof(short)*(topLineDown->right.p - topLineDown->left.p+1));
                    //vMsg("ko?");

                }//if(lineQueueDown.size() > 0)


                //上の方向についてチェック
                if( lineQueueUp.size() > 0 ) {

                        boost::shared_ptr<vsLine> currentLineUp;
                        boost::shared_ptr<vsLine> topLineUp;
                        topLineUp = lineQueueUp.front();
                        lineQueueUp.pop();


                        //ラインを左に伸ばす
                        while( topLineUp->left.x > 0 && *(topLineUp->left.p) == 1 ) {

                            //画像からはみ出ないとする
                            topLineUp->left.shift(-1,0,0 );

                        }


                        //ラインを右に伸ばす
                        while( topLineUp->right.x < m_dims[0]-1 && *(topLineUp->right.p) == 1 ) {

                            topLineUp->right.shift(1,0,0);

                        }


                    //ラインを構成する各ピクセルの真上のピクセルのチェック
                    if( topLineUp->left.y > 0 ) {

                            vsLineElement<short> p = topLineUp->left;

                            for( p.shift(0,-1,0); p.x < m_dims[0]-1 && p.x <= topLineUp->right.x; p.shift(1,0,0) ) {

                                     //真下をチェック
                                     if( *(p.p) == 1 ) {
                                         //1なので削除対象

                                         if( currentLineUp.get() == 0 ) {

                                             //まだラインが作られてないので新しく作りましょう
                                             currentLineUp.reset( new vsLine );
                                             currentLineUp->left = p;
                                             currentLineUp->right = topLineUp->right;
                                             currentLineUp->right.shift(0,-1,0);

                                         }

                                     } else {

                                         //0なので削除対象でない
                                         if( currentLineUp.get() != 0 ) {

                                             //すでにラインが作ってあった場合は，ラインが右端に到達したことを意味する
                                             currentLineUp->right = p;
                                             lineQueueUp.push(currentLineUp);

                                             currentLineUp.reset();

                                         }

                                    }

                            }


                            if( currentLineUp.get() != 0 ) {

                                lineQueueUp.push(currentLineUp);

                                currentLineUp.reset();

                            }

/*
                            //前回調べた直線より左に長いor右に長いとき，↓のピクセルについても調べる
                           if( (topLineUp->left.x < lastLineLXUp || topLineUp->right.x > lastLineRXUp) && topLineUp->right.y < dims[1]-1  ) {

                                vsLineElement<short> p = topLineUp->left;
                                boost::shared_ptr<vsLine> currentLineDown;

                                for( p.shift(0,1,0); p.x < m_dims[0]-1 && p.x <= topLineUp->right.x; p.shift(1,0,0) ) {

                                         //真下をチェック
                                         if( *(p.p) == 1 ) {
                                             //1なので削除対象

                                             if( currentLineDown.get() == 0 ) {

                                                 //まだラインが作られてないので新しく作りましょう
                                                 currentLineDown.reset( new vsLine );
                                                 currentLineDown->left = p;
                                                 currentLineDown->right.x = -1;

                                             }

                                         } else {

                                             //0なので削除対象でない
                                             if( currentLineDown.get() != 0 ) {

                                                 //すでにラインが作ってあった場合は，ラインが右端に到達したことを意味する
                                                 currentLineDown->right = p;
                                                 lineQueueDown.push(currentLineDown);
                                                 currentLineDown.reset();

                                             }

                                        }

                                }

                           }//if( (topLineUp->left.x < lastLineLXUp || topLineDown->right.x > lastLineRXUp) && topLineUp->right.y < dims[1]-1  )
*/
                    }//if( topLineUp->left.y > 0 )


                    lastLineLXUp = topLineUp->left.x;
                    lastLineLXUp = topLineUp->right.x;

                    memset(topLineUp->left.p,0,sizeof(short)*(topLineUp->right.p -topLineUp->left.p+1));


              }



        }//while( lineQueueUp.size() > 0 )


    }

    //vMsg("Z End");

   //debug_ct(dstImage,"result");

    return true;
}

