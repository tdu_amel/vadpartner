﻿#ifndef VSHISTGRAMEMPHASIZE_H
#define VSHISTGRAMEMPHASIZE_H
#include "vsvtkinitializer.h"
#include <vtkSmartPointer.h>
#include <vtkImageData.h>

class vsHistgramEmphasize
{
private:
    int from[2];
    int to[2];
public:
    vsHistgramEmphasize();
    virtual ~vsHistgramEmphasize(){}
    void setInputInfo(int min,int max);
    void setOutputInfo(int min,int max);
    void execute(vtkSmartPointer<vtkImageData>& dst, vtkImageData *src);
};

#endif // VSHISTGRAMEMPHASIZE_H
