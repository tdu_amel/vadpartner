﻿#ifndef DATAEXPORTERSETVAL_H
#define DATAEXPORTERSETVAL_H

#include <QWidget>
#include <QHash>
#include <QString>
#include <boost/shared_ptr.hpp>
#include "vsdataexportersetting.h"

namespace Ui {
class DataExporterSetVal;
}

class MainWindow;

class vsDataExporterSetVal : public QWidget
{
    Q_OBJECT

public:
    explicit vsDataExporterSetVal(QWidget *parent = 0);
    ~vsDataExporterSetVal();

    bool event(QEvent *e);

    void setMainWindow(MainWindow* window);

private slots:

    void on_pushButton_new_clicked();

    void on_comboBox_currentIndexChanged(int index);

    void on_pushButton_delete_clicked();

    void on_pushButton_edit_clicked();

    void on_pushButton_clicked();

protected:
    void setupComboBox();

private:
    Ui::DataExporterSetVal *ui;
    QHash<QString,boost::shared_ptr<vsExportDataInfo>> m_data;
    size_t m_settingNum;
    MainWindow* m_mainwindow;

    bool export_setting(boost::shared_ptr<vsExportDataInfo>& data);
    bool loadXMLs();
    bool loadXML(QString& filename,boost::shared_ptr<vsExportDataInfo>& data);
    void getCurrentExportInfo(boost::shared_ptr<vsExportDataInfo>& data);
};

#endif // DATAEXPORTERSETVAL_H
