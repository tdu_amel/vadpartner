﻿#ifndef vsSTLManager_H
#define vsSTLManager_H
#include "vsvtkinitializer.h"
#include <vector>
#include <vtkActor.h>
#include <vtkSmartPointer.h>

class QMainWindow;

class vsSTLManager
{
protected:
    std::vector< vtkSmartPointer<vtkActor> > m_ActorSTL;
    int m_oldSelectedSTL;
    QMainWindow* m_mainwindow;
public:
    vsSTLManager();
    virtual ~vsSTLManager();
    void loadSTL(const char* fileName, vtkSmartPointer<vtkActor>& actor );
    void selectSTL(vtkActor* actor );
};

#endif // vsSTLManager_H
