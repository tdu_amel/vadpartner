﻿#include "vsexportdata.h"
#include <boost/shared_ptr.hpp>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include "MainWindow.h"
#include "vmsg.h"



int vsExportDataFactory::getDataArrayID(vsExportData::DataType datatype,
                                                             vsExportData::DataValInfo info) {


    switch(datatype) {
        case vsExportData::DataType_DAY:
        return info - vsExportData::VSEXPORTDATAVALINFO_DAY_DATA_START - 1;
        break;
        case vsExportData::DataType_STL:
        return info - vsExportData::VSEXPORTDATAVALINFO_STL_DATA_START - 1;
        break;
        case vsExportData::DataType_HUMANMODEL:
        return info - vsExportData::VSEXPORTDATAVALINFO_HUMANMODEL_DATA_START - 1;
        break;
        case vsExportData::DataType_MEASUREMENT:
        return info - vsExportData::VSEXPORTDATAVALINFO_MEASUREMENT_DATA_START - 1;
        break;
        case vsExportData::DataType_CAMERA:
        return info - vsExportData::VSEXPORTDATAVALINFO_CAMERA_DATA_START - 1;
        break;
        case vsExportData::DataType_USERINPUT:
        return info - vsExportData::VSEXPORTDATAVALINFO_USERINPUT_DATA_START - 1;
        break;
    default:
        break;
    }

    return -1;
}

vsExportData::DataValInfo vsExportDataFactory::getDataInfoID(vsExportData::DataType datatype, int selectID) {

    switch(datatype) {
        case vsExportData::DataType_DAY:
        return getDayID(selectID);
        break;
        case vsExportData::DataType_STL:
        return getSTLID(selectID);
        break;
        case vsExportData::DataType_HUMANMODEL:
        return getHumanModelID(selectID);
        break;
        case vsExportData::DataType_MEASUREMENT:
        return getMeasurementID(selectID);
        break;
        case vsExportData::DataType_CAMERA:
        return getCameraID(selectID);
        break;
        case vsExportData::DataType_USERINPUT:
        return getUserInputID(selectID);
        break;
    default:
        break;
    }
    return vsExportData::VSEXPORTDATAVALINFO_INVALID;
}



QString vsExportDataFactory::name_type[] = {
    QCoreApplication::tr("Day"),
    QCoreApplication::tr("STL"),
    QCoreApplication::tr("Human Model"),
    QCoreApplication::tr("DICOM"),
    QCoreApplication::tr("Measurement"),
    QCoreApplication::tr("Camera"),
    QCoreApplication::tr("User Input"),
    QString()
};


QString vsExportDataFactory::name_day[] = {
        QCoreApplication::tr("DICOM(yyyy/mm/dd)"),
        QCoreApplication::tr("Today(yyyy/mm/dd)"),
        QCoreApplication::tr("Today(yyyy/mm/dd hh:mm:ss)"),
        QCoreApplication::tr("Export Time(yyyy/mm/dd)"),
        QCoreApplication::tr("Export Time(yyyy/mm/dd hh:mm:ss)"),
        QCoreApplication::tr("User Input(yyyy/mm/dd)"),
        QCoreApplication::tr("User Input(yyyy/mm/dd hh:mm:ss)"),
        QString()
};

QString vsExportDataFactory::name_stl[] = {
       QCoreApplication::tr("File Name"),
       QCoreApplication::tr("Euler Angle of  a STL Model[deg*3]"),
       QCoreApplication::tr("World Transform Matrix of a Model"),
       QString()
};

QString vsExportDataFactory::name_humanmodel[] = {
        QCoreApplication::tr("Source Data Directory Name"),
        QCoreApplication::tr("Euler Angle of a Human Model[deg*3]"),
        QCoreApplication::tr("World Transform Matrix of a Model"),
        QCoreApplication::tr("Local Origin of a Model"),
        QString()
};


QString vsExportDataFactory::name_dicomdata[] = {
    QCoreApplication::tr("Data Directory Name"),
    QCoreApplication::tr("2D Image Width"),
    QCoreApplication::tr("2D Image Height"),
    QCoreApplication::tr("Slice Thickness"),
    QCoreApplication::tr("Image Position (Patient)"),
    QCoreApplication::tr("Image Orientation (Patient)"),
    QCoreApplication::tr("Window Center"),
    QCoreApplication::tr("Window Width"),
    QString()
};

QString vsExportDataFactory::name_measurement[] = {
    QCoreApplication::tr("Measured Distance[mm]"),
    QCoreApplication::tr("Measured Angle[deg]"),
    QString()
};

QString vsExportDataFactory::name_userinput[] = {
    QCoreApplication::tr("Integer"),
    QCoreApplication::tr("Mixed Decimal"),
    QCoreApplication::tr("String"),
    QString()
};


QString vsExportDataFactory::name_camera[] = {
    QCoreApplication::tr("Camera Eye Pos"),
    QCoreApplication::tr("Camera Fixation pos"),
    QCoreApplication::tr("Camera Upvec"),
    QCoreApplication::tr("camera matrix(view matrix)"),
    QString()
};


vsExportData::DataValInfo vsExportDataFactory::infovalue_day[] = {
    vsExportData::VSEXPORTDATAVALINFO_DAY_DATA_START,
    vsExportData::VSEXPORTDATAVALINFO_DAY_DICOM_YYYYMMDD,
    vsExportData::VSEXPORTDATAVALINFO_DAY_TODAY_YYYYMMDD,
    vsExportData::VSEXPORTDATAVALINFO_DAY_TODAY_YYYYMMDD_HHMMSS,
    vsExportData::VSEXPORTDATAVALINFO_DAY_EXPORT_TIME_YYYYMMDD,
    vsExportData::VSEXPORTDATAVALINFO_DAY_EXPORT_TIME_YYYYMMDD_HHMMSS,
    vsExportData::VSEXPORTDATAVALINFO_DAY_EXPORT_TIME_YYYYMMDD_HHMMSS,
    vsExportData::VSEXPORTDATAVALINFO_DAY_USER_INPUT_YYYYMMDD,
    vsExportData::VSEXPORTDATAVALINFO_DAY_USER_INPUT_YYYYMMDD_HHMMSS,
    vsExportData::VSEXPORTDATAVALINFO_DAY_DATA_END
};

vsExportData::DataValInfo vsExportDataFactory::infovalue_stl[] = {
    vsExportData::VSEXPORTDATAVALINFO_STL_DATA_START,
    vsExportData::VSEXPORTDATAVALINFO_STL_FILE_NAME,
    vsExportData::VSEXPORTDATAVALINFO_STL_EULER_ANGLE_OF_A_STL_MODEL_DEG3,
    vsExportData::VSEXPORTDATAVALINFO_STL_WORLD_TRANSFORM_MATRIX_OF_A_MODEL,
    vsExportData::VSEXPORTDATAVALINFO_STL_DATA_END
};

vsExportData::DataValInfo vsExportDataFactory::infovalue_humanmodel[] = {
    vsExportData::VSEXPORTDATAVALINFO_HUMANMODEL_DATA_START,
    vsExportData::VSEXPORTDATAVALINFO_HUMANMODEL_SOURCE_DATA_DIRECTORY_NAME,
    vsExportData::VSEXPORTDATAVALINFO_HUMANMODEL_EULER_ANGLE_OF_A_HUMAN_MODEL_DEG3,
    vsExportData::VSEXPORTDATAVALINFO_HUMANMODEL_WORLD_TRANSFORM_MATRIX_OF_A_MODEL,
    vsExportData::VSEXPORTDATAVALINFO_HUMANMODEL_LOCAL_ORIGIN_OF_A_MODEL,
    vsExportData::VSEXPORTDATAVALINFO_HUMANMODEL_WORLD_TRANSFORM_MATRIX_OF_A_MODEL,
    vsExportData::VSEXPORTDATAVALINFO_HUMANMODEL_DATA_END
};

vsExportData::DataValInfo vsExportDataFactory::infovalue_dicomdata[] = {
    vsExportData::VSEXPORTDATAVALINFO_DICOMDATA_DATA_START,
    vsExportData::VSEXPORTDATAVALINFO_DICOMDATA_DATA_DIRECTORY_NAME,
    vsExportData::VSEXPORTDATAVALINFO_DICOMDATA_2D_IMAGE_WIDTH,
    vsExportData::VSEXPORTDATAVALINFO_DICOMDATA_2D_IMAGE_HEIGHT,
    vsExportData::VSEXPORTDATAVALINFO_DICOMDATA_SLICE_THICKNESS,
    vsExportData::VSEXPORTDATAVALINFO_DICOMDATA_IMAGE_POSITION_PATIENT,
    vsExportData::VSEXPORTDATAVALINFO_DICOMDATA_IMAGE_ORIENTATION_PATIENT,
    vsExportData::VSEXPORTDATAVALINFO_DICOMDATA_WINDOW_CENTER,
    vsExportData::VSEXPORTDATAVALINFO_DICOMDATA_WINDOW_WIDTH,
    vsExportData::VSEXPORTDATAVALINFO_DICOMDATA_DATA_END
};

vsExportData::DataValInfo vsExportDataFactory::infovalue_measurement[] = {
    vsExportData::VSEXPORTDATAVALINFO_MEASUREMENT_DATA_START,
    vsExportData::VSEXPORTDATAVALINFO_MEASUREMENT_MEASURED_DISTANCE_MM,
    vsExportData::VSEXPORTDATAVALINFO_MEASUREMENT_MEASURED_ANGLE_DEG,
    vsExportData::VSEXPORTDATAVALINFO_MEASUREMENT_DATA_END
};

vsExportData::DataValInfo vsExportDataFactory::infovalue_userinput[] = {
    vsExportData::VSEXPORTDATAVALINFO_USERINPUT_DATA_START,
    vsExportData::VSEXPORTDATAVALINFO_USERINPUT_INTEGER,
    vsExportData::VSEXPORTDATAVALINFO_USERINPUT_MIXED_DECIMAL,
    vsExportData::VSEXPORTDATAVALINFO_USERINPUT_STRING,
    vsExportData::VSEXPORTDATAVALINFO_USERINPUT_DATA_END
};

vsExportData::DataValInfo vsExportDataFactory::infovalue_camera[] = {
    vsExportData::VSEXPORTDATAVALINFO_CAMERA_DATA_START,
    vsExportData::VSEXPORTDATAVALINFO_CAMERA_CAMERA_EYE_POS,
    vsExportData::VSEXPORTDATAVALINFO_CAMERA_CAMERA_FIXATION_POS,
    vsExportData::VSEXPORTDATAVALINFO_CAMERA_CAMERA_UPVEC,
    vsExportData::VSEXPORTDATAVALINFO_CAMERA_CAMERA_MATRIX_VIEW_MATRIX,
    vsExportData::VSEXPORTDATAVALINFO_CAMERA_DATA_END
};


QString* vsExportDataFactory::names[] = {
    &vsExportDataFactory::name_day[0],
    &vsExportDataFactory::name_stl[0],
    &vsExportDataFactory::name_humanmodel[0],
    &vsExportDataFactory::name_dicomdata[0],
    &vsExportDataFactory::name_measurement[0],
    &vsExportDataFactory::name_camera[0],
    &vsExportDataFactory::name_userinput[0]
};


vsExportData::DataValInfo vsExportDataFactory::getID( int selectID, vsExportData::DataValInfo* array, int len ) {

    if ((0 <= selectID) && (selectID < len) ) {
        return array[selectID+1];
    }
    else {
        return vsExportData::VSEXPORTDATAVALINFO_INVALID;
    }
    return vsExportData::VSEXPORTDATAVALINFO_INVALID;

}


vsExportData::DataValInfo vsExportDataFactory::getDayID(int selectID ) {

    int dataSize = vsExportData::VSEXPORTDATAVALINFO_DAY_DATA_END
                 - vsExportData::VSEXPORTDATAVALINFO_DAY_DATA_START
                 + 1;

    return getID(selectID,infovalue_day,dataSize);
}

vsExportData::DataValInfo vsExportDataFactory::getSTLID(int selectID ) {

    int dataSize = vsExportData::VSEXPORTDATAVALINFO_STL_DATA_END
                 - vsExportData::VSEXPORTDATAVALINFO_STL_DATA_START;

    return getID(selectID,infovalue_stl,dataSize);

}

vsExportData::DataValInfo vsExportDataFactory::getHumanModelID(int selectID ) {

    int dataSize = vsExportData::VSEXPORTDATAVALINFO_HUMANMODEL_DATA_END
                 - vsExportData::VSEXPORTDATAVALINFO_HUMANMODEL_DATA_START
                 + 1;

    return getID(selectID,infovalue_humanmodel,dataSize);

}

vsExportData::DataValInfo vsExportDataFactory::getDICOMDataID( int selectID ) {
    int dataSize = vsExportData::VSEXPORTDATAVALINFO_DICOMDATA_DATA_END
                 - vsExportData::VSEXPORTDATAVALINFO_DICOMDATA_DATA_START
            + 1;

    return getID(selectID,infovalue_dicomdata,dataSize);
}

vsExportData::DataValInfo vsExportDataFactory::getMeasurementID(int selectID ) {
    int dataSize = vsExportData::VSEXPORTDATAVALINFO_DICOMDATA_DATA_END
                 - vsExportData::VSEXPORTDATAVALINFO_DICOMDATA_DATA_START
            + 1;

    return getID(selectID,infovalue_measurement,dataSize);
}

vsExportData::DataValInfo vsExportDataFactory::getCameraID(int selectID ) {
    int dataSize = vsExportData::VSEXPORTDATAVALINFO_DICOMDATA_DATA_END
                 - vsExportData::VSEXPORTDATAVALINFO_DICOMDATA_DATA_START
            + 1;

    return getID(selectID,infovalue_camera,dataSize);
}

vsExportData::DataValInfo vsExportDataFactory::getUserInputID(int selectID ) {
    int dataSize = vsExportData::VSEXPORTDATAVALINFO_DICOMDATA_DATA_END
                 - vsExportData::VSEXPORTDATAVALINFO_DICOMDATA_DATA_START
            + 1;

    return getID(selectID,infovalue_userinput,dataSize);
}


void clearLayout(QLayout* layout, bool deleteWidgets = true)
{
    while (QLayoutItem* item = layout->takeAt(0))
    {
        QWidget* widget;
        if (  (deleteWidgets)
              && (widget = item->widget())  ) {
            delete widget;
        }
        if (QLayout* childLayout = item->layout()) {
            clearLayout(childLayout, deleteWidgets);
        }
        delete item;
    }
}



/**
 * @brief addExportDataUI
 * エクスポートするデータを入力するUIを指定されたVerticalLayoutに追加します．
 * @param ui_area　UIが追加されるVerticalLayout
 * @param data　データが追加されるUI
 */
void vsExportDataFactory::addExportDataUI(MainWindow* mainwindow, QVBoxLayout** ui_area,
                                          boost::shared_ptr<vsExportDataInfo> &data) {

    if ( !data.get() ) {
        qDebug("vsExportDataFactory::addExportDataUI>data is null");
        return;
    }


    if (ui_area == 0) {
        return;
    }

    clearLayout((*ui_area),true);

    QString data_type_str;

    QVector<boost::shared_ptr<vsExportData>>& vec = data->m_data;


    for ( int i = 0; i < vec.size(); i++ ) {

        int strIdx = 0;
        //qDebug("vsExportDataFactory::type:%d",vec[i]->type);

        switch( vec[i]->type ) {
        case vsExportData::DataType_DAY:
            addExportDataUI_Date(*ui_area,vec[i]);
            data_type_str = name_day[strIdx];
            break;
        case vsExportData::DataType_STL:
            data_type_str = name_stl[strIdx];
            break;
        case vsExportData::DataType_HUMANMODEL:
            data_type_str = name_humanmodel[strIdx];
            break;
        case vsExportData::DataType_MEASUREMENT:
            data_type_str = name_measurement[strIdx];
            addExportDataUI_Measurement(mainwindow,*ui_area,vec[i]);
            break;
        case vsExportData::DataType_CAMERA:
            data_type_str = name_camera[strIdx];
            break;
        case vsExportData::DataType_USERINPUT:
            data_type_str = name_userinput[strIdx];
            break;
        default:
            data_type_str = "Invalid";
            break;
        }

        //QWidget* widget = new QLabel(vec[i]->identification+QString("(")+data_type_str+QString(")"));
        //ui_area->addWidget(widget);

    }


}


void vsExportDataFactory::addExportDataUI_Date(QVBoxLayout* ui_area,boost::shared_ptr<vsExportData>& data) {

    vBlock("addExportDataUI_Date");

    if ( !data.get() ) {
        vWarning("data is null");
        return;
    }


}


void vsExportDataFactory::addExportDataUI_Measurement(MainWindow* mainwindow,QVBoxLayout* ui_area,
                                                      boost::shared_ptr<vsExportData>& data) {

    vBlock("addExportDataUI_Measurement");


    if ( !data.get() ) {
        vWarning("data is null");
        return;
    }

    QHBoxLayout* hbox = new QHBoxLayout();
    hbox->setObjectName("actual_export_ui_area");

    int id = data->info;
    vDebugVar(id);

    int strIdx = id - vsExportData::VSEXPORTDATAVALINFO_MEASUREMENT_DATA_START -1;

    vDebugVar(strIdx);
    QString data_type_str = name_measurement[strIdx];


    QLabel* widget = new QLabel(data->identification+QString("(")+data_type_str+QString(")"));
    hbox->addWidget(widget);



    QLineEdit* measureVal = new QLineEdit();
    measureVal->setReadOnly(true);
    //measureVal->setEnabled(false);

    hbox->addWidget(measureVal);

    QPushButton* measure_button = new QPushButton(QString("Measure"));
    measure_button->setProperty("myui", (long long)(ui_area));//x64対応

    hbox->addWidget(measure_button);

    measure_button->setProperty("lineedit", (long long)(measureVal));//x64対応
    ui_area->addLayout(hbox);

    switch(data->info) {
    case vsExportData::VSEXPORTDATAVALINFO_MEASUREMENT_MEASURED_DISTANCE_MM:
        measure_button->connect(measure_button,SIGNAL(clicked()),mainwindow,SLOT(export_measure_dist_start()));
        vMsg("dist");
        break;
    case vsExportData::VSEXPORTDATAVALINFO_MEASUREMENT_MEASURED_ANGLE_DEG:
        measure_button->connect(measure_button,SIGNAL(clicked()),mainwindow,SLOT(export_measure_angle_start()));
        vMsg("angle");
        break;
    default:
        vWarning("oth:%d",data->info);
        break;
    }


}
