﻿#ifndef VSIMAGELOADERDICOM_H
#define VSIMAGELOADERDICOM_H
#include "vsImageDicom.h"
#include <boost/shared_ptr.hpp>
//DICOMイメージローダー
class vsImageLoaderDICOM
{
private:
    typedef __int16 vsDicomGroup;//16ビット整数型にvsDicomGroup型という別名をつける。
    typedef __int16 vsDicomElement;//16ビット整数型にvsDicomElement型という別名をつける。
public:
    vsImageLoaderDICOM();
     bool load(const char* kFileName, boost::shared_ptr<vsImageDICOM>* out);//これで読み込みを実行。
};

#endif // VSIMAGELOADERDICOM_H
