﻿#ifndef VSBONEDATACONSTRUCTOR_H
#define VSBONEDATACONSTRUCTOR_H
#include <vtkSmartPointer.h>
#include "vsOrganExtraction.h"
class vtkActor;
class vtkImageData;

class vsBoneDataConstructor:public vsOrganExtraction
{
public:
    vsBoneDataConstructor();
    static void constructImageData(vtkSmartPointer<vtkImageData>& dscImg,vtkImageData *srcImg);
    static void constructModel(vtkSmartPointer<vtkActor> &actor, vtkImageData *srcImg);
};

#endif // VSBONEDATACONSTRUCTOR_H
