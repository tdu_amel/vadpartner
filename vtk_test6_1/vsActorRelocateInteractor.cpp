﻿#include "vsActorRelocateInteractor.h"
#include <vtkSmartPointer.h>
#include <vtkObjectFactory.h>

vsActorRelocateInteractor::vsActorRelocateInteractor()
    :ui(0),m_container(0),m_stlManager(0),m_pressed(false),m_selected_actor(0)
{

}

void vsActorRelocateInteractor::setContainer(vsTreeItemContainer* container) {
    m_container = container;
}


void vsActorRelocateInteractor::setUI(Ui_VadSim *_ui) {
    ui = _ui;
}

/**
 * @brief vsActorRelocateInteractor::getUI
 * @return
 */
Ui_VadSim* vsActorRelocateInteractor::getUI() {
    return ui;
}

/**
 * @brief vsActorRelocateInteractor::setSTLManager
 * @param stlManager
 */
void vsActorRelocateInteractor::setSTLManager(vsSTLManager* stlManager) {
    m_stlManager = stlManager;
}


/**
 * @brief setSelectedActorName 選択されたActorの名前をUIのテキストラベル「label_objectName」に表示
 * @param actor
 */
void vsActorRelocateInteractor::setSelectedActorName(vtkProp* actor) {

    if( m_container == 0) {
        //m_containerにActorを渡して名前を調べるのでm_containerだとエラー
        vMsg("m_container is NULL!(myInteractorStyleMove::setSelectedObjectName)");
        return;
    }

    std::list<vsTreeItem*>::iterator itr = m_container->begin();

    for( ; itr != m_container->end(); ++itr ) {

        if( (*itr)->m_prop == actor  ) {

            QString label = (*itr)->text(0);
            vMsg("%s\n", qPrintable( label ) );

            ui->label_objectName->setText(label.toStdString().c_str());
            break;
        }

    }
}


/**
 * @brief vsActorRelocateInteractor::OnLeftButtonDown
 */
void vsActorRelocateInteractor::OnLeftButtonDown()
{

    // Forward events

    int x = this->Interactor->GetEventPosition()[0];
    int y = this->Interactor->GetEventPosition()[1];
    this->FindPokedRenderer(x, y);
    this->FindPickedActor(x, y);

    if (this->CurrentRenderer == NULL || this->InteractionProp == NULL)
      {
      std::cout << "Nothing selected." << std::endl;

      if(m_stlManager != 0){

           m_stlManager->selectSTL(0);

      }

      ui->qvtkWidget1->GetRenderWindow()->Render();
      return;
      }

    vtkSmartPointer<vtkPropCollection> actors =
      vtkSmartPointer<vtkPropCollection>::New();

    this->InteractionProp->GetActors(actors);
    actors->InitTraversal();
    vtkActor* actor = m_selected_actor = vtkActor::SafeDownCast(actors->GetNextProp());

    if( actor != NULL ) {

            vtkInteractorStyleTrackballActor::OnMiddleButtonDown();
            setRadioButtonState(actor);
            setSTLPosition(actor);
            setSTLAngle(actor);
            setSelectedActorName(actor);

            if(m_stlManager != 0){

                 m_stlManager->selectSTL(actor);

            }

            ui->qvtkWidget1->GetRenderWindow()->Render();


    } else {


        vtkInteractorStyleTrackballActor::OnMiddleButtonDown();

        this->InteractionProp = NULL;

    }

    ui->qvtkWidget1->GetRenderWindow()->Render();
}





/**
 * @brief vsActorRelocateInteractor::OnLeftButtonUp
 */
void vsActorRelocateInteractor::OnLeftButtonUp()
{

    // Forward events

    int x = this->Interactor->GetEventPosition()[0];
    int y = this->Interactor->GetEventPosition()[1];
    this->FindPokedRenderer(x, y);
    this->FindPickedActor(x, y);

    if (this->CurrentRenderer == NULL || this->InteractionProp == NULL)
      {
      std::cout << "Nothing selected." << std::endl;
      return;
      }

    vtkSmartPointer<vtkPropCollection> actors =
      vtkSmartPointer<vtkPropCollection>::New();

    this->InteractionProp->GetActors(actors);
    actors->InitTraversal();
    vtkActor* actor = vtkActor::SafeDownCast(actors->GetNextProp());

    if( actor != NULL ) {


            //printf("   actSTL\n");
            vtkInteractorStyleTrackballActor::OnMiddleButtonUp();
            conveyTransform(actor);
           setRadioButtonState(actor);
           setSTLPosition(actor);
           setSTLAngle(actor);
           setSelectedActorName(actor);



   } else {


        vtkInteractorStyleTrackballActor::OnMiddleButtonUp();

        this->InteractionProp = NULL;

    }

   ui->qvtkWidget1->GetRenderWindow()->Render();
}





/**
 * @brief vsActorRelocateInteractor::OnRightButtonDown
 */
void vsActorRelocateInteractor::OnRightButtonDown()
{
   vtkInteractorStyleTrackballActor::OnLeftButtonDown();
   vtkSmartPointer<vtkPropCollection> actors =
     vtkSmartPointer<vtkPropCollection>::New();
   vMsg("1");
   if( this->InteractionProp == NULL) {

       if(m_stlManager != 0){

            m_stlManager->selectSTL(0);

       }
       ui->qvtkWidget1->GetRenderWindow()->Render();
       return;


   }
   this->InteractionProp->GetActors(actors);
   if( actors == NULL ) {
       std::cout << "Nothing selected." << std::endl;

       return;

   }
   vMsg("2");
   actors->InitTraversal();
   vtkActor* actor = vtkActor::SafeDownCast(actors->GetNextProp());
   if( actor == NULL ) {
       if(m_stlManager != 0){

            m_stlManager->selectSTL(0);

       }
   return;

   }
   vMsg("3");
   setRadioButtonState(actor);
   setSTLPosition(actor);
   setSTLAngle(actor);
   setSelectedActorName(actor);
   vMsg("4");
   if(m_stlManager != 0){
        m_stlManager->selectSTL(actor);
   }

   ui->qvtkWidget1->GetRenderWindow()->Render();

}


/**
 * @brief OnLeftButtonUp
 */
void vsActorRelocateInteractor::OnRightButtonUp()
{
   vtkInteractorStyleTrackballActor::OnLeftButtonUp();
   vtkSmartPointer<vtkPropCollection> actors =
     vtkSmartPointer<vtkPropCollection>::New();

   if( this->InteractionProp == NULL) return;
   this->InteractionProp->GetActors(actors);

   if( actors == NULL ) {
       return;
   }

   actors->InitTraversal();
   vtkActor* actor = vtkActor::SafeDownCast(actors->GetNextProp());
   if( actor == NULL ) {

       return;

   }

   conveyTransform(actor);
   setRadioButtonState(actor);
   setSTLPosition(actor);
   setSTLAngle(actor);
   setSelectedActorName(actor);

}




//vtkStandardNewMacro(vsActorRelocateInteractor)
/**
 * @brief vsActorRelocateInteractor::New
 * @return
 */
vsActorRelocateInteractor *
vsActorRelocateInteractor::New()
{
    return new vsActorRelocateInteractor();
}









void vsActorRelocateInteractor::conveyTransform(vtkProp* root_actor) {

    if( root_actor == NULL ) return;

    //vMsg("convey");
    std::list< vsTreeItem* >::iterator itr;
    m_container->findItem(itr,root_actor);
    if( itr == m_container->end() ) {
        //root_actorはコンテナの中に見つからなかった
        return;
    }

    vtkSmartPointer<vtkTransform> transform_stack = vtkSmartPointer<vtkTransform>::New();
    transform_stack->PreMultiply();
    transform_stack->Identity();


    vtkSmartPointer<vtkMatrix4x4> root_actor_mat = vtkSmartPointer<vtkMatrix4x4>::New();
    root_actor_mat = root_actor->GetMatrix();


    convey(transform_stack, root_actor_mat, (*itr));

    ui->qvtkWidget1->GetRenderWindow()->Render();

}


void vsActorRelocateInteractor::convey(vtkTransform* transform_stack, vtkMatrix4x4* parent_mat, vsTreeItem* item) {

    if( m_container == 0) {
        vMsg("m_container is NULL!");
        return;
    }

    //親行列を変換行列スタックに積む
    transform_stack->Push();
    transform_stack->PreMultiply();
    transform_stack->Concatenate(parent_mat);


    //vMsg("num:%d",item->childCount());
    for( int i = 0; i < item->childCount(); ++i ) {

        vtkSmartPointer<vtkMatrix4x4> currentUserMatrix = vtkSmartPointer<vtkMatrix4x4>::New();
        currentUserMatrix = reinterpret_cast<vtkProp3D*>( reinterpret_cast<vsTreeItem*>(item->child(i))->m_prop )->GetUserMatrix();

        vtkSmartPointer<vtkMatrix4x4> currentMatrix = vtkSmartPointer<vtkMatrix4x4>::New();
        currentMatrix = reinterpret_cast<vtkProp3D*>( reinterpret_cast<vsTreeItem*>(item->child(i))->m_prop )->GetMatrix();

         if (currentUserMatrix) {
             vMsg("user mat is not supported!");
             // vtkMatrix4x4::Multiply4x4( currentUserMatrix, currentUserMatrix, currentMatrix );
         }
         else {
             currentUserMatrix = vtkSmartPointer<vtkMatrix4x4>::New();
             currentUserMatrix->DeepCopy(currentMatrix);
         }


        //vtkMatrix4x4::Multiply4x4( currentUserMatrix, currentUserMatrix, reinterpret_cast<vsTreeItem*>(item->child(newidx))->m_parentInvMat );
        //vtkMatrix4x4::Multiply4x4(matrix,matrix,currentUserMatrix);


        vtkSmartPointer<vtkMatrix4x4> matrix = vtkSmartPointer<vtkMatrix4x4>::New();
        transform_stack->GetMatrix(matrix);

        vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
        transform->PostMultiply();
        transform->Identity();
        transform->SetMatrix(matrix);

        double set_p[3];
        double set_a[3];

        //今までの変換から平行移動成分を抜き出し
        double p[3];
        transform->GetPosition(p);

        //今までの変換から回転成分を抜き出し
        double a[3];
        transform->GetOrientation(a);

        //子パーツの位置を取得
        double p_c[3];
        reinterpret_cast<vtkProp3D*>( reinterpret_cast<vsTreeItem*>(item->child(i))->m_prop )->GetPosition(p_c);

        //子パーツの角度を取得
        double a_c[3];
        reinterpret_cast<vtkProp3D*>( reinterpret_cast<vsTreeItem*>(item->child(i))->m_prop )->GetOrientation(a_c);


        for(int j = 0; j < 3; ++j ) {

            //今までの変換から取り出した位置と回転から前回のもの(ツリーアイテムに保存してある)を引く
            set_p[j] = p[j] - reinterpret_cast<vsTreeItem*>(item->child(i))->pos[j];
            set_a[j] = a[j] - reinterpret_cast<vsTreeItem*>(item->child(i))->angle[j];

            //今回の位置，角度を加える(孫アイテムがずれるのを防ぐために上で前回の位置や角度を引く)
            set_p[j] += p_c[j];
            set_a[j] += a_c[j];

            //ツリーアイテムに今回の位置と角度を保存
            reinterpret_cast<vsTreeItem*>(item->child(i))->pos[j] = p[j];
            reinterpret_cast<vsTreeItem*>(item->child(i))->angle[j] = a[j];
        }

        //アクター側にも位置を角度を設定
        reinterpret_cast<vtkProp3D*>( reinterpret_cast<vsTreeItem*>(item->child(i))->m_prop )->SetPosition(set_p);
        reinterpret_cast<vtkProp3D*>( reinterpret_cast<vsTreeItem*>(item->child(i))->m_prop )->SetOrientation(set_a);

        //行列を計算
        reinterpret_cast<vtkProp3D*>( reinterpret_cast<vsTreeItem*>(item->child(i))->m_prop )->ComputeMatrix();

       //計算し終わった行列を取得
       vtkSmartPointer<vtkMatrix4x4> child_mat = vtkSmartPointer<vtkMatrix4x4>::New();
       child_mat = reinterpret_cast<vtkProp3D*>( reinterpret_cast<vsTreeItem*>(item->child(i))->m_prop )->GetMatrix();

       //コンテナにアクターを渡して，それを持つツリーアイテムを取得する
       std::list< vsTreeItem* >::iterator childItr;
       m_container->findItem(childItr, reinterpret_cast<vtkProp3D*>( reinterpret_cast<vsTreeItem*>(item->child(i))->m_prop ));

       if (childItr != m_container->end())
       {
           //自分の子アイテムに自分の動きを伝達
           convey(transform_stack, child_mat, childItr);
       }

    }

    //最初に積んだ　parent_matを取り除く
    transform_stack->Pop();


}




void vsActorRelocateInteractor::setSTLPosition(vtkProp* actor) {

    vtkActor* actSTL = vtkActor::SafeDownCast(actor);
    if( actSTL == NULL || ui == NULL ) {

        return;

    }


    std::list< vsTreeItem* >::iterator itr;
    m_container->findItem(itr,actor);
    if( itr == m_container->end()) return;


    //vtkSmartPointer<vtkMatrix4x4> mat = vtkSmartPointer<vtkMatrix4x4>::New();
    //reinterpret_cast<vtkProp3D*>( (*itr)->m_prop)->GetMatrix(mat);

    double p[3];
    reinterpret_cast<vtkProp3D*>((*itr)->m_prop)->GetPosition(p);



    if( ui->radioButton_rel->isChecked() ) {

        for(int j = 0; j < 3; ++j ) {

           p[j] = p[j] - (*itr)->pos[j];
           //set_a[j] = a[j] - reinterpret_cast<vsTreeItem*>((*itr)->child(newidx))->angle[j];

        }

    }


    //std::cout << "p: " << p[0] << " " << p[1] << " " << p[2] << std::endl;

    ui->edit_x->setText(QString::number(p[0]));
    ui->edit_y->setText(QString::number(p[1]));
    ui->edit_z->setText(QString::number(p[2]));



}




/**
 * @brief setPosEdit
 */
void vsActorRelocateInteractor::setPosEdit() {

    QList<QTreeWidgetItem*> items = ui->treeWidget->selectedItems();

    double p[3];
    reinterpret_cast<vtkProp3D*>( reinterpret_cast<vsTreeItem*>(items.at(0)) )->GetPosition(p);

    if( ui->radioButton_rel->isChecked() ) {

        for(int j = 0; j < 3; ++j ) {

           p[j] = p[j] - reinterpret_cast<vsTreeItem*>(items.at(0))->pos[j];


        }

    }


    ui->edit_x->setText(QString::number(p[0]));
    ui->edit_y->setText(QString::number(p[1]));
    ui->edit_z->setText(QString::number(p[2]));

}


/**
 * @brief setSTLAngle
 * @param actor
 */
void vsActorRelocateInteractor::setSTLAngle(vtkProp* actor) {

    vtkActor* actSTL = vtkActor::SafeDownCast(actor);
   if( actSTL == NULL || ui == NULL ) {

       return;

   }

   std::list< vsTreeItem* >::iterator itr;
   m_container->findItem(itr,actor);
   if( itr == m_container->end()) return;

   vtkSmartPointer<vtkMatrix4x4> mat = vtkSmartPointer<vtkMatrix4x4>::New();
   actSTL->GetMatrix(mat);

   vtkSmartPointer<vtkTransform> trans = vtkSmartPointer<vtkTransform>::New();

   trans->SetMatrix(mat);

   double angle[3];
   trans->GetOrientation(angle);


   if( ui->radioButton_rel->isChecked() ) {

       for(int j = 0; j < 3; ++j ) {

         angle[j] = angle[j] - (*itr)->angle[j];

       }

   }

   ui->edit_angle_x->setText(QString::number(angle[0]));
   ui->edit_angle_y->setText(QString::number(angle[1]));
   ui->edit_angle_z->setText(QString::number(angle[2]));

}

