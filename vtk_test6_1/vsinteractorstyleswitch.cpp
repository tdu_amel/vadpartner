﻿#include "vsinteractorstyleswitch.h"
#include <vtkObjectFactory.h>
#include <vtkCallbackCommand.h>

vsInteractorStyleSwitch *
vsInteractorStyleSwitch::New()
{
    return new vsInteractorStyleSwitch();
}

vsInteractorStyleSwitch::~vsInteractorStyleSwitch() {

}

vsInteractorStyleSwitch::vsInteractorStyleSwitch():m_container(0)
{

    //切り替え先を親クラスで設定されたのものから強制変更

    if ( this->TrackballActor ) {
        //忘れるとメモリリーク
        this->TrackballActor->Delete();
    }
    this->TrackballActor = vsActorRelocateInteractor::New();//マウス操作をアクター操作に変換するInteractor


    if ( this->TrackballCamera ) {
        //忘れるとメモリリーク
        this->TrackballCamera->Delete();
    }
    this->TrackballCamera = vsCameraInteractor::New();//マウス操作をカメラ操作に変換するInteractor

    this->JoystickOrTrackball = VTKIS_TRACKBALL;
    this->CameraOrActor = VTKIS_ACTOR;
    this->CurrentStyle = this->TrackballCamera;//初期操作はカメラ

}

void vsInteractorStyleSwitch::setContainer(vsTreeItemContainer* container) {

    m_container = container;
    ((vsActorRelocateInteractor*) (this->TrackballActor))->setContainer(m_container);

}


/**
 * @brief setDistRepresetation 距離計測機能をカメラ操作Interactorにセット
 * @param _rep　セットする距離測定機能クラスのインスタンスへのポインタ
 */
void vsInteractorStyleSwitch::setDistRepresetation(vtkDistanceRepresentation *_rep) {

    reinterpret_cast<vsCameraInteractor*>(this->TrackballCamera)->setDistRepresetation(_rep);

}


/**
 * @brief setDistRepresetation 角度計測機能をカメラ操作Interactorにセット
 * @param _rep　セットする角度測定機能クラスのインスタンスへのポインタ
 */
void vsInteractorStyleSwitch::setAngleRepresetation(vtkAngleRepresentation *_rep) {

    reinterpret_cast<vsCameraInteractor*>(this->TrackballCamera)->setAngleRepresetation(_rep);

}

void vsInteractorStyleSwitch::setCameraMode() {

    CameraOrActor = VTKIS_CAMERA;
    MultiTouch = false;
    EventCallbackCommand->SetAbortFlag(1);
    SetCurrentStyle();
    setModeName("Camera Move");

}


void vsInteractorStyleSwitch::setMoveMode() {

    this->CameraOrActor = VTKIS_ACTOR;
    this->MultiTouch = false;
    this->EventCallbackCommand->SetAbortFlag(1);
    SetCurrentStyle();
    setModeName("VAD Move");
}

void vsInteractorStyleSwitch::setUI(Ui_VadSim *___ui) {

    reinterpret_cast<vsActorRelocateInteractor*>(this->TrackballActor)->setUI(___ui);
}


void vsInteractorStyleSwitch::OnChar() {

      switch (this->Interactor->GetKeyCode())
      {
        case 'j':
        case 'J':
          break;
        case 't':
        case 'T':
          this->JoystickOrTrackball = VTKIS_TRACKBALL;
          this->MultiTouch = false;
          this->EventCallbackCommand->SetAbortFlag(1);
          break;
        case 'c':
        case 'C':
          setCameraMode();
          return;
        case 'a':
        case 'A':
          setMoveMode();
          return;
        case 'm':
        case 'M':
          //this->MultiTouch = true;
          //this->EventCallbackCommand->SetAbortFlag(1);
          break;
      }
      // Set the CurrentStyle pointer to the picked style
      this->SetCurrentStyle();

}

/**
 * @brief vsInteractorStyleSwitch::setModeName
 * @param str
 */
void vsInteractorStyleSwitch::setModeName(QString str) {

    vsActorRelocateInteractor* movActor = reinterpret_cast<vsActorRelocateInteractor*>(this->TrackballActor);
    if (movActor) {
        Ui_VadSim* ___ui = movActor->getUI();
        if (___ui) {
            if (___ui->label_ModeDesc) {
                ___ui->label_ModeDesc->setText( QString("Operate Mode:") + str );
            }
        }
    }

}
