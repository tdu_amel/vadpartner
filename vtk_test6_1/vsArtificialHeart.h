﻿#ifndef VSXMLREADER_H
#define VSXMLREADER_H
#include "vsvtkinitializer.h"
#include <boost/shared_ptr.hpp>

#include <list>
#include <vtkObject.h>
#include <vtkSmartPointer.h>
#include <string>
#include <tinyxml2.h>
#include <QString>
#include <QFileInfo.h>
#include <QDir.h>

class vtkActor;


struct vsArtificialHeart
{
    boost::shared_ptr<vsArtificialHeart> firstChild;
    boost::shared_ptr<vsArtificialHeart> nextSibling;
    QString name;
    std::string attr;

    vsArtificialHeart() {

    }
};




class vsArtificialHeartParser:public vtkObject
{
private:
    QDir folderName;
public:
    static vsArtificialHeartParser *New();


    /**
     * @brief vsArtificialHeartParser::parseXML コンパウンドVADの情報が記録されたXMLの解析を行って、パーツ分けされたVADデータを構築する
     * @param file　読み込むXML
     * @param ah　構築されたデータが入るスマートポインタ
     */
    bool parseXML(QString& file,boost::shared_ptr<vsArtificialHeart>& ah);

    /**
     * @brief vsArtificialHeartParser::getElement
     * @param me 下のelementの読み込んだ情報の保存先
     * @param element　上のmeの情報が入っていたファイルタグ
     */
    void getElement(boost::shared_ptr<vsArtificialHeart>& me,tinyxml2::XMLElement* element);

};



//std::list< vtkSmartPointer<vsArtificialHeart> > obj;
typedef std::list< vtkSmartPointer<vsArtificialHeart> > vsArtificialHeartList;






#endif // VSXMLREADER_H
