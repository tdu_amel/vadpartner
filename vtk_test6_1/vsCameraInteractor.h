﻿#ifndef myMouseInteractorStyle_H
#define myMouseInteractorStyle_H
#include "vMsg.h"
#include "vstreeitemcontainer.h"
#include "vsvtkinitializer.h"
#include <vtkAngleRepresentation3D.h>
#include <vtkAngleWidget.h>
#include <vtkDistanceRepresentation3D.h>
#include <vtkDistanceWidget.h>
#include <vtkInteractorStyleJoystickActor.h>
#include <vtkInteractorStyleJoystickCamera.h>
#include <vtkInteractorStyleTrackballActor.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkSmartPointer.h>

//#include "ui_mainwindow.h"


// Define interaction style
class vsCameraInteractor : public vtkInteractorStyleTrackballCamera
{
private:
    vtkDistanceRepresentation* distRep;
    vtkAngleRepresentation* angleRep;

  public:
    static vsCameraInteractor* New();

    vsCameraInteractor():distRep(0),angleRep(0) {

    }

    virtual ~vsCameraInteractor(){
    }

    void setDistRepresetation(vtkDistanceRepresentation *_rep) {
        distRep = _rep;
    }



    void setAngleRepresetation(vtkAngleRepresentation *_rep) {
        angleRep = _rep;
    }

    void OnMiddleButtonDown(){}
    void OnMiddleButtonUp(){}

    void OnLeftButtonDown() {
        vtkInteractorStyleTrackballCamera::OnMiddleButtonDown();
    }

    void OnLeftButtonUp() {
         vtkInteractorStyleTrackballCamera::OnMiddleButtonUp();
    }


    void OnRightButtonDown() {
        vtkInteractorStyleTrackballCamera::OnLeftButtonDown();
    }

    void OnRightButtonUp() {
         vtkInteractorStyleTrackballCamera::OnLeftButtonUp();
    }

};





#endif // myMouseInteractorStyle_H
