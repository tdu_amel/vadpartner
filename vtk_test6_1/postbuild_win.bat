SETLOCAL
:called by qmake 
:	projectfile(.pro) line 308 (or around there...)-> "mypostprocess.commands =  [directory]/postbuild_win.bat $$TARGET $$OUT_PWD"
:%1 $$TARGET(=binary basename without extention)
:%2 configure type, debug or release
:%3 $$OUT_PWD(=output directory)
:%4 backupDir

:@echo ARG1 is %1
:@echo ARG2 is %2
:@echo ARG3 is %3
:@echo ARG4 is %4

SET BACKUPDIR=%~dp0..\bin\%4\%2
SET MYQT_OUTPUTDIR=%3\%2
:SET MYQT_OUTPUTNAME=%1
:SET MYQTBIN_PATH=%MYQT_OUTPUTDIR%\%MYQT_OUTPUTNAME%.exe
:SET MYQTPDB_PATH=%MYQT_OUTPUTDIR%\%MYQT_OUTPUTNAME%.pdb

:qmファイル(GUIの翻訳設定ファイル)のコピー
copy %~dp0\*.qm %MYQT_OUTPUTDIR%


:IF NOT EXIST %BACKUPDIR% mkdir %BACKUPDIR%

:: exeファイルのコピー
:IF EXIST %MYQTBIN_PATH% (
	:copy %MYQTBIN_PATH% %BACKUPDIR%
:)

:: pdbファイルのコピー
:IF EXIST %MYQTPDB_PATH% (
:	copy %MYQTPDB_PATH% %BACKUPDIR%
:)

ENDLOCAL