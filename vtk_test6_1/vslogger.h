﻿#ifndef VSLOGGER_H
#define VSLOGGER_H
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/file_sinks.h>

/**
 * @brief The vsLogger class ログクラス
 *
 */
class vsLogger:public boost::noncopyable //コピー禁止
{
public:
    /**
     * @brief getInstance ロガーにアクセスするための関数
     * @return　作成されたもしくは作成済みのロガーのインスタンスを変えす
     */
    static vsLogger* getInstance();

    void init(char* sink, char* filePath);
    void addCategory(char* category);
    void infoLog(char* category, char* log);
    //..errorなども適当に追加
private:
    shared_ptr<spdlog::sinks::daily_file_sink_mt> m_pLogger;
    map<string,spdlog::sink_ptr> m_pLogMap;
};

#endif // VSLOGGER_H
