﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "vsvtkinitializer.h"
#include <QMainWindow>
#include <vtkSmartVolumeMapper.h>
#include <vtkSmartPointer.h>
#include <vtkCollection.h>
#include <QMenu.h>
#include <QMenuBar.h>
#include <QSharedPointer>
#include <boost/shared_ptr.hpp>
#include <QThread>
#include "vsExtraction.h"
#include "vsTransverseView.h"
#include "vstreeitemcontainer.h"
#include "vsSTLManager.h"
#include "vsliverdataconstructor.h"



// Forward Qt class declarations
namespace Ui {
class VadSim;
}

/*
 * Qtクラス以外の前方クラス宣言
 */
class vtkRenderWindow;
class vtkRenderer;
class QVTKInteractor;
class vtkRenderWindowInteractor;
class vtkTextWidget;
class vtkAxesActor;
class vtkOrientationMarkerWidget;
class vtkCamera;
class vtkActor;
class vtkDistanceWidget;
class vtkAngleWidget;
class vsInteractorStyleSwitch;
class vtkDistanceRepresentation3D;
class vtkAngleRepresentation3D;
class vsTreeItem;
class QTreeWidgetItem;
struct vsArtificialHeart;
class vsTransverseView;
class vsDataExporterSetVal;
struct distanceWidgetCallBack;


/**
 * @brief 4つに分かれたビューがあるメインウィンドウのクラス　いわゆる「神クラス」化しているのでだれか機能を分割してください
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT
public:

    // Constructor/Destructor
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


protected:

    /**
     * @brief initGUI　GUIの設定を行う
     */
    void initGUI();

    /**
     * @brief initVTK　VTKの初期化処理を行う
     */
    void initVTK();

public:
    /**
     * @brief resetCamera　カメラの初期化を行う
     * @param v　初期化を行いたいビュー(4分割部分)のID
     */
    void resetCamera(int v);

    /**
     * @brief rerender　すべてのビューでカメラの初期化を行う
     */
    void resetCameraAll();

    /**
     * @brief rerender　再描画を行う
     * @param v　再描画を行いたいビュー(4分割部分)のID
     */
    void rerender(int v);

    /**
     * @brief rerender　すべてのビューで再描画を行う
     */
    void rerenderAll();


    void construct_Lung(vsTreeItem* treeitemHumanBody, vtkImageData *loadedDicom);
    void construct_Bone(vsTreeItem* treeitemHumanBody, vtkImageData *loadedDicom);
    void construct_Liver(vsTreeItem* treeitemHumanBody, vtkImageData *loadedDicom, double w, double l);
    void construct_Heart(vsTreeItem* treeitemHumanBody, vtkImageData *loadedDicom);

    void loadSTL();
    void loadCompoundSTL();
    void loadCT();

 /*
  * 以下はスロット　一般的にはイベントハンドラやコールバックと呼ばれるもの
  * 　「ボタンを押す」など「シグナル」が発行されたら，それと結びつけられたスロットが自動で呼ばれる
  * 結び付けの処理はinitGUIを参照
 */
public slots:

    void slotExit();

    void slotSetAngleWidget();
    void slotSetDistanceWidget();

    /**
     * @brief VadSim::slotResetCamera
     * カメラリセットのボタンを押したときに呼ばれるスロット
     */
    void slotResetCamera();
    void slotEditingFinished();
    void slotRadioClicked();
    void slotTreeSelected (QTreeWidgetItem * item, int);
    void slotSlideBarChanged(int val);
    void slotSlideBarChanged2(int);

    void export_measure_dist_start();
    void export_measure_dist_end();

    void export_measure_angle_start();
    void export_measure_angle_end();

    void on_pushButton_toCamMode_clicked();

    void on_pushButton_toMoveMode_clicked();

    /*
     * 以下はメニューアイテムを押したときに呼ばれるスロット
     */
    void on_actionOpen_DICOM_triggered();

    void on_actionOpen_Compound_VAD_triggered();

    void on_actionOpen_VAD_triggered();

private:

    bool event(QEvent *e);

    /**
     * @brief VadSim::closeEvent
     * ウィンドウが閉じられるときに自動的に呼ばれる関数
     * @param e
     * イベントオブジェクト
     */
    void closeEvent(QCloseEvent* e);

    //Designer form
    Ui::VadSim *ui;

    void setOrientionIcon();

    /**
     * @brief VadSim::addCompoundSTL
     * XMLをパースして得られたファイル名から実際にSTL読み込んでいく．
     * @param parent
     *  treeviewの親アイテム
     * @param ah
     * STLのデータとファイル名を入れるが，でこの時点ではファイル名のみが入っている
     */
    void addCompoundSTL(QTreeWidgetItem* parent,boost::shared_ptr<vsArtificialHeart>& ah);


 /*
 * メンバー変数
 */
private:

    QSharedPointer<vsDataExporterSetVal> m_setvalWindow;///<データ出力ウィンドウへのポインタ

    vsTransverseView view[3];///< 二次元ビュー(三方向)を保管しておく配列
    vtkSmartPointer<vtkRenderer> m_renderer[4];///<　二次元ビュー＋三次元ビューのレンダラーを保管しておく配列
    vtkRenderWindowInteractor* m_renderinteractor[4];///<　二次元ビュー＋三次元ビューのインターアクター(マウスやキーのコールバック)を保管しておく配列
    vtkSmartPointer<vtkCamera> m_camera[4];///<　二次元ビュー＋三次元ビューのカメラを保管しておく配列
    vtkSmartPointer<vsInteractorStyleSwitch> m_style;///<三次元ビューのインターアクターの細かい設定を保存しておくクラスのスマートポインタ

    vtkSmartPointer< vtkAxesActor > axes;///< vtkOrientationMarkerWidgetで使う矢印
    vtkSmartPointer<vtkOrientationMarkerWidget>  widget;///< 三次元ビューで小さな座標軸を表示するウィジェット

    vtkSmartPointer<vtkDistanceWidget> distanceWidget;///< 三次元ビューで距離測定（のマウス位置取得とか距離計算とかの内部処理）をするウィジェット
    vtkSmartPointer<vtkDistanceRepresentation3D> dist_representation;///< 距離測定ウィジェットの見た目
    vtkSmartPointer<distanceWidgetCallBack> _dist_widget_callback;

    vtkSmartPointer<vtkAngleWidget> angleWidget;///< 三次元ビューで角度測定をするウィジェット
    vtkSmartPointer<vtkAngleRepresentation3D> _angle_representation;///< 角度測定ウィジェットの見た目

    vsSTLManager m_stlManager;///<　STLの選択処理で色を変えたりするためにSTLはこのマネージャを通して読み込みを行うこと
    vsTreeItemContainer m_treeItems;///< ツリービューのアイテムを保存しておくクラス
    vsExtraction m_extraction;

    /*
     * Actorは実際に描画するときに使うデータのこと
     */
    vtkSmartPointer<vtkActor> actor_bone;
    vtkSmartPointer<vtkActor> actor_lung;
    vtkSmartPointer<vtkActor> actor_heart;
    vtkSmartPointer<vtkActor> actor_liver;
    vtkSmartPointer<vtkActor> actor_dicom_plane;

};

#endif // MAINWINDOW_H
