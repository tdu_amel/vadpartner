﻿#include "vsdicomgrayscaller.h"
#include <vtkImageData.h>
#include <QDebug>

vsDicomGrayScaller::vsDicomGrayScaller():m_iWindowWidth(200),m_iWindowCenter(100) {


}

void vsDicomGrayScaller::setWCWL( const double window_center, const double window_level ) {

    m_iWindowWidth = window_level;
    m_iWindowCenter = window_center;

}


void vsDicomGrayScaller::getPixels(vtkSmartPointer<vtkImageData>& dstImage, vtkImageData *srcImage,bool transparent ) {


    double ct_min = (double)((double)m_iWindowCenter - 0.5  - (double)((m_iWindowWidth -1.0 ) / 2.0));
    double ct_max = (double)((double)m_iWindowCenter  + (double)((m_iWindowWidth ) / 2.0));

    //vMsg("min max = (%d %d)",ct_min,ct_max);
    int* dims = srcImage->GetDimensions();
 //std::cout << "Dims: " << " x: " << dims[0] << " y: " << dims[1] << " z: " << dims[2] << std::endl;

    int* ext = srcImage->GetExtent();


    //
    dstImage = vtkSmartPointer<vtkImageData>::New();

    //次元を設定．
    dstImage->SetDimensions(dims[0],dims[1],dims[2]);



    //1ピクセルあたりのデータ量を設定
    dstImage->AllocateScalars(VTK_TYPE_UINT8,4);

/*
 *


int idxR, idxY, idxZ;
int maxY, maxZ;
vtkIdType inIncX, inIncY, inIncZ;
vtkIdType outIncX, outIncY, outIncZ;
int rowLength;
int outExt[6];

inData->GetContinuousIncrements(outExt, inIncX, inIncY, inIncZ);
outData->GetContinuousIncrements(outExt, outIncX, outIncY, outIncZ);

rowLength = (outExt[1] - outExt[0]+1) * inData->GetNumberOfScalarComponents();
maxY = outExt[3] - outExt[2];
maxZ = outExt[5] - outExt[4];

for (idxZ = 0; idxZ <= maxZ; idxZ++)
{
    for (idxY = 0; !self->AbortExecute && idxY <= maxY; idxY++)
    {

        for (idxR = 0; idxR < rowLength; idxR++)
        {
            // Pixel operation. Add foo. Dumber would be impossible.
            *outPtr = (OT)((float)(*inPtr) + foo);
            outPtr++;
            inPtr++;
        }

        outPtr += outIncY;
        inPtr += inIncY;

    }

    outPtr += outIncZ;
    inPtr += inIncZ;
}

 *
 */
      for( int z = 0; z < dims[2]; ++z) {
        for( int y = 0; y < dims[1]; ++y) {
            for( int x = 0; x < dims[0]; ++x) {

                short* src_pixel = static_cast<short*>(srcImage->GetScalarPointer(x,y,z));
                unsigned char* dst_pixel = static_cast<unsigned char*>(dstImage->GetScalarPointer(x,y,z));

                unsigned char tmp = 0;

                if((double)((*src_pixel)) <=  ct_min) { // 最小値

                        tmp = 0;

                } else if((double)((*src_pixel)) > ct_max) { // 最大値

                        tmp = 0xff;

                } else { // 中間値をとる

                        double result = (double)(*src_pixel) - ct_min;
                        result /= (double)((double)m_iWindowWidth-1.0);

                        result *= 255.0;
                        if(result > 255 ) result = 255;
                        if(result < 0 ) result = 0;
                        tmp = (unsigned char)ceil(result); // 切り上げを行う

               }


               dst_pixel[0] = dst_pixel[1] = dst_pixel[2] = (unsigned char)tmp;

               dst_pixel[3] = 255;
               if( transparent ) {
                   //不透明度の決定(完全に黒のところは消す)
                   if( dst_pixel[0] <= 0 )dst_pixel[3] = 0;
               }



            }
        }
    }
}
