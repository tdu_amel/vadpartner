<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja_JP">
<context>
    <name>DataExporterSetVal</name>
    <message>
        <location filename="dataexportersetval.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dataexportersetval.ui" line="35"/>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dataexportersetval.ui" line="68"/>
        <source>Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dataexportersetval.ui" line="98"/>
        <source>Edit</source>
        <translation>編集</translation>
    </message>
    <message>
        <location filename="dataexportersetval.ui" line="105"/>
        <source>New</source>
        <translation>追加</translation>
    </message>
    <message>
        <location filename="dataexportersetval.ui" line="112"/>
        <source>Delete</source>
        <translation>削除</translation>
    </message>
</context>
<context>
    <name>DataExporterSetting</name>
    <message>
        <location filename="dataexportersetting.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dataexportersetting.ui" line="31"/>
        <source>Setting Name</source>
        <translation>セッティング名</translation>
    </message>
    <message>
        <location filename="dataexportersetting.ui" line="46"/>
        <source>identification</source>
        <translation>識別子</translation>
    </message>
    <message>
        <location filename="dataexportersetting.ui" line="51"/>
        <source>data type</source>
        <translation>データ形式</translation>
    </message>
    <message>
        <location filename="dataexportersetting.ui" line="56"/>
        <source>data value</source>
        <translation>値</translation>
    </message>
    <message>
        <location filename="dataexportersetting.ui" line="69"/>
        <source>+</source>
        <translation></translation>
    </message>
    <message>
        <location filename="dataexportersetting.ui" line="76"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="dataexportersetting.ui" line="119"/>
        <source>OK</source>
        <translation></translation>
    </message>
    <message>
        <location filename="dataexportersetting.ui" line="126"/>
        <source>Cancel</source>
        <translation>キャンセル</translation>
    </message>
    <message>
        <location filename="dataexportersetting.ui" line="133"/>
        <source>Apply</source>
        <translation>適用</translation>
    </message>
    <message>
        <location filename="DataExporterSetting.cpp" line="30"/>
        <source>dicom(yyyy/mm/dd)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="DataExporterSetting.cpp" line="31"/>
        <source>today(yyyy/mm/dd)</source>
        <translation>今日(yyyy/mm/dd)</translation>
    </message>
    <message>
        <location filename="DataExporterSetting.cpp" line="32"/>
        <source>today(yyyy/mm/dd hh:mm:ss)</source>
        <translation>今日(yyyy/mm/dd hh:mm:ss)</translation>
    </message>
    <message>
        <location filename="DataExporterSetting.cpp" line="33"/>
        <source>ExportTime(yyyy/mm/dd)</source>
        <translation>出力時間(yyyy/mm/dd)</translation>
    </message>
    <message>
        <location filename="DataExporterSetting.cpp" line="34"/>
        <location filename="DataExporterSetting.cpp" line="35"/>
        <source>ExportTime(yyyy/mm/dd hh:mm:ss)</source>
        <translation>出力時間(yyyy/mm/dd hh:mm:ss)</translation>
    </message>
    <message>
        <location filename="DataExporterSetting.cpp" line="36"/>
        <source>User Input(yyyy/mm/dd)</source>
        <translation>自由入力(yyyy/mm/dd)</translation>
    </message>
    <message>
        <location filename="DataExporterSetting.cpp" line="37"/>
        <source>User Input(yyyy/mm/dd hh:mm:ss)</source>
        <translation>自由入力(yyyy/mm/dd hh:mm:ss)</translation>
    </message>
    <message>
        <location filename="DataExporterSetting.cpp" line="43"/>
        <source>Measured Angle[deg]</source>
        <translation>計測角度[deg]</translation>
    </message>
    <message>
        <location filename="DataExporterSetting.cpp" line="44"/>
        <source>Measured Distance[mm]</source>
        <translation>計測距離[mm]</translation>
    </message>
    <message>
        <location filename="DataExporterSetting.cpp" line="45"/>
        <source>User Input</source>
        <translation>自由入力</translation>
    </message>
</context>
<context>
    <name>VadSim</name>
    <message>
        <location filename="mainwindow.ui" line="20"/>
        <source>VadSim</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="85"/>
        <source>GroupBox</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="106"/>
        <source>absolute coord</source>
        <translation>絶対座標</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="113"/>
        <source>relative coord</source>
        <translation>相対座標</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="147"/>
        <source>MaxCT</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="154"/>
        <location filename="mainwindow.ui" line="208"/>
        <source>25</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="201"/>
        <source>MinCT</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="255"/>
        <source>Slice</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="277"/>
        <source>ObjectName:</source>
        <translation>操作中のオブジェクト：</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="293"/>
        <source>Position</source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="428"/>
        <source>Angle</source>
        <translation>角度</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="574"/>
        <source>ExportMode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="581"/>
        <source>Dst Measure</source>
        <translation>距離計測</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="604"/>
        <source>Angle Measure</source>
        <translation>角度計測</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="627"/>
        <source>Reset Camera</source>
        <translation>カメラリセット</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="641"/>
        <source>Camera Mode</source>
        <translation>カメラモード</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="664"/>
        <source>Move Model Mode</source>
        <translation>VAD操作モード</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="338"/>
        <location filename="mainwindow.ui" line="473"/>
        <source>X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="375"/>
        <location filename="mainwindow.ui" line="510"/>
        <source>Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="412"/>
        <location filename="mainwindow.ui" line="547"/>
        <source>Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="719"/>
        <source>Exit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="724"/>
        <source>Help</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="127"/>
        <source>Choose Compound Vad Data</source>
        <translation>複合VADデータを選択してください</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="219"/>
        <source>Choose Vad Data file</source>
        <translation>VADデータファイルを選択してください</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="527"/>
        <source>Choose CT data directory</source>
        <translation>CTデータファイルがあるフォルダを選択してください</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="769"/>
        <source>Data List</source>
        <translation>データリスト</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="792"/>
        <source>Open DICOM data</source>
        <oldsource>&amp;OpenDICOM</oldsource>
        <translation>DICOMファイルを開く</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="793"/>
        <source>Open DICOM files from Selected Directory</source>
        <translation>選択したフォルダからDICOMファイルを読み込み、人体データを作成します</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="797"/>
        <source>Open VAD data</source>
        <translation>VADデータを開く</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="808"/>
        <source>File(&amp;F)</source>
        <translation>ファイル(&amp;F)</translation>
    </message>
    <message>
        <source>OpenDICOM</source>
        <translation type="vanished">DICOMファイル(一人分)をフォルダを指定して開きます</translation>
    </message>
    <message>
        <source>&amp;OpenSTL</source>
        <translation type="vanished">(&amp;O)STLファイルを開く</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="798"/>
        <source>Open STL</source>
        <oldsource>OpenSTL</oldsource>
        <translation>STLファイル形式のVADデータを開きます</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="802"/>
        <source>OpenSTL(Compound)</source>
        <oldsource>&amp;OpenSTL(Compound)</oldsource>
        <translation>複合STLファイルを開く</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="803"/>
        <source>Load XML-Styled file as Compound VadData</source>
        <translation>XML形式のデータを読み込み、パーツ分けされた複合VADデータを構築します</translation>
    </message>
</context>
</TS>
