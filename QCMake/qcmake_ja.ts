<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja_JP">
<context>
    <name>MainWindow</name>
    <message>
        <source>where to make the project:</source>
        <oldsource>where to build the binaries:</oldsource>
        <translation type="vanished">どこにプロジェクトファイルを作成するか：</translation>
    </message>
    <message>
        <source>where is the vtk code:</source>
        <translation type="vanished">VTKのライブラリのある場所：</translation>
    </message>
    <message>
        <source>where is the source code:</source>
        <translation type="vanished">ソースコードのある場所：</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="256"/>
        <source>Open Source Code Directory</source>
        <translation>VadSimソースコードのディレクトリを開く</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="262"/>
        <source>Open VTK root Directory</source>
        <translation>VTKのルートディレクトリを開く</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="268"/>
        <source>Open Output Directory</source>
        <translation>プロジェクトの出力先ディレクトリを設定</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="276"/>
        <source>Open TinyXML Library Directory</source>
        <translation>TinyXMLライブラリのディレクトリを開く</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="282"/>
        <source>Open Boost Library Directory</source>
        <translation>Boostライブラリのディレクトリを開く</translation>
    </message>
</context>
<context>
    <name>QProjectMaker</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>QtWindowMaker</source>
        <translation>QtProjectMaker</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="34"/>
        <location filename="mainwindow.ui" line="100"/>
        <location filename="mainwindow.ui" line="107"/>
        <location filename="mainwindow.ui" line="135"/>
        <location filename="mainwindow.ui" line="161"/>
        <source>Browse</source>
        <translation>設定</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="54"/>
        <source>where is Boost Dir</source>
        <translation>どこにBoostライブラリがあるか</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="114"/>
        <source>where to make the project:</source>
        <translation>どこにプロジェクトファイルを作成するか：</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="121"/>
        <source>where is the vtk code:</source>
        <translation>VTKのライブラリのある場所：</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="128"/>
        <source>where is the source code:</source>
        <translation>ソースコードのある場所：</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="151"/>
        <source>where is TinyXML Dir</source>
        <translation>どこにTinyXMLライブラリがあるか</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="198"/>
        <source>Generate</source>
        <translation type="unfinished">プロジェクトファイルの作成</translation>
    </message>
</context>
</TS>
