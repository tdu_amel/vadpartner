#-------------------------------------------------
#
# Project created by QtCreator 2016-12-27T13:11:57
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtProjectMaker
TEMPLATE = app

TRANSLATIONS = qcmake_ja.ts

SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

DISTFILES += \
    qcmake_ja.ts


