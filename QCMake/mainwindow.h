﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_src_clicked();

    void on_pushButton_vtk_clicked();

    void on_pushButton_dst_clicked();

    void on_pushButton_gen_clicked();


    void on_pushButton_tinyxml_clicked();

    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
    QString m_srcDir;
    QString m_vtkDir;
    QString m_dstDir;
    QString m_boostDir;
    QString m_tinyXMLDir;
};

#endif // MAINWINDOW_H
