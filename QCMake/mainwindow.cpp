﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QFile>
#include <QTextStream>
#include <QRegExp>
#include <QStringList>
#include <QDirIterator>
#include <iostream>

    void writeFilePath(QTextStream& stream,QStringList &pattern,QDir &dir,QString writeFormat,bool checkSubDir = true) {

        QDir basePath(dir);

        QDirIterator it(dir.absolutePath(), pattern, QDir::Files, (checkSubDir)?QDirIterator::Subdirectories:QDirIterator::NoIteratorFlags);
        while (it.hasNext()) {
            QString str = writeFormat.arg(basePath.relativeFilePath(it.next()));
            stream << str;
        }
    }

    void writeCommonPart(QTextStream& stream) {

        stream << "QT       += core gui" << "\n";
        stream << "greaterThan(QT_MAJOR_VERSION, 4): QT += widgets"<<"\n";
        stream << "TARGET = VadSim"<<"\n";
        stream << "TEMPLATE = app"<<"\n";
        stream << "LIBTYPE = shared"<<"\n";
        stream << "#LIBTYPE = static"<<"\n";
        stream << "XMLLIBTYPE = $$LIBTYPE"<<"\n";
        stream << "CONFIG(debug,debug|release){"<<"\n";
        stream << "DEBUG_OR_RELEASE = debug"<<"\n";
        stream << "}"<<"\n";
        stream << "CONFIG(release,debug|release){"<<"\n";
        stream << "DEBUG_OR_RELEASE = release"<<"\n";
        stream << "}"<<"\n";
        stream << "TRANSLATIONS = vadsim_ja.ts"<<"\n";
        stream << "DISTFILES = \\\nvadsim_ja.ts"<<"\n";


    }


    void writeUIPath(QTextStream& stream,QDir &uiDir) {

        stream << "FORMS    += ";
        writeFilePath(stream,QStringList()<<"*.ui",uiDir,QString("\\\n\t%1"),true);
        stream << "\t\n";
    }

    void makeVtkIncDir(QString &vtkDir) {

        QDir incdir = vtkDir +"/include";
        if( !incdir.exists() ) {
            QDir().mkdir(incdir.absolutePath());
        }

        QDirIterator it(vtkDir,QStringList()<<"*.h"<<"*.txx", QDir::Files,QDirIterator::Subdirectories);
        while (it.hasNext()) {
            QString srcFileName = it.next();
            QFileInfo info(srcFileName);
            QString dstFileName = incdir.absolutePath()+"/"+info.fileName();
            if( info.dir() != incdir)
                QFile::copy(srcFileName,dstFileName);
        }

    }


    void writeIncPath(QTextStream& stream,QString &vtkDir,QString &boostDir,QString &tinyxmlDir) {

        makeVtkIncDir(vtkDir);

        QDir dDir(vtkDir);
        QDir bDir(boostDir);
        QDir xmlDir(tinyxmlDir);

        stream << "INCLUDEPATH    += ";
        stream << dDir.absolutePath()<<"/include" << "\\\n";
        stream << bDir.absolutePath() << "\\\n";
        stream << xmlDir.absolutePath() << "\n";

    }




    void writeLibPath(QTextStream& stream,QString &vtkDir,QString &tinyXmlDir) {

        QStringList releaseLib;
        QStringList debugLib;



        QDirIterator it(QDir(vtkDir).absolutePath(), QStringList()<<"*.lib", QDir::Files, QDirIterator::Subdirectories);
        while (it.hasNext()) {
            QString path = it.next();
            qDebug(path.toStdString().c_str());
            QFileInfo info(path);

            if( path.indexOf("debug",0,Qt::CaseInsensitive) != -1)
                debugLib << QString("\tLIBS += -L%1 -l%2\n").arg(info.absoluteDir().absolutePath(), info.baseName());
            if( path.indexOf("release",0,Qt::CaseInsensitive) != -1)
                releaseLib << QString("\tLIBS += -L%1 -l%2\n").arg(info.absoluteDir().absolutePath(),info.baseName());
        }


        QDirIterator it2(QDir(tinyXmlDir).absolutePath(), QStringList()<<"*.lib", QDir::Files, QDirIterator::Subdirectories);
        while (it2.hasNext()) {
            QString path = it2.next();
            qDebug(path.toStdString().c_str());
            QFileInfo info(path);

            if( path.indexOf("debug",0,Qt::CaseInsensitive) != -1)
                debugLib << QString("\tLIBS += -L%1 -l%2\n").arg(info.absoluteDir().absolutePath(), info.baseName());
            if( path.indexOf("release",0,Qt::CaseInsensitive) != -1)
                releaseLib << QString("\tLIBS += -L%1 -l%2\n").arg(info.absoluteDir().absolutePath(),info.baseName());
        }


        stream << "CONFIG(release,debug|release){"<<"\n";

        for (QStringList::iterator itr = releaseLib.begin();
                 itr != releaseLib.end(); ++itr) {
            stream << *itr;
        }
        stream << "}"<<"\n";

        stream << "CONFIG(debug,debug|release){"<<"\n";
        for (QStringList::iterator itr = debugLib.begin();
                 itr != debugLib.end(); ++itr) {
            stream << *itr;
        }
        stream << "}"<<"\n";
    }

    void writeHeaderPath(QTextStream& stream) {
        stream << "HEADERS =";
        writeFilePath(stream,QStringList()<<"*.h",QDir::current(),QString("\\\n\t%1"));
        stream << "\n";
    }


    void writeSourcePath(QTextStream& stream) {
        stream << "SOURCES =";
        writeFilePath(stream,QStringList()<<"*.cpp",QDir::current(),QString("\\\n\t%1"));
        stream << "\n";
    }



    void copyFile(QString &src,QString &dst) {

        QDirIterator it(dst,QStringList()<<"*.h"<<"*.txx"<<"*.cpp"<<"*.qm"<<"*.ts"<<"*.ui"<<"*.bat"<<"*.ico", QDir::Files,QDirIterator::Subdirectories);
        while (it.hasNext()) {
            QString srcFileName = it.next();
            QFileInfo info(srcFileName);
            QString dstFileName = src+"/"+info.fileName();
            if( srcFileName != dstFileName)
                QFile::copy(srcFileName,dstFileName);
        }

    }

    void writeOth(QTextStream& stream) {

        stream << "\n"
                << "CONFIG(debug, debug|release) {\n"
                << "BUILDTYPE = Debug\n"
                << "}";

        stream << "\n\n";


        stream << "CONFIG(release, debug|release) {\n"
               << "BUILDTYPE = Release\n"
               << "}\n";

        stream << "contains(QMAKE_HOST.arch, x86_64):{\n"
                         << "BITDEPTH = 64\n"
                         << "}\n";


       stream << "!contains(QMAKE_HOST.arch, x86_64):{\n"
                << "BITDEPTH = 32\n"
                << "}\n";


       stream << "equals(LIBTYPE, static){\n"
                << "DEFINES += VS_USE_STATIC_LIB\n"
                << "} else {\n"
                << "DEFINES += VS_USE_SHARED_LIB\n"
                << "}\n";


       stream << "#BACKUPPATH = \"build-Qt_\"$$QT_MAJOR_VERSION\"_\"$$QT_MINOR_VERSION\"_\"$$QT_PATCH_VERSION\"_\"$$BITDEPTH\"bit\"\n"
              << "#message($$OUT_PWD)\n"
              << "#message($$BACKUPPATH)\n"
              << "mypostprocess.target = debug_and_release\n";

        stream << "win32 {\n"
            << "RC_ICONS = appicon.ico" << "\n"
            << "\tOUT_PWD_WIN = $$replace(OUT_PWD, /, \\\\) #replace '/' to '\'\n"
            << "\tmypostprocess.commands =  $$IN_PWD/postbuild_win.bat $$TARGET $$BUILDTYPE $$OUT_PWD_WIN $$BACKUPPATH\n"

            << "\tgreaterThan(QT_MAJOR_VERSION, 4): {\n"

            << "\t\tDEPLOYTOOLDIR = $$replace(QMAKE_QMAKE, qmake.exe, )\n"
            << "\t\tDEPLOYTOOLDIR = $$replace(DEPLOYTOOLDIR, /, \\\\) #replace '/' to '\'\n"

            << "\tmessage(CALL:$$DEPLOYTOOLDIR\\windeployqt $$OUT_PWD_WIN)\n"
            << "\tmypostprocess2.target = debug_and_release\n"
            << "\tmypostprocess2.commands =\n"
            << "\tQMAKE_EXTRA_TARGETS += mypostprocess2\n"
            << "\t}\n"
        << " }\n";
        stream << "!win32 {\n"
            << "\tmypostprocess.commands =\n"
            << "\tmessage(Line $$_LINE_:No post build action. See \"mypostprocess.commands\" line in this .pro file )\n"
            << "}\n";

        stream << "QMAKE_EXTRA_TARGETS += mypostprocess\n";


    }

    void makeProjFile(QString &dstDir,QString &srcDir,QString &vtkDir,QString &boostDir,QString &tinyXMLDir) {

        //.h,.cpp,.uiなどをプロジェクトのフォルダにコピー
        copyFile(dstDir,srcDir);

        //VTKのヘッダファイルをコピーして一つのフォルダにまとめる
        makeVtkIncDir(vtkDir);

        //以降プロジェクトフォルダのみで作業するため、カレントディレクトリをプロジェクトフォルダに変更
        QDir::setCurrent(dstDir);
        QFile file("vadsim.pro");
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
            return;


        QTextStream out(&file);
        writeCommonPart(out);
        writeUIPath(out,QDir::current());
        writeIncPath(out,vtkDir,boostDir,tinyXMLDir);
        writeLibPath(out,vtkDir,tinyXMLDir);
        writeHeaderPath(out);
        writeSourcePath(out);
        writeOth(out);

    }

void MainWindow::on_pushButton_gen_clicked()
{
    makeProjFile(m_dstDir,m_srcDir,m_vtkDir,m_boostDir,m_tinyXMLDir);

}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_src_clicked()
{
    m_srcDir = QFileDialog::getExistingDirectory(this, tr("Open Source Code Directory"),m_srcDir );
    ui->lineEdit_src->setText(m_srcDir);
}

void MainWindow::on_pushButton_vtk_clicked()
{
    m_vtkDir = QFileDialog::getExistingDirectory(this, tr("Open VTK root Directory"),m_vtkDir );
    ui->lineEdit_vtk->setText(m_vtkDir);
}

void MainWindow::on_pushButton_dst_clicked()
{
    m_dstDir = QFileDialog::getExistingDirectory(this, tr("Open Output Directory"),m_dstDir );
    ui->lineEdit_proj->setText(m_dstDir);
}



void MainWindow::on_pushButton_tinyxml_clicked()
{
    m_tinyXMLDir = QFileDialog::getExistingDirectory(this, tr("Open TinyXML Library Directory"),m_dstDir );
    ui->lineEdit_tinyxml->setText(m_tinyXMLDir);
}

void MainWindow::on_pushButton_clicked()
{
    m_boostDir = QFileDialog::getExistingDirectory(this, tr("Open Boost Library Directory"),m_boostDir );
    ui->lineEdit_proj->setText(m_boostDir);
}
