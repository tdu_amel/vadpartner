#-------------------------------------------------
#
# Project created by QtCreator 2014-12-17T14:49:14
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets


TARGET = vtk_test6_1
TEMPLATE = app


#LIBTYPE = shared
LIBTYPE = static
XMLLIBTYPE = $$LIBTYPE
CONFIG(debug,debug|release){
DEBUG_OR_RELEASE = debug
}
CONFIG(release,debug|release){
DEBUG_OR_RELEASE = release
}

greaterThan(QT_MAJOR_VERSION, 4): {

CONFIG(debug,debug|release){
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkalglib-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkChartsCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkCommonColor-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkCommonComputationalGeometry-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkCommonCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkCommonDataModel-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkCommonExecutionModel-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkCommonMath-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkCommonMisc-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkCommonSystem-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkCommonTransforms-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkDICOMParser-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkDomainsChemistry-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkDomainsChemistryOpenGL2-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkexoIIc-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkexpat-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkFiltersAMR-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkFiltersCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkFiltersExtraction-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkFiltersFlowPaths-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkFiltersGeneral-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkFiltersGeneric-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkFiltersGeometry-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkFiltersHybrid-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkFiltersHyperTree-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkFiltersImaging-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkFiltersModeling-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkFiltersParallel-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkFiltersParallelImaging-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkFiltersPoints-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkFiltersProgrammable-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkFiltersSelection-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkFiltersSMP-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkFiltersSources-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkFiltersStatistics-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkFiltersTexture-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkFiltersVerdict-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkfreetype-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkGeovisCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkgl2ps-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkglew-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkGUISupportQt-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkGUISupportQtOpenGL-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkhdf5-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkhdf5_hl-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkImagingColor-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkImagingCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkImagingFourier-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkImagingGeneral-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkImagingHybrid-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkImagingMath-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkImagingMorphological-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkImagingSources-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkImagingStatistics-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkImagingStencil-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkInfovisCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkInfovisLayout-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkInteractionImage-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkInteractionStyle-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkInteractionWidgets-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkIOAMR-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkIOCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkIOEnSight-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkIOExodus-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkIOExport-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkIOGeometry-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkIOImage-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkIOImport-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkIOInfovis-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkIOLegacy-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkIOLSDyna-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkIOMINC-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkIOMovie-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkIONetCDF-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkIOParallel-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkIOParallelXML-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkIOPLY-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkIOSQL-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkIOTecplotTable-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkIOVideo-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkIOXML-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkIOXMLParser-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkjpeg-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkjsoncpp-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtklibxml2-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkmetaio-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkNetCDF-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkNetCDF_cxx-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkoggtheora-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkParallelCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkpng-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkproj4-7.1_d
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkRenderingAnnotation-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkRenderingContext2D-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkRenderingContextOpenGL2-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkRenderingCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkRenderingFreeType-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkRenderingGL2PSOpenGL2-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkRenderingImage-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkRenderingLabel-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkRenderingLOD-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkRenderingOpenGL2-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkRenderingVolume-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkRenderingVolumeOpenGL2-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtksqlite-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtksys-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtktiff-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkverdict-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkViewsContext2D-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkViewsCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkViewsInfovis-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/debug -lvtkzlib-7.1
}

CONFIG(release,debug|release){
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkalglib-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkChartsCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkCommonColor-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkCommonComputationalGeometry-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkCommonCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkCommonDataModel-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkCommonExecutionModel-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkCommonMath-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkCommonMisc-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkCommonSystem-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkCommonTransforms-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkDICOMParser-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkDomainsChemistry-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkDomainsChemistryOpenGL2-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkexoIIc-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkexpat-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkFiltersAMR-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkFiltersCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkFiltersExtraction-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkFiltersFlowPaths-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkFiltersGeneral-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkFiltersGeneric-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkFiltersGeometry-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkFiltersHybrid-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkFiltersHyperTree-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkFiltersImaging-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkFiltersModeling-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkFiltersParallel-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkFiltersParallelImaging-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkFiltersPoints-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkFiltersProgrammable-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkFiltersSelection-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkFiltersSMP-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkFiltersSources-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkFiltersStatistics-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkFiltersTexture-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkFiltersVerdict-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkfreetype-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkGeovisCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkgl2ps-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkglew-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkGUISupportQt-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkGUISupportQtOpenGL-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkhdf5-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkhdf5_hl-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkImagingColor-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkImagingCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkImagingFourier-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkImagingGeneral-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkImagingHybrid-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkImagingMath-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkImagingMorphological-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkImagingSources-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkImagingStatistics-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkImagingStencil-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkInfovisCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkInfovisLayout-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkInteractionImage-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkInteractionStyle-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkInteractionWidgets-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkIOAMR-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkIOCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkIOEnSight-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkIOExodus-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkIOExport-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkIOGeometry-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkIOImage-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkIOImport-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkIOInfovis-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkIOLegacy-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkIOLSDyna-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkIOMINC-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkIOMovie-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkIONetCDF-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkIOParallel-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkIOParallelXML-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkIOPLY-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkIOSQL-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkIOTecplotTable-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkIOVideo-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkIOXML-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkIOXMLParser-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkjpeg-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkjsoncpp-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtklibxml2-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkmetaio-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkNetCDF-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkNetCDF_cxx-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkoggtheora-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkParallelCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkpng-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkproj4-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkRenderingAnnotation-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkRenderingContext2D-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkRenderingContextOpenGL2-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkRenderingCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkRenderingFreeType-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkRenderingGL2PSOpenGL2-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkRenderingImage-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkRenderingLabel-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkRenderingLOD-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkRenderingOpenGL2-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkRenderingVolume-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkRenderingVolumeOpenGL2-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtksqlite-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtksys-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtktiff-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkverdict-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkViewsContext2D-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkViewsCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkViewsInfovis-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt5_$$LIBTYPE/lib/release -lvtkzlib-7.1
}
INCLUDEPATH += $$quote(C:/VTK/vtk7.0_build/qt5_$$LIBTYPE/include)
INCLUDEPATH += $$quote(C:/VTK/vtk7.0_build/qt5_$$LIBTYPE/include/vtk-7.1)
}#greaterThan(QT_MAJOR_VERSION, 4): {

!greaterThan(QT_MAJOR_VERSION, 4): {
CONFIG(debug,debug|release){
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkalglib-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkChartsCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkCommonColor-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkCommonComputationalGeometry-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkCommonCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkCommonDataModel-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkCommonExecutionModel-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkCommonMath-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkCommonMisc-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkCommonSystem-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkCommonTransforms-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkDICOMParser-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkDomainsChemistry-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkDomainsChemistryOpenGL2-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkexoIIc-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkexpat-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkFiltersAMR-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkFiltersCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkFiltersExtraction-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkFiltersFlowPaths-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkFiltersGeneral-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkFiltersGeneric-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkFiltersGeometry-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkFiltersHybrid-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkFiltersHyperTree-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkFiltersImaging-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkFiltersModeling-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkFiltersParallel-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkFiltersParallelImaging-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkFiltersPoints-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkFiltersProgrammable-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkFiltersSelection-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkFiltersSMP-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkFiltersSources-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkFiltersStatistics-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkFiltersTexture-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkFiltersVerdict-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkfreetype-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkGeovisCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkgl2ps-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkglew-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkGUISupportQt-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkGUISupportQtOpenGL-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkhdf5-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkhdf5_hl-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkImagingColor-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkImagingCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkImagingFourier-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkImagingGeneral-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkImagingHybrid-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkImagingMath-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkImagingMorphological-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkImagingSources-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkImagingStatistics-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkImagingStencil-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkInfovisCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkInfovisLayout-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkInteractionImage-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkInteractionStyle-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkInteractionWidgets-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkIOAMR-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkIOCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkIOEnSight-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkIOExodus-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkIOExport-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkIOGeometry-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkIOImage-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkIOImport-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkIOInfovis-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkIOLegacy-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkIOLSDyna-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkIOMINC-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkIOMovie-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkIONetCDF-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkIOParallel-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkIOParallelXML-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkIOPLY-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkIOSQL-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkIOTecplotTable-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkIOVideo-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkIOXML-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkIOXMLParser-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkjpeg-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkjsoncpp-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtklibxml2-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkmetaio-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkNetCDF-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkNetCDF_cxx-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkoggtheora-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkParallelCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkpng-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkproj4-7.1_d
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkRenderingAnnotation-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkRenderingContext2D-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkRenderingContextOpenGL2-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkRenderingCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkRenderingFreeType-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkRenderingGL2PSOpenGL2-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkRenderingImage-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkRenderingLabel-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkRenderingLOD-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkRenderingOpenGL2-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkRenderingVolume-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkRenderingVolumeOpenGL2-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtksqlite-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtksys-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtktiff-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkverdict-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkViewsContext2D-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkViewsCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkViewsInfovis-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/debug -lvtkzlib-7.1
}

CONFIG(release,debug|release){
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkalglib-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkChartsCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkCommonColor-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkCommonComputationalGeometry-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkCommonCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkCommonDataModel-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkCommonExecutionModel-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkCommonMath-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkCommonMisc-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkCommonSystem-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkCommonTransforms-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkDICOMParser-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkDomainsChemistry-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkDomainsChemistryOpenGL2-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkexoIIc-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkexpat-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkFiltersAMR-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkFiltersCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkFiltersExtraction-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkFiltersFlowPaths-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkFiltersGeneral-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkFiltersGeneric-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkFiltersGeometry-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkFiltersHybrid-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkFiltersHyperTree-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkFiltersImaging-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkFiltersModeling-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkFiltersParallel-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkFiltersParallelImaging-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkFiltersPoints-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkFiltersProgrammable-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkFiltersSelection-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkFiltersSMP-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkFiltersSources-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkFiltersStatistics-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkFiltersTexture-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkFiltersVerdict-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkfreetype-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkGeovisCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkgl2ps-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkglew-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkGUISupportQt-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkGUISupportQtOpenGL-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkhdf5-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkhdf5_hl-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkImagingColor-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkImagingCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkImagingFourier-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkImagingGeneral-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkImagingHybrid-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkImagingMath-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkImagingMorphological-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkImagingSources-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkImagingStatistics-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkImagingStencil-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkInfovisCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkInfovisLayout-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkInteractionImage-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkInteractionStyle-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkInteractionWidgets-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkIOAMR-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkIOCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkIOEnSight-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkIOExodus-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkIOExport-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkIOGeometry-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkIOImage-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkIOImport-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkIOInfovis-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkIOLegacy-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkIOLSDyna-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkIOMINC-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkIOMovie-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkIONetCDF-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkIOParallel-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkIOParallelXML-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkIOPLY-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkIOSQL-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkIOTecplotTable-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkIOVideo-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkIOXML-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkIOXMLParser-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkjpeg-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkjsoncpp-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtklibxml2-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkmetaio-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkNetCDF-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkNetCDF_cxx-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkoggtheora-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkParallelCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkpng-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkproj4-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkRenderingAnnotation-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkRenderingContext2D-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkRenderingContextOpenGL2-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkRenderingCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkRenderingFreeType-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkRenderingGL2PSOpenGL2-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkRenderingImage-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkRenderingLabel-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkRenderingLOD-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkRenderingOpenGL2-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkRenderingVolume-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkRenderingVolumeOpenGL2-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtksqlite-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtksys-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtktiff-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkverdict-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkViewsContext2D-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkViewsCore-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkViewsInfovis-7.1
        LIBS += -LC:/VTK/vtk7.0_build/qt4_$$LIBTYPE/lib/release -lvtkzlib-7.1
}

INCLUDEPATH += $$quote(C:/VTK/vtk7.0_build/qt4_$$LIBTYPE/include)
}#!greaterThan(QT_MAJOR_VERSION, 4): {


equals(LIBTYPE,shared) {
    MT_OR_MD = MD
} else {
    MT_OR_MD = MT
}

XMLLIBPATH = -LD:/VCFiles/ThirdParty/tinyxml2/lib/$$XMLLIBTYPE
XMLLIBPATH = $$join(XMLLIBPATH,,,_x64_)
XMLLIBPATH = $$join(XMLLIBPATH,,,$$MT_OR_MD)
message($$XMLLIBPATH)
INCLUDEPATH += $$quote(D:/VCFiles/ThirdParty/tinyxml2/include)
CONFIG(debug, debug|release) {
LIBS+= $$XMLLIBPATH -ltinyxml2d
}

CONFIG(release, debug|release) {
LIBS+= $$XMLLIBPATH -ltinyxml2
}



INCLUDEPATH += $$quote(D:/VCFiles/ThirdParty/boost_1_61_0)
INCLUDEPATH += $$quote(D:/VCFiles/ThirdParty/spdlog/include)

HEADERS += vtk_test6_1/mainwindow.h\
        vtk_test6_1/MyCounter.h\
        vtk_test6_1/vsActorRelocateInteractor.h\
        vtk_test6_1/vsArtificialHeart.h\
        vtk_test6_1/vsbodyboxproductor.h\
        vtk_test6_1/vsCameraInteractor.h\
        vtk_test6_1/vsdicomgrayscaller.h\
        vtk_test6_1/vsdicomseriesstyle.h\
        vtk_test6_1/vsextraction.h\
        vtk_test6_1/vshistgramemphasize.h\
        vtk_test6_1/vsImage.h\
        vtk_test6_1/vsImageDicom.h\
        vtk_test6_1/vsimagegrowing.h\
        vtk_test6_1/vsimageloaderdicom.h\
        vtk_test6_1/vsimageplanewidgetinteractstyle.h\
        vtk_test6_1/vsliverdataconstructor.h\
        vtk_test6_1/vslungdataconstructor.h\
        vtk_test6_1/vsmacro.h\
        vtk_test6_1/vsorganextraction.h\
        vtk_test6_1/vsregiongrowingchecker.h\
        vtk_test6_1/vsSTLManager.h\
        vtk_test6_1/vstransverseview.h\
        vtk_test6_1/vstreeitem.h\
        vtk_test6_1/vstreeitemcontainer.h\
        vtk_test6_1/vsvector.h\
        vtk_test6_1/vsViewport.h\
        vtk_test6_1/vsVolumeManager.h\
        vtk_test6_1/vsvtkinitializer.h\
        vtk_test6_1/vsbonedataconstructor.h \
        vtk_test6_1/vsdicomtoImgsaver.h \
    vtk_test6_1/vsheartdataconstructor.h \
    vtk_test6_1/vsexportdata.h \
    vtk_test6_1/vsinteractorstyleswitch.h \
    vtk_test6_1/vsmouseinteractorstyle.h \
    vtk_test6_1/vsutil.h \
    vtk_test6_1/vmsg.h \
    vtk_test6_1/vsctprogressfunctionobj.h \
    vtk_test6_1/vsdataexportersetval.h \
    vtk_test6_1/vsdataexportersetting.h \
    vtk_test6_1/vslogger.h

SOURCES += vtk_test6_1/main.cpp\
        vtk_test6_1/mainwindow.cpp\
        vtk_test6_1/vsActorRelocateInteractor.cpp\
        vtk_test6_1/vsArtificialHeart.cpp\
        vtk_test6_1/vsbodyboxproductor.cpp\
        vtk_test6_1/vsCameraInteractor.cpp\
        vtk_test6_1/vsdicomgrayscaller.cpp\
        vtk_test6_1/vsdicomseriesstyle.cpp\
        vtk_test6_1/vsextraction.cpp\
        vtk_test6_1/vshistgramemphasize.cpp\
        vtk_test6_1/vsimagegrowing.cpp\
        vtk_test6_1/vsimageloaderdicom.cpp\
        vtk_test6_1/vsimageplanewidgetinteractstyle.cpp\
        vtk_test6_1/vsliverdataconstructor.cpp\
        vtk_test6_1/vslungdataconstructor.cpp\
        vtk_test6_1/vsorganextraction.cpp\
        vtk_test6_1/vsregiongrowingchecker.cpp\
        vtk_test6_1/vsSTLManager.cpp\
        vtk_test6_1/vstransverseview.cpp\
        vtk_test6_1/vstreeitem.cpp\
        vtk_test6_1/vstreeitemcontainer.cpp\
        vtk_test6_1/vsviewport.cpp\
        vtk_test6_1/vsVolumeManager.cpp\
    vtk_test6_1/vsbonedataconstructor.cpp \
    vtk_test6_1/vsdicomtoImgsaver.cpp \
    vtk_test6_1/vsheartdataconstructor.cpp \
    vtk_test6_1/vsexportdata.cpp \
    vtk_test6_1/vsinteractorstyleswitch.cpp \
    vtk_test6_1/vmsg.cpp \
    vtk_test6_1/vsctprogressfunctionobj.cpp \
    vtk_test6_1/vsdataexportersetval.cpp \
    vtk_test6_1/vsDataExporterSetting.cpp \
    vtk_test6_1/vslogger.cpp




FORMS    += vtk_test6_1/mainwindow.ui \
    vtk_test6_1/dataexportersetting.ui \
    vtk_test6_1/dataexportersetval.ui


TRANSLATIONS = vtk_test6_1/vadsimtr_ja.ts

DISTFILES += \
    vtk_test6_1/vadsimtr_ja.ts



CONFIG(debug, debug|release) {
BUILDTYPE = Debug
}

CONFIG(release, debug|release) {
BUILDTYPE = Release
}

contains(QMAKE_HOST.arch, x86_64):{
    BITDEPTH = 64
}
!contains(QMAKE_HOST.arch, x86_64):{
    BITDEPTH = 32
}

equals(LIBTYPE, static){
    DEFINES += VS_USE_STATIC_LIB
} else {
    DEFINES += VS_USE_SHARED_LIB
}


#BACKUPPATH = "build-Qt_"$$QT_MAJOR_VERSION"_"$$QT_MINOR_VERSION"_"$$QT_PATCH_VERSION"_"$$BITDEPTH"bit"
#message($$OUT_PWD)
#message($$BACKUPPATH)
dammytarget.target = dammy
mypostprocess.target = dammytarget



win32 {

    RC_ICONS = vtk_test6_1/appicon.ico

    OUT_PWD_WIN = $$replace(OUT_PWD, /, \\) #replace '/' to '\'
    mypostprocess.commands =  $$PWD/vtk_test6_1/postbuild_win.bat $$TARGET $$BUILDTYPE $$OUT_PWD_WIN $$BACKUPPATH


    greaterThan(QT_MAJOR_VERSION, 4): {

        DEPLOYTOOLDIR = $$replace(QMAKE_QMAKE, qmake.exe, )
        DEPLOYTOOLDIR = $$replace(DEPLOYTOOLDIR, /, \\) #replace '/' to '\'

        #message(CALL:$$DEPLOYTOOLDIR\\windeployqt $$OUT_PWD_WIN)
        mypostprocess2.target = dammytarget
        mypostprocess2.commands =
        QMAKE_EXTRA_TARGETS += mypostprocess2
    }
}
!win32 {
    mypostprocess.commands =
    message(Line $$_LINE_:No post build action. See \"mypostprocess.commands\" line in this .pro file )
}


QMAKE_EXTRA_TARGETS += mypostprocess
POST_TARGETDEPS += dammytarget

RESOURCES += \
    resource.qrc

